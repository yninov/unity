﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "AssemblyU2DCSharp_Item_WeaponType3894335344.h"
#include "AssemblyU2DCSharp_Item_ItemType1150357105.h"
#include "AssemblyU2DCSharp_Item_ItemRarity3672222668.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Item
struct  Item_t2440468191  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 Item::itemID
	int32_t ___itemID_2;
	// System.String Item::itemName
	String_t* ___itemName_3;
	// System.Int32 Item::itemPrice
	int32_t ___itemPrice_4;
	// System.Single Item::armor
	float ___armor_5;
	// System.Single Item::shield
	float ___shield_6;
	// System.Single Item::shieldRegen
	float ___shieldRegen_7;
	// System.Single Item::speed
	float ___speed_8;
	// System.Single Item::fireRate
	float ___fireRate_9;
	// System.Single Item::shieldDamage
	float ___shieldDamage_10;
	// System.Single Item::armorDamage
	float ___armorDamage_11;
	// UnityEngine.Sprite Item::itemImage
	Sprite_t309593783 * ___itemImage_12;
	// UnityEngine.GameObject Item::itemVisual
	GameObject_t1756533147 * ___itemVisual_13;
	// System.Boolean Item::itemPurchased
	bool ___itemPurchased_14;
	// Item/WeaponType Item::weaponType
	int32_t ___weaponType_15;
	// Item/ItemType Item::itemType
	int32_t ___itemType_16;
	// Item/ItemRarity Item::itemRarity
	int32_t ___itemRarity_17;
	// System.String Item::itemShipType
	String_t* ___itemShipType_18;

public:
	inline static int32_t get_offset_of_itemID_2() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___itemID_2)); }
	inline int32_t get_itemID_2() const { return ___itemID_2; }
	inline int32_t* get_address_of_itemID_2() { return &___itemID_2; }
	inline void set_itemID_2(int32_t value)
	{
		___itemID_2 = value;
	}

	inline static int32_t get_offset_of_itemName_3() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___itemName_3)); }
	inline String_t* get_itemName_3() const { return ___itemName_3; }
	inline String_t** get_address_of_itemName_3() { return &___itemName_3; }
	inline void set_itemName_3(String_t* value)
	{
		___itemName_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemName_3, value);
	}

	inline static int32_t get_offset_of_itemPrice_4() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___itemPrice_4)); }
	inline int32_t get_itemPrice_4() const { return ___itemPrice_4; }
	inline int32_t* get_address_of_itemPrice_4() { return &___itemPrice_4; }
	inline void set_itemPrice_4(int32_t value)
	{
		___itemPrice_4 = value;
	}

	inline static int32_t get_offset_of_armor_5() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___armor_5)); }
	inline float get_armor_5() const { return ___armor_5; }
	inline float* get_address_of_armor_5() { return &___armor_5; }
	inline void set_armor_5(float value)
	{
		___armor_5 = value;
	}

	inline static int32_t get_offset_of_shield_6() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___shield_6)); }
	inline float get_shield_6() const { return ___shield_6; }
	inline float* get_address_of_shield_6() { return &___shield_6; }
	inline void set_shield_6(float value)
	{
		___shield_6 = value;
	}

	inline static int32_t get_offset_of_shieldRegen_7() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___shieldRegen_7)); }
	inline float get_shieldRegen_7() const { return ___shieldRegen_7; }
	inline float* get_address_of_shieldRegen_7() { return &___shieldRegen_7; }
	inline void set_shieldRegen_7(float value)
	{
		___shieldRegen_7 = value;
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___speed_8)); }
	inline float get_speed_8() const { return ___speed_8; }
	inline float* get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(float value)
	{
		___speed_8 = value;
	}

	inline static int32_t get_offset_of_fireRate_9() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___fireRate_9)); }
	inline float get_fireRate_9() const { return ___fireRate_9; }
	inline float* get_address_of_fireRate_9() { return &___fireRate_9; }
	inline void set_fireRate_9(float value)
	{
		___fireRate_9 = value;
	}

	inline static int32_t get_offset_of_shieldDamage_10() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___shieldDamage_10)); }
	inline float get_shieldDamage_10() const { return ___shieldDamage_10; }
	inline float* get_address_of_shieldDamage_10() { return &___shieldDamage_10; }
	inline void set_shieldDamage_10(float value)
	{
		___shieldDamage_10 = value;
	}

	inline static int32_t get_offset_of_armorDamage_11() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___armorDamage_11)); }
	inline float get_armorDamage_11() const { return ___armorDamage_11; }
	inline float* get_address_of_armorDamage_11() { return &___armorDamage_11; }
	inline void set_armorDamage_11(float value)
	{
		___armorDamage_11 = value;
	}

	inline static int32_t get_offset_of_itemImage_12() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___itemImage_12)); }
	inline Sprite_t309593783 * get_itemImage_12() const { return ___itemImage_12; }
	inline Sprite_t309593783 ** get_address_of_itemImage_12() { return &___itemImage_12; }
	inline void set_itemImage_12(Sprite_t309593783 * value)
	{
		___itemImage_12 = value;
		Il2CppCodeGenWriteBarrier(&___itemImage_12, value);
	}

	inline static int32_t get_offset_of_itemVisual_13() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___itemVisual_13)); }
	inline GameObject_t1756533147 * get_itemVisual_13() const { return ___itemVisual_13; }
	inline GameObject_t1756533147 ** get_address_of_itemVisual_13() { return &___itemVisual_13; }
	inline void set_itemVisual_13(GameObject_t1756533147 * value)
	{
		___itemVisual_13 = value;
		Il2CppCodeGenWriteBarrier(&___itemVisual_13, value);
	}

	inline static int32_t get_offset_of_itemPurchased_14() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___itemPurchased_14)); }
	inline bool get_itemPurchased_14() const { return ___itemPurchased_14; }
	inline bool* get_address_of_itemPurchased_14() { return &___itemPurchased_14; }
	inline void set_itemPurchased_14(bool value)
	{
		___itemPurchased_14 = value;
	}

	inline static int32_t get_offset_of_weaponType_15() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___weaponType_15)); }
	inline int32_t get_weaponType_15() const { return ___weaponType_15; }
	inline int32_t* get_address_of_weaponType_15() { return &___weaponType_15; }
	inline void set_weaponType_15(int32_t value)
	{
		___weaponType_15 = value;
	}

	inline static int32_t get_offset_of_itemType_16() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___itemType_16)); }
	inline int32_t get_itemType_16() const { return ___itemType_16; }
	inline int32_t* get_address_of_itemType_16() { return &___itemType_16; }
	inline void set_itemType_16(int32_t value)
	{
		___itemType_16 = value;
	}

	inline static int32_t get_offset_of_itemRarity_17() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___itemRarity_17)); }
	inline int32_t get_itemRarity_17() const { return ___itemRarity_17; }
	inline int32_t* get_address_of_itemRarity_17() { return &___itemRarity_17; }
	inline void set_itemRarity_17(int32_t value)
	{
		___itemRarity_17 = value;
	}

	inline static int32_t get_offset_of_itemShipType_18() { return static_cast<int32_t>(offsetof(Item_t2440468191, ___itemShipType_18)); }
	inline String_t* get_itemShipType_18() const { return ___itemShipType_18; }
	inline String_t** get_address_of_itemShipType_18() { return &___itemShipType_18; }
	inline void set_itemShipType_18(String_t* value)
	{
		___itemShipType_18 = value;
		Il2CppCodeGenWriteBarrier(&___itemShipType_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
