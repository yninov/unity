﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerShip
struct PlayerShip_t2635532215;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PlayerShip::.ctor(System.Int32,System.String,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Int32,System.String)
extern "C"  void PlayerShip__ctor_m2607274753 (PlayerShip_t2635532215 * __this, int32_t ___id0, String_t* ___name1, float ___armor2, float ___regenA3, float ___shield4, float ___regenS5, float ___speed6, float ___fireRate7, float ___adamage8, float ___sdamage9, bool ___purchased10, int32_t ___price11, String_t* ___type12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerShip::.ctor()
extern "C"  void PlayerShip__ctor_m3901879582 (PlayerShip_t2635532215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
