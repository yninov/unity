﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerShip
struct  PlayerShip_t2635532215  : public Il2CppObject
{
public:
	// System.Int32 PlayerShip::shipID
	int32_t ___shipID_0;
	// System.String PlayerShip::shipName
	String_t* ___shipName_1;
	// System.Single PlayerShip::shipMaxArmor
	float ___shipMaxArmor_2;
	// System.Single PlayerShip::shipRegenArmor
	float ___shipRegenArmor_3;
	// System.Single PlayerShip::shipMaxShield
	float ___shipMaxShield_4;
	// System.Single PlayerShip::shipRegenShield
	float ___shipRegenShield_5;
	// System.Single PlayerShip::shipSpeed
	float ___shipSpeed_6;
	// System.Single PlayerShip::shipFireRate
	float ___shipFireRate_7;
	// System.Single PlayerShip::armorDamage
	float ___armorDamage_8;
	// System.Single PlayerShip::shieldDamage
	float ___shieldDamage_9;
	// System.Boolean PlayerShip::purchasedShip
	bool ___purchasedShip_10;
	// System.Int32 PlayerShip::shipPrice
	int32_t ___shipPrice_11;
	// System.String PlayerShip::shipType
	String_t* ___shipType_12;

public:
	inline static int32_t get_offset_of_shipID_0() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipID_0)); }
	inline int32_t get_shipID_0() const { return ___shipID_0; }
	inline int32_t* get_address_of_shipID_0() { return &___shipID_0; }
	inline void set_shipID_0(int32_t value)
	{
		___shipID_0 = value;
	}

	inline static int32_t get_offset_of_shipName_1() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipName_1)); }
	inline String_t* get_shipName_1() const { return ___shipName_1; }
	inline String_t** get_address_of_shipName_1() { return &___shipName_1; }
	inline void set_shipName_1(String_t* value)
	{
		___shipName_1 = value;
		Il2CppCodeGenWriteBarrier(&___shipName_1, value);
	}

	inline static int32_t get_offset_of_shipMaxArmor_2() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipMaxArmor_2)); }
	inline float get_shipMaxArmor_2() const { return ___shipMaxArmor_2; }
	inline float* get_address_of_shipMaxArmor_2() { return &___shipMaxArmor_2; }
	inline void set_shipMaxArmor_2(float value)
	{
		___shipMaxArmor_2 = value;
	}

	inline static int32_t get_offset_of_shipRegenArmor_3() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipRegenArmor_3)); }
	inline float get_shipRegenArmor_3() const { return ___shipRegenArmor_3; }
	inline float* get_address_of_shipRegenArmor_3() { return &___shipRegenArmor_3; }
	inline void set_shipRegenArmor_3(float value)
	{
		___shipRegenArmor_3 = value;
	}

	inline static int32_t get_offset_of_shipMaxShield_4() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipMaxShield_4)); }
	inline float get_shipMaxShield_4() const { return ___shipMaxShield_4; }
	inline float* get_address_of_shipMaxShield_4() { return &___shipMaxShield_4; }
	inline void set_shipMaxShield_4(float value)
	{
		___shipMaxShield_4 = value;
	}

	inline static int32_t get_offset_of_shipRegenShield_5() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipRegenShield_5)); }
	inline float get_shipRegenShield_5() const { return ___shipRegenShield_5; }
	inline float* get_address_of_shipRegenShield_5() { return &___shipRegenShield_5; }
	inline void set_shipRegenShield_5(float value)
	{
		___shipRegenShield_5 = value;
	}

	inline static int32_t get_offset_of_shipSpeed_6() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipSpeed_6)); }
	inline float get_shipSpeed_6() const { return ___shipSpeed_6; }
	inline float* get_address_of_shipSpeed_6() { return &___shipSpeed_6; }
	inline void set_shipSpeed_6(float value)
	{
		___shipSpeed_6 = value;
	}

	inline static int32_t get_offset_of_shipFireRate_7() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipFireRate_7)); }
	inline float get_shipFireRate_7() const { return ___shipFireRate_7; }
	inline float* get_address_of_shipFireRate_7() { return &___shipFireRate_7; }
	inline void set_shipFireRate_7(float value)
	{
		___shipFireRate_7 = value;
	}

	inline static int32_t get_offset_of_armorDamage_8() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___armorDamage_8)); }
	inline float get_armorDamage_8() const { return ___armorDamage_8; }
	inline float* get_address_of_armorDamage_8() { return &___armorDamage_8; }
	inline void set_armorDamage_8(float value)
	{
		___armorDamage_8 = value;
	}

	inline static int32_t get_offset_of_shieldDamage_9() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shieldDamage_9)); }
	inline float get_shieldDamage_9() const { return ___shieldDamage_9; }
	inline float* get_address_of_shieldDamage_9() { return &___shieldDamage_9; }
	inline void set_shieldDamage_9(float value)
	{
		___shieldDamage_9 = value;
	}

	inline static int32_t get_offset_of_purchasedShip_10() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___purchasedShip_10)); }
	inline bool get_purchasedShip_10() const { return ___purchasedShip_10; }
	inline bool* get_address_of_purchasedShip_10() { return &___purchasedShip_10; }
	inline void set_purchasedShip_10(bool value)
	{
		___purchasedShip_10 = value;
	}

	inline static int32_t get_offset_of_shipPrice_11() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipPrice_11)); }
	inline int32_t get_shipPrice_11() const { return ___shipPrice_11; }
	inline int32_t* get_address_of_shipPrice_11() { return &___shipPrice_11; }
	inline void set_shipPrice_11(int32_t value)
	{
		___shipPrice_11 = value;
	}

	inline static int32_t get_offset_of_shipType_12() { return static_cast<int32_t>(offsetof(PlayerShip_t2635532215, ___shipType_12)); }
	inline String_t* get_shipType_12() const { return ___shipType_12; }
	inline String_t** get_address_of_shipType_12() { return &___shipType_12; }
	inline void set_shipType_12(String_t* value)
	{
		___shipType_12 = value;
		Il2CppCodeGenWriteBarrier(&___shipType_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
