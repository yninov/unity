﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InventoryPanel
struct InventoryPanel_t2677987186;
// Item
struct Item_t2440468191;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Item2440468191.h"

// System.Void InventoryPanel::.ctor()
extern "C"  void InventoryPanel__ctor_m1511447747 (InventoryPanel_t2677987186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryPanel::Start()
extern "C"  void InventoryPanel_Start_m3640293759 (InventoryPanel_t2677987186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryPanel::AddItem(Item)
extern "C"  void InventoryPanel_AddItem_m2777303932 (InventoryPanel_t2677987186 * __this, Item_t2440468191 * ___itemToAdd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryPanel::BuyFromMarket()
extern "C"  void InventoryPanel_BuyFromMarket_m2715282707 (InventoryPanel_t2677987186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryPanel::SellItemFromInventory()
extern "C"  void InventoryPanel_SellItemFromInventory_m1399147720 (InventoryPanel_t2677987186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryPanel::EquipItems()
extern "C"  void InventoryPanel_EquipItems_m3713614609 (InventoryPanel_t2677987186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
