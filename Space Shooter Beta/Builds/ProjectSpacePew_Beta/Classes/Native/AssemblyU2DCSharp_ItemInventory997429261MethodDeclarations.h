﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemInventory
struct ItemInventory_t997429261;
// Item
struct Item_t2440468191;
// ShipsEquipment
struct ShipsEquipment_t1003200637;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ShipsEquipment1003200637.h"
#include "AssemblyU2DCSharp_Item2440468191.h"

// System.Void ItemInventory::.ctor()
extern "C"  void ItemInventory__ctor_m1344467504 (ItemInventory_t997429261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInventory::Start()
extern "C"  void ItemInventory_Start_m720955688 (ItemInventory_t997429261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInventory::ItemsToList()
extern "C"  void ItemInventory_ItemsToList_m3796793559 (ItemInventory_t997429261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInventory::ControlItemsList()
extern "C"  void ItemInventory_ControlItemsList_m574720063 (ItemInventory_t997429261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInventory::ChekEquipmentAtStartUp()
extern "C"  void ItemInventory_ChekEquipmentAtStartUp_m3977349727 (ItemInventory_t997429261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInventory::ChekInventoryAtStartUp()
extern "C"  void ItemInventory_ChekInventoryAtStartUp_m2259022607 (ItemInventory_t997429261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Item ItemInventory::GetWeaponItem(System.Int32)
extern "C"  Item_t2440468191 * ItemInventory_GetWeaponItem_m1346040764 (ItemInventory_t997429261 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Item ItemInventory::GetArmorItem(System.Int32)
extern "C"  Item_t2440468191 * ItemInventory_GetArmorItem_m820844179 (ItemInventory_t997429261 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Item ItemInventory::GetShieldItem(System.Int32)
extern "C"  Item_t2440468191 * ItemInventory_GetShieldItem_m3227698875 (ItemInventory_t997429261 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Item ItemInventory::GetEngineItem(System.Int32)
extern "C"  Item_t2440468191 * ItemInventory_GetEngineItem_m2654585570 (ItemInventory_t997429261 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInventory::SpawnMarketItems()
extern "C"  void ItemInventory_SpawnMarketItems_m131373105 (ItemInventory_t997429261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInventory::SwitchEquipedItems(ShipsEquipment,Item)
extern "C"  void ItemInventory_SwitchEquipedItems_m1895146111 (ItemInventory_t997429261 * __this, ShipsEquipment_t1003200637 * ___ship0, Item_t2440468191 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInventory::InstantiateWeapon()
extern "C"  void ItemInventory_InstantiateWeapon_m2838280588 (ItemInventory_t997429261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemInventory::SwitchEquipedItemsVisual(Item)
extern "C"  void ItemInventory_SwitchEquipedItemsVisual_m1546765688 (ItemInventory_t997429261 * __this, Item_t2440468191 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
