﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemInventory
struct ItemInventory_t997429261;
// System.Collections.Generic.List`1<PlayerShip>
struct List_1_t2004653347;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipsDatabase
struct  ShipsDatabase_t1101834110  : public MonoBehaviour_t1158329972
{
public:
	// ItemInventory ShipsDatabase::inventory
	ItemInventory_t997429261 * ___inventory_2;
	// System.Collections.Generic.List`1<PlayerShip> ShipsDatabase::dataBase
	List_1_t2004653347 * ___dataBase_3;

public:
	inline static int32_t get_offset_of_inventory_2() { return static_cast<int32_t>(offsetof(ShipsDatabase_t1101834110, ___inventory_2)); }
	inline ItemInventory_t997429261 * get_inventory_2() const { return ___inventory_2; }
	inline ItemInventory_t997429261 ** get_address_of_inventory_2() { return &___inventory_2; }
	inline void set_inventory_2(ItemInventory_t997429261 * value)
	{
		___inventory_2 = value;
		Il2CppCodeGenWriteBarrier(&___inventory_2, value);
	}

	inline static int32_t get_offset_of_dataBase_3() { return static_cast<int32_t>(offsetof(ShipsDatabase_t1101834110, ___dataBase_3)); }
	inline List_1_t2004653347 * get_dataBase_3() const { return ___dataBase_3; }
	inline List_1_t2004653347 ** get_address_of_dataBase_3() { return &___dataBase_3; }
	inline void set_dataBase_3(List_1_t2004653347 * value)
	{
		___dataBase_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataBase_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
