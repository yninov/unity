﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemDatabase
struct ItemDatabase_t2502586666;
// System.Collections.Generic.List`1<ShipsEquipment>
struct List_1_t372321769;
// System.Collections.Generic.List`1<Item>
struct List_1_t1809589323;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// InventoryPanel
struct InventoryPanel_t2677987186;
// ShipsController
struct ShipsController_t970893927;
// ShipsEquipment
struct ShipsEquipment_t1003200637;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemInventory
struct  ItemInventory_t997429261  : public MonoBehaviour_t1158329972
{
public:
	// ItemDatabase ItemInventory::data
	ItemDatabase_t2502586666 * ___data_2;
	// System.Collections.Generic.List`1<ShipsEquipment> ItemInventory::shipsEquipment
	List_1_t372321769 * ___shipsEquipment_3;
	// System.Collections.Generic.List`1<Item> ItemInventory::weaponItems
	List_1_t1809589323 * ___weaponItems_4;
	// System.Collections.Generic.List`1<Item> ItemInventory::armorItems
	List_1_t1809589323 * ___armorItems_5;
	// System.Collections.Generic.List`1<Item> ItemInventory::shieldItems
	List_1_t1809589323 * ___shieldItems_6;
	// System.Collections.Generic.List`1<Item> ItemInventory::engineItems
	List_1_t1809589323 * ___engineItems_7;
	// System.Collections.Generic.List`1<System.Int32> ItemInventory::inventoryByID
	List_1_t1440998580 * ___inventoryByID_8;
	// UnityEngine.GameObject ItemInventory::weaponsSlotMenu
	GameObject_t1756533147 * ___weaponsSlotMenu_9;
	// UnityEngine.GameObject ItemInventory::armorSlotMenu
	GameObject_t1756533147 * ___armorSlotMenu_10;
	// UnityEngine.GameObject ItemInventory::shieldSlotMenu
	GameObject_t1756533147 * ___shieldSlotMenu_11;
	// UnityEngine.GameObject ItemInventory::engineSlotMenu
	GameObject_t1756533147 * ___engineSlotMenu_12;
	// UnityEngine.GameObject ItemInventory::weaponHolder
	GameObject_t1756533147 * ___weaponHolder_13;
	// UnityEngine.GameObject ItemInventory::armorHolder
	GameObject_t1756533147 * ___armorHolder_14;
	// UnityEngine.GameObject ItemInventory::shieldHolder
	GameObject_t1756533147 * ___shieldHolder_15;
	// UnityEngine.GameObject ItemInventory::engineHolder
	GameObject_t1756533147 * ___engineHolder_16;
	// UnityEngine.GameObject ItemInventory::inventoryPanelBuy
	GameObject_t1756533147 * ___inventoryPanelBuy_17;
	// UnityEngine.GameObject ItemInventory::inventoryPanelEquip
	GameObject_t1756533147 * ___inventoryPanelEquip_18;
	// InventoryPanel ItemInventory::marketInventoryPanel
	InventoryPanel_t2677987186 * ___marketInventoryPanel_19;
	// InventoryPanel ItemInventory::inventoryPanel
	InventoryPanel_t2677987186 * ___inventoryPanel_20;
	// ShipsController ItemInventory::shipController
	ShipsController_t970893927 * ___shipController_21;
	// ShipsEquipment ItemInventory::ship0Equipment
	ShipsEquipment_t1003200637 * ___ship0Equipment_22;
	// ShipsEquipment ItemInventory::ship1Equipment
	ShipsEquipment_t1003200637 * ___ship1Equipment_23;
	// ShipsEquipment ItemInventory::ship2Equipment
	ShipsEquipment_t1003200637 * ___ship2Equipment_24;
	// ShipsEquipment ItemInventory::ship3Equipment
	ShipsEquipment_t1003200637 * ___ship3Equipment_25;
	// UnityEngine.UI.Image ItemInventory::weaponEquipImage
	Image_t2042527209 * ___weaponEquipImage_26;
	// UnityEngine.UI.Image ItemInventory::armorEquipImage
	Image_t2042527209 * ___armorEquipImage_27;
	// UnityEngine.UI.Image ItemInventory::shieldEquipImage
	Image_t2042527209 * ___shieldEquipImage_28;
	// UnityEngine.UI.Image ItemInventory::engineEquipImage
	Image_t2042527209 * ___engineEquipImage_29;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___data_2)); }
	inline ItemDatabase_t2502586666 * get_data_2() const { return ___data_2; }
	inline ItemDatabase_t2502586666 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(ItemDatabase_t2502586666 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier(&___data_2, value);
	}

	inline static int32_t get_offset_of_shipsEquipment_3() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___shipsEquipment_3)); }
	inline List_1_t372321769 * get_shipsEquipment_3() const { return ___shipsEquipment_3; }
	inline List_1_t372321769 ** get_address_of_shipsEquipment_3() { return &___shipsEquipment_3; }
	inline void set_shipsEquipment_3(List_1_t372321769 * value)
	{
		___shipsEquipment_3 = value;
		Il2CppCodeGenWriteBarrier(&___shipsEquipment_3, value);
	}

	inline static int32_t get_offset_of_weaponItems_4() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___weaponItems_4)); }
	inline List_1_t1809589323 * get_weaponItems_4() const { return ___weaponItems_4; }
	inline List_1_t1809589323 ** get_address_of_weaponItems_4() { return &___weaponItems_4; }
	inline void set_weaponItems_4(List_1_t1809589323 * value)
	{
		___weaponItems_4 = value;
		Il2CppCodeGenWriteBarrier(&___weaponItems_4, value);
	}

	inline static int32_t get_offset_of_armorItems_5() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___armorItems_5)); }
	inline List_1_t1809589323 * get_armorItems_5() const { return ___armorItems_5; }
	inline List_1_t1809589323 ** get_address_of_armorItems_5() { return &___armorItems_5; }
	inline void set_armorItems_5(List_1_t1809589323 * value)
	{
		___armorItems_5 = value;
		Il2CppCodeGenWriteBarrier(&___armorItems_5, value);
	}

	inline static int32_t get_offset_of_shieldItems_6() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___shieldItems_6)); }
	inline List_1_t1809589323 * get_shieldItems_6() const { return ___shieldItems_6; }
	inline List_1_t1809589323 ** get_address_of_shieldItems_6() { return &___shieldItems_6; }
	inline void set_shieldItems_6(List_1_t1809589323 * value)
	{
		___shieldItems_6 = value;
		Il2CppCodeGenWriteBarrier(&___shieldItems_6, value);
	}

	inline static int32_t get_offset_of_engineItems_7() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___engineItems_7)); }
	inline List_1_t1809589323 * get_engineItems_7() const { return ___engineItems_7; }
	inline List_1_t1809589323 ** get_address_of_engineItems_7() { return &___engineItems_7; }
	inline void set_engineItems_7(List_1_t1809589323 * value)
	{
		___engineItems_7 = value;
		Il2CppCodeGenWriteBarrier(&___engineItems_7, value);
	}

	inline static int32_t get_offset_of_inventoryByID_8() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___inventoryByID_8)); }
	inline List_1_t1440998580 * get_inventoryByID_8() const { return ___inventoryByID_8; }
	inline List_1_t1440998580 ** get_address_of_inventoryByID_8() { return &___inventoryByID_8; }
	inline void set_inventoryByID_8(List_1_t1440998580 * value)
	{
		___inventoryByID_8 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryByID_8, value);
	}

	inline static int32_t get_offset_of_weaponsSlotMenu_9() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___weaponsSlotMenu_9)); }
	inline GameObject_t1756533147 * get_weaponsSlotMenu_9() const { return ___weaponsSlotMenu_9; }
	inline GameObject_t1756533147 ** get_address_of_weaponsSlotMenu_9() { return &___weaponsSlotMenu_9; }
	inline void set_weaponsSlotMenu_9(GameObject_t1756533147 * value)
	{
		___weaponsSlotMenu_9 = value;
		Il2CppCodeGenWriteBarrier(&___weaponsSlotMenu_9, value);
	}

	inline static int32_t get_offset_of_armorSlotMenu_10() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___armorSlotMenu_10)); }
	inline GameObject_t1756533147 * get_armorSlotMenu_10() const { return ___armorSlotMenu_10; }
	inline GameObject_t1756533147 ** get_address_of_armorSlotMenu_10() { return &___armorSlotMenu_10; }
	inline void set_armorSlotMenu_10(GameObject_t1756533147 * value)
	{
		___armorSlotMenu_10 = value;
		Il2CppCodeGenWriteBarrier(&___armorSlotMenu_10, value);
	}

	inline static int32_t get_offset_of_shieldSlotMenu_11() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___shieldSlotMenu_11)); }
	inline GameObject_t1756533147 * get_shieldSlotMenu_11() const { return ___shieldSlotMenu_11; }
	inline GameObject_t1756533147 ** get_address_of_shieldSlotMenu_11() { return &___shieldSlotMenu_11; }
	inline void set_shieldSlotMenu_11(GameObject_t1756533147 * value)
	{
		___shieldSlotMenu_11 = value;
		Il2CppCodeGenWriteBarrier(&___shieldSlotMenu_11, value);
	}

	inline static int32_t get_offset_of_engineSlotMenu_12() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___engineSlotMenu_12)); }
	inline GameObject_t1756533147 * get_engineSlotMenu_12() const { return ___engineSlotMenu_12; }
	inline GameObject_t1756533147 ** get_address_of_engineSlotMenu_12() { return &___engineSlotMenu_12; }
	inline void set_engineSlotMenu_12(GameObject_t1756533147 * value)
	{
		___engineSlotMenu_12 = value;
		Il2CppCodeGenWriteBarrier(&___engineSlotMenu_12, value);
	}

	inline static int32_t get_offset_of_weaponHolder_13() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___weaponHolder_13)); }
	inline GameObject_t1756533147 * get_weaponHolder_13() const { return ___weaponHolder_13; }
	inline GameObject_t1756533147 ** get_address_of_weaponHolder_13() { return &___weaponHolder_13; }
	inline void set_weaponHolder_13(GameObject_t1756533147 * value)
	{
		___weaponHolder_13 = value;
		Il2CppCodeGenWriteBarrier(&___weaponHolder_13, value);
	}

	inline static int32_t get_offset_of_armorHolder_14() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___armorHolder_14)); }
	inline GameObject_t1756533147 * get_armorHolder_14() const { return ___armorHolder_14; }
	inline GameObject_t1756533147 ** get_address_of_armorHolder_14() { return &___armorHolder_14; }
	inline void set_armorHolder_14(GameObject_t1756533147 * value)
	{
		___armorHolder_14 = value;
		Il2CppCodeGenWriteBarrier(&___armorHolder_14, value);
	}

	inline static int32_t get_offset_of_shieldHolder_15() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___shieldHolder_15)); }
	inline GameObject_t1756533147 * get_shieldHolder_15() const { return ___shieldHolder_15; }
	inline GameObject_t1756533147 ** get_address_of_shieldHolder_15() { return &___shieldHolder_15; }
	inline void set_shieldHolder_15(GameObject_t1756533147 * value)
	{
		___shieldHolder_15 = value;
		Il2CppCodeGenWriteBarrier(&___shieldHolder_15, value);
	}

	inline static int32_t get_offset_of_engineHolder_16() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___engineHolder_16)); }
	inline GameObject_t1756533147 * get_engineHolder_16() const { return ___engineHolder_16; }
	inline GameObject_t1756533147 ** get_address_of_engineHolder_16() { return &___engineHolder_16; }
	inline void set_engineHolder_16(GameObject_t1756533147 * value)
	{
		___engineHolder_16 = value;
		Il2CppCodeGenWriteBarrier(&___engineHolder_16, value);
	}

	inline static int32_t get_offset_of_inventoryPanelBuy_17() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___inventoryPanelBuy_17)); }
	inline GameObject_t1756533147 * get_inventoryPanelBuy_17() const { return ___inventoryPanelBuy_17; }
	inline GameObject_t1756533147 ** get_address_of_inventoryPanelBuy_17() { return &___inventoryPanelBuy_17; }
	inline void set_inventoryPanelBuy_17(GameObject_t1756533147 * value)
	{
		___inventoryPanelBuy_17 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryPanelBuy_17, value);
	}

	inline static int32_t get_offset_of_inventoryPanelEquip_18() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___inventoryPanelEquip_18)); }
	inline GameObject_t1756533147 * get_inventoryPanelEquip_18() const { return ___inventoryPanelEquip_18; }
	inline GameObject_t1756533147 ** get_address_of_inventoryPanelEquip_18() { return &___inventoryPanelEquip_18; }
	inline void set_inventoryPanelEquip_18(GameObject_t1756533147 * value)
	{
		___inventoryPanelEquip_18 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryPanelEquip_18, value);
	}

	inline static int32_t get_offset_of_marketInventoryPanel_19() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___marketInventoryPanel_19)); }
	inline InventoryPanel_t2677987186 * get_marketInventoryPanel_19() const { return ___marketInventoryPanel_19; }
	inline InventoryPanel_t2677987186 ** get_address_of_marketInventoryPanel_19() { return &___marketInventoryPanel_19; }
	inline void set_marketInventoryPanel_19(InventoryPanel_t2677987186 * value)
	{
		___marketInventoryPanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___marketInventoryPanel_19, value);
	}

	inline static int32_t get_offset_of_inventoryPanel_20() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___inventoryPanel_20)); }
	inline InventoryPanel_t2677987186 * get_inventoryPanel_20() const { return ___inventoryPanel_20; }
	inline InventoryPanel_t2677987186 ** get_address_of_inventoryPanel_20() { return &___inventoryPanel_20; }
	inline void set_inventoryPanel_20(InventoryPanel_t2677987186 * value)
	{
		___inventoryPanel_20 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryPanel_20, value);
	}

	inline static int32_t get_offset_of_shipController_21() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___shipController_21)); }
	inline ShipsController_t970893927 * get_shipController_21() const { return ___shipController_21; }
	inline ShipsController_t970893927 ** get_address_of_shipController_21() { return &___shipController_21; }
	inline void set_shipController_21(ShipsController_t970893927 * value)
	{
		___shipController_21 = value;
		Il2CppCodeGenWriteBarrier(&___shipController_21, value);
	}

	inline static int32_t get_offset_of_ship0Equipment_22() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___ship0Equipment_22)); }
	inline ShipsEquipment_t1003200637 * get_ship0Equipment_22() const { return ___ship0Equipment_22; }
	inline ShipsEquipment_t1003200637 ** get_address_of_ship0Equipment_22() { return &___ship0Equipment_22; }
	inline void set_ship0Equipment_22(ShipsEquipment_t1003200637 * value)
	{
		___ship0Equipment_22 = value;
		Il2CppCodeGenWriteBarrier(&___ship0Equipment_22, value);
	}

	inline static int32_t get_offset_of_ship1Equipment_23() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___ship1Equipment_23)); }
	inline ShipsEquipment_t1003200637 * get_ship1Equipment_23() const { return ___ship1Equipment_23; }
	inline ShipsEquipment_t1003200637 ** get_address_of_ship1Equipment_23() { return &___ship1Equipment_23; }
	inline void set_ship1Equipment_23(ShipsEquipment_t1003200637 * value)
	{
		___ship1Equipment_23 = value;
		Il2CppCodeGenWriteBarrier(&___ship1Equipment_23, value);
	}

	inline static int32_t get_offset_of_ship2Equipment_24() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___ship2Equipment_24)); }
	inline ShipsEquipment_t1003200637 * get_ship2Equipment_24() const { return ___ship2Equipment_24; }
	inline ShipsEquipment_t1003200637 ** get_address_of_ship2Equipment_24() { return &___ship2Equipment_24; }
	inline void set_ship2Equipment_24(ShipsEquipment_t1003200637 * value)
	{
		___ship2Equipment_24 = value;
		Il2CppCodeGenWriteBarrier(&___ship2Equipment_24, value);
	}

	inline static int32_t get_offset_of_ship3Equipment_25() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___ship3Equipment_25)); }
	inline ShipsEquipment_t1003200637 * get_ship3Equipment_25() const { return ___ship3Equipment_25; }
	inline ShipsEquipment_t1003200637 ** get_address_of_ship3Equipment_25() { return &___ship3Equipment_25; }
	inline void set_ship3Equipment_25(ShipsEquipment_t1003200637 * value)
	{
		___ship3Equipment_25 = value;
		Il2CppCodeGenWriteBarrier(&___ship3Equipment_25, value);
	}

	inline static int32_t get_offset_of_weaponEquipImage_26() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___weaponEquipImage_26)); }
	inline Image_t2042527209 * get_weaponEquipImage_26() const { return ___weaponEquipImage_26; }
	inline Image_t2042527209 ** get_address_of_weaponEquipImage_26() { return &___weaponEquipImage_26; }
	inline void set_weaponEquipImage_26(Image_t2042527209 * value)
	{
		___weaponEquipImage_26 = value;
		Il2CppCodeGenWriteBarrier(&___weaponEquipImage_26, value);
	}

	inline static int32_t get_offset_of_armorEquipImage_27() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___armorEquipImage_27)); }
	inline Image_t2042527209 * get_armorEquipImage_27() const { return ___armorEquipImage_27; }
	inline Image_t2042527209 ** get_address_of_armorEquipImage_27() { return &___armorEquipImage_27; }
	inline void set_armorEquipImage_27(Image_t2042527209 * value)
	{
		___armorEquipImage_27 = value;
		Il2CppCodeGenWriteBarrier(&___armorEquipImage_27, value);
	}

	inline static int32_t get_offset_of_shieldEquipImage_28() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___shieldEquipImage_28)); }
	inline Image_t2042527209 * get_shieldEquipImage_28() const { return ___shieldEquipImage_28; }
	inline Image_t2042527209 ** get_address_of_shieldEquipImage_28() { return &___shieldEquipImage_28; }
	inline void set_shieldEquipImage_28(Image_t2042527209 * value)
	{
		___shieldEquipImage_28 = value;
		Il2CppCodeGenWriteBarrier(&___shieldEquipImage_28, value);
	}

	inline static int32_t get_offset_of_engineEquipImage_29() { return static_cast<int32_t>(offsetof(ItemInventory_t997429261, ___engineEquipImage_29)); }
	inline Image_t2042527209 * get_engineEquipImage_29() const { return ___engineEquipImage_29; }
	inline Image_t2042527209 ** get_address_of_engineEquipImage_29() { return &___engineEquipImage_29; }
	inline void set_engineEquipImage_29(Image_t2042527209 * value)
	{
		___engineEquipImage_29 = value;
		Il2CppCodeGenWriteBarrier(&___engineEquipImage_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
