﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShipStats
struct ShipStats_t3127462681;

#include "codegen/il2cpp-codegen.h"

// System.Void ShipStats::.ctor()
extern "C"  void ShipStats__ctor_m1488302020 (ShipStats_t3127462681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipStats::Start()
extern "C"  void ShipStats_Start_m1131029172 (ShipStats_t3127462681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
