﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShipsController
struct ShipsController_t970893927;

#include "codegen/il2cpp-codegen.h"

// System.Void ShipsController::.ctor()
extern "C"  void ShipsController__ctor_m1752701194 (ShipsController_t970893927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsController::Start()
extern "C"  void ShipsController_Start_m462316142 (ShipsController_t970893927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsController::Next()
extern "C"  void ShipsController_Next_m1809562893 (ShipsController_t970893927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsController::Previous()
extern "C"  void ShipsController_Previous_m1367434359 (ShipsController_t970893927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsController::SwapShips()
extern "C"  void ShipsController_SwapShips_m2468613320 (ShipsController_t970893927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsController::CheckPurchaseStatus()
extern "C"  void ShipsController_CheckPurchaseStatus_m2042212127 (ShipsController_t970893927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsController::SendActivShipID()
extern "C"  void ShipsController_SendActivShipID_m3212050452 (ShipsController_t970893927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
