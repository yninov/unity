﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AbilityData
struct AbilityData_t479751090;

#include "codegen/il2cpp-codegen.h"

// System.Void AbilityData::.ctor()
extern "C"  void AbilityData__ctor_m3308524385 (AbilityData_t479751090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AbilityData::CheckShipId()
extern "C"  void AbilityData_CheckShipId_m1615972874 (AbilityData_t479751090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
