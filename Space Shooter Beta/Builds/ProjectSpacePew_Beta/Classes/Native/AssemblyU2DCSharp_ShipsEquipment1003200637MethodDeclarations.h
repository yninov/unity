﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShipsEquipment
struct ShipsEquipment_t1003200637;

#include "codegen/il2cpp-codegen.h"

// System.Void ShipsEquipment::.ctor()
extern "C"  void ShipsEquipment__ctor_m2190693100 (ShipsEquipment_t1003200637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsEquipment::Start()
extern "C"  void ShipsEquipment_Start_m1113167804 (ShipsEquipment_t1003200637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsEquipment::SetEqupedItems()
extern "C"  void ShipsEquipment_SetEqupedItems_m3820658024 (ShipsEquipment_t1003200637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsEquipment::SetEquipmentIDs()
extern "C"  void ShipsEquipment_SetEquipmentIDs_m3042426158 (ShipsEquipment_t1003200637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
