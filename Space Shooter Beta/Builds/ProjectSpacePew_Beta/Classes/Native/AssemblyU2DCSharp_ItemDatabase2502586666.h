﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Item>
struct List_1_t1809589323;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemDatabase
struct  ItemDatabase_t2502586666  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Item> ItemDatabase::weaponsDB
	List_1_t1809589323 * ___weaponsDB_2;
	// System.Collections.Generic.List`1<Item> ItemDatabase::armorsDB
	List_1_t1809589323 * ___armorsDB_3;
	// System.Collections.Generic.List`1<Item> ItemDatabase::shieldsDB
	List_1_t1809589323 * ___shieldsDB_4;
	// System.Collections.Generic.List`1<Item> ItemDatabase::enginesDB
	List_1_t1809589323 * ___enginesDB_5;

public:
	inline static int32_t get_offset_of_weaponsDB_2() { return static_cast<int32_t>(offsetof(ItemDatabase_t2502586666, ___weaponsDB_2)); }
	inline List_1_t1809589323 * get_weaponsDB_2() const { return ___weaponsDB_2; }
	inline List_1_t1809589323 ** get_address_of_weaponsDB_2() { return &___weaponsDB_2; }
	inline void set_weaponsDB_2(List_1_t1809589323 * value)
	{
		___weaponsDB_2 = value;
		Il2CppCodeGenWriteBarrier(&___weaponsDB_2, value);
	}

	inline static int32_t get_offset_of_armorsDB_3() { return static_cast<int32_t>(offsetof(ItemDatabase_t2502586666, ___armorsDB_3)); }
	inline List_1_t1809589323 * get_armorsDB_3() const { return ___armorsDB_3; }
	inline List_1_t1809589323 ** get_address_of_armorsDB_3() { return &___armorsDB_3; }
	inline void set_armorsDB_3(List_1_t1809589323 * value)
	{
		___armorsDB_3 = value;
		Il2CppCodeGenWriteBarrier(&___armorsDB_3, value);
	}

	inline static int32_t get_offset_of_shieldsDB_4() { return static_cast<int32_t>(offsetof(ItemDatabase_t2502586666, ___shieldsDB_4)); }
	inline List_1_t1809589323 * get_shieldsDB_4() const { return ___shieldsDB_4; }
	inline List_1_t1809589323 ** get_address_of_shieldsDB_4() { return &___shieldsDB_4; }
	inline void set_shieldsDB_4(List_1_t1809589323 * value)
	{
		___shieldsDB_4 = value;
		Il2CppCodeGenWriteBarrier(&___shieldsDB_4, value);
	}

	inline static int32_t get_offset_of_enginesDB_5() { return static_cast<int32_t>(offsetof(ItemDatabase_t2502586666, ___enginesDB_5)); }
	inline List_1_t1809589323 * get_enginesDB_5() const { return ___enginesDB_5; }
	inline List_1_t1809589323 ** get_address_of_enginesDB_5() { return &___enginesDB_5; }
	inline void set_enginesDB_5(List_1_t1809589323 * value)
	{
		___enginesDB_5 = value;
		Il2CppCodeGenWriteBarrier(&___enginesDB_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
