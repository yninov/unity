﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_CreditsUpdater2579388263.h"
#include "AssemblyU2DCSharp_GameController3607102586.h"
#include "AssemblyU2DCSharp_InventoryPanel2677987186.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "AssemblyU2DCSharp_Item_WeaponType3894335344.h"
#include "AssemblyU2DCSharp_Item_ItemType1150357105.h"
#include "AssemblyU2DCSharp_Item_ItemRarity3672222668.h"
#include "AssemblyU2DCSharp_ItemDatabase2502586666.h"
#include "AssemblyU2DCSharp_ItemInventory997429261.h"
#include "AssemblyU2DCSharp_PlayerShip2635532215.h"
#include "AssemblyU2DCSharp_ShipStats3127462681.h"
#include "AssemblyU2DCSharp_ShipsController970893927.h"
#include "AssemblyU2DCSharp_ShipsDatabase1101834110.h"
#include "AssemblyU2DCSharp_ShipsEquipment1003200637.h"
#include "AssemblyU2DCSharp_AbilityData479751090.h"
#include "AssemblyU2DCSharp_MainMenu4009084430.h"
#include "AssemblyU2DCSharp_Marketplace506058393.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1704[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (CreditsUpdater_t2579388263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[1] = 
{
	CreditsUpdater_t2579388263::get_offset_of_creditsText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (GameController_t3607102586), -1, sizeof(GameController_t3607102586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1708[4] = 
{
	GameController_t3607102586_StaticFields::get_offset_of_current_2(),
	GameController_t3607102586::get_offset_of_shipID_3(),
	GameController_t3607102586::get_offset_of_credits_4(),
	GameController_t3607102586::get_offset_of_abilityTokes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (InventoryPanel_t2677987186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[11] = 
{
	InventoryPanel_t2677987186::get_offset_of_itemDatabase_2(),
	InventoryPanel_t2677987186::get_offset_of_data_3(),
	InventoryPanel_t2677987186::get_offset_of_mainMenu_4(),
	InventoryPanel_t2677987186::get_offset_of_itemInventory_5(),
	InventoryPanel_t2677987186::get_offset_of_menuScript_6(),
	InventoryPanel_t2677987186::get_offset_of_itemIdText_7(),
	InventoryPanel_t2677987186::get_offset_of_itemNameText_8(),
	InventoryPanel_t2677987186::get_offset_of_itemBonusText_9(),
	InventoryPanel_t2677987186::get_offset_of_itemTypeText_10(),
	InventoryPanel_t2677987186::get_offset_of_itemImageSprite_11(),
	InventoryPanel_t2677987186::get_offset_of_itemAdded_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (Item_t2440468191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[17] = 
{
	Item_t2440468191::get_offset_of_itemID_2(),
	Item_t2440468191::get_offset_of_itemName_3(),
	Item_t2440468191::get_offset_of_itemPrice_4(),
	Item_t2440468191::get_offset_of_armor_5(),
	Item_t2440468191::get_offset_of_shield_6(),
	Item_t2440468191::get_offset_of_shieldRegen_7(),
	Item_t2440468191::get_offset_of_speed_8(),
	Item_t2440468191::get_offset_of_fireRate_9(),
	Item_t2440468191::get_offset_of_shieldDamage_10(),
	Item_t2440468191::get_offset_of_armorDamage_11(),
	Item_t2440468191::get_offset_of_itemImage_12(),
	Item_t2440468191::get_offset_of_itemVisual_13(),
	Item_t2440468191::get_offset_of_itemPurchased_14(),
	Item_t2440468191::get_offset_of_weaponType_15(),
	Item_t2440468191::get_offset_of_itemType_16(),
	Item_t2440468191::get_offset_of_itemRarity_17(),
	Item_t2440468191::get_offset_of_itemShipType_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (WeaponType_t3894335344)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1711[9] = 
{
	WeaponType_t3894335344::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (ItemType_t1150357105)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1712[5] = 
{
	ItemType_t1150357105::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (ItemRarity_t3672222668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1713[6] = 
{
	ItemRarity_t3672222668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (ItemDatabase_t2502586666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1714[4] = 
{
	ItemDatabase_t2502586666::get_offset_of_weaponsDB_2(),
	ItemDatabase_t2502586666::get_offset_of_armorsDB_3(),
	ItemDatabase_t2502586666::get_offset_of_shieldsDB_4(),
	ItemDatabase_t2502586666::get_offset_of_enginesDB_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (ItemInventory_t997429261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[28] = 
{
	ItemInventory_t997429261::get_offset_of_data_2(),
	ItemInventory_t997429261::get_offset_of_shipsEquipment_3(),
	ItemInventory_t997429261::get_offset_of_weaponItems_4(),
	ItemInventory_t997429261::get_offset_of_armorItems_5(),
	ItemInventory_t997429261::get_offset_of_shieldItems_6(),
	ItemInventory_t997429261::get_offset_of_engineItems_7(),
	ItemInventory_t997429261::get_offset_of_inventoryByID_8(),
	ItemInventory_t997429261::get_offset_of_weaponsSlotMenu_9(),
	ItemInventory_t997429261::get_offset_of_armorSlotMenu_10(),
	ItemInventory_t997429261::get_offset_of_shieldSlotMenu_11(),
	ItemInventory_t997429261::get_offset_of_engineSlotMenu_12(),
	ItemInventory_t997429261::get_offset_of_weaponHolder_13(),
	ItemInventory_t997429261::get_offset_of_armorHolder_14(),
	ItemInventory_t997429261::get_offset_of_shieldHolder_15(),
	ItemInventory_t997429261::get_offset_of_engineHolder_16(),
	ItemInventory_t997429261::get_offset_of_inventoryPanelBuy_17(),
	ItemInventory_t997429261::get_offset_of_inventoryPanelEquip_18(),
	ItemInventory_t997429261::get_offset_of_marketInventoryPanel_19(),
	ItemInventory_t997429261::get_offset_of_inventoryPanel_20(),
	ItemInventory_t997429261::get_offset_of_shipController_21(),
	ItemInventory_t997429261::get_offset_of_ship0Equipment_22(),
	ItemInventory_t997429261::get_offset_of_ship1Equipment_23(),
	ItemInventory_t997429261::get_offset_of_ship2Equipment_24(),
	ItemInventory_t997429261::get_offset_of_ship3Equipment_25(),
	ItemInventory_t997429261::get_offset_of_weaponEquipImage_26(),
	ItemInventory_t997429261::get_offset_of_armorEquipImage_27(),
	ItemInventory_t997429261::get_offset_of_shieldEquipImage_28(),
	ItemInventory_t997429261::get_offset_of_engineEquipImage_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (PlayerShip_t2635532215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[13] = 
{
	PlayerShip_t2635532215::get_offset_of_shipID_0(),
	PlayerShip_t2635532215::get_offset_of_shipName_1(),
	PlayerShip_t2635532215::get_offset_of_shipMaxArmor_2(),
	PlayerShip_t2635532215::get_offset_of_shipRegenArmor_3(),
	PlayerShip_t2635532215::get_offset_of_shipMaxShield_4(),
	PlayerShip_t2635532215::get_offset_of_shipRegenShield_5(),
	PlayerShip_t2635532215::get_offset_of_shipSpeed_6(),
	PlayerShip_t2635532215::get_offset_of_shipFireRate_7(),
	PlayerShip_t2635532215::get_offset_of_armorDamage_8(),
	PlayerShip_t2635532215::get_offset_of_shieldDamage_9(),
	PlayerShip_t2635532215::get_offset_of_purchasedShip_10(),
	PlayerShip_t2635532215::get_offset_of_shipPrice_11(),
	PlayerShip_t2635532215::get_offset_of_shipType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (ShipStats_t3127462681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[3] = 
{
	ShipStats_t3127462681::get_offset_of_shipID_2(),
	ShipStats_t3127462681::get_offset_of_ship_3(),
	ShipStats_t3127462681::get_offset_of_ships_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (ShipsController_t970893927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[13] = 
{
	ShipsController_t970893927::get_offset_of_ships_2(),
	ShipsController_t970893927::get_offset_of_allShips_3(),
	ShipsController_t970893927::get_offset_of_purchasedShips_4(),
	ShipsController_t970893927::get_offset_of_shipAdded0_5(),
	ShipsController_t970893927::get_offset_of_shipAdded1_6(),
	ShipsController_t970893927::get_offset_of_shipAdded2_7(),
	ShipsController_t970893927::get_offset_of_shipAdded3_8(),
	ShipsController_t970893927::get_offset_of_ship0_9(),
	ShipsController_t970893927::get_offset_of_ship1_10(),
	ShipsController_t970893927::get_offset_of_ship2_11(),
	ShipsController_t970893927::get_offset_of_ship3_12(),
	ShipsController_t970893927::get_offset_of_shipRotationNumber_13(),
	ShipsController_t970893927::get_offset_of_itemInventory_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (ShipsDatabase_t1101834110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[2] = 
{
	ShipsDatabase_t1101834110::get_offset_of_inventory_2(),
	ShipsDatabase_t1101834110::get_offset_of_dataBase_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (ShipsEquipment_t1003200637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[7] = 
{
	ShipsEquipment_t1003200637::get_offset_of_data_2(),
	ShipsEquipment_t1003200637::get_offset_of_shipID_3(),
	ShipsEquipment_t1003200637::get_offset_of_equipment_4(),
	ShipsEquipment_t1003200637::get_offset_of_weapon_5(),
	ShipsEquipment_t1003200637::get_offset_of_armor_6(),
	ShipsEquipment_t1003200637::get_offset_of_shield_7(),
	ShipsEquipment_t1003200637::get_offset_of_engine_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (AbilityData_t479751090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[9] = 
{
	AbilityData_t479751090::get_offset_of_shipMaxArmor_2(),
	AbilityData_t479751090::get_offset_of_shipRegenArmor_3(),
	AbilityData_t479751090::get_offset_of_shipMaxShield_4(),
	AbilityData_t479751090::get_offset_of_shipRegenShield_5(),
	AbilityData_t479751090::get_offset_of_shipSpeed_6(),
	AbilityData_t479751090::get_offset_of_shipFireRate_7(),
	AbilityData_t479751090::get_offset_of_shipDamage_8(),
	AbilityData_t479751090::get_offset_of_abilityName_9(),
	AbilityData_t479751090::get_offset_of_abilityLevel_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (MainMenu_t4009084430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[15] = 
{
	MainMenu_t4009084430::get_offset_of_inventoryMenu_2(),
	MainMenu_t4009084430::get_offset_of_abilityMenu_3(),
	MainMenu_t4009084430::get_offset_of_missionMenu_4(),
	MainMenu_t4009084430::get_offset_of_marketPlaceMenu_5(),
	MainMenu_t4009084430::get_offset_of_weaponInventoryMenu_6(),
	MainMenu_t4009084430::get_offset_of_shieldInventoryMenu_7(),
	MainMenu_t4009084430::get_offset_of_armorInventoryMenu_8(),
	MainMenu_t4009084430::get_offset_of_engineInventoryMenu_9(),
	MainMenu_t4009084430::get_offset_of_shipsSlotMenu_10(),
	MainMenu_t4009084430::get_offset_of_weaponsSlotMenu_11(),
	MainMenu_t4009084430::get_offset_of_armorSlotMenu_12(),
	MainMenu_t4009084430::get_offset_of_shieldSlotMenu_13(),
	MainMenu_t4009084430::get_offset_of_enginesSlotMenu_14(),
	MainMenu_t4009084430::get_offset_of_camInventoryAnim_15(),
	MainMenu_t4009084430::get_offset_of_inventoryAnim_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (Marketplace_t506058393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[6] = 
{
	Marketplace_t506058393::get_offset_of_ships_2(),
	Marketplace_t506058393::get_offset_of_shipController_3(),
	Marketplace_t506058393::get_offset_of_shipNumber_4(),
	Marketplace_t506058393::get_offset_of_shipName_5(),
	Marketplace_t506058393::get_offset_of_price_6(),
	Marketplace_t506058393::get_offset_of_id_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
