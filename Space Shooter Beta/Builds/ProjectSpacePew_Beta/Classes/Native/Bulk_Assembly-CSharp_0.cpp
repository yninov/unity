﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AbilityData
struct AbilityData_t479751090;
// CreditsUpdater
struct CreditsUpdater_t2579388263;
// GameController
struct GameController_t3607102586;
// InventoryPanel
struct InventoryPanel_t2677987186;
// ItemDatabase
struct ItemDatabase_t2502586666;
// System.Object
struct Il2CppObject;
// ItemInventory
struct ItemInventory_t997429261;
// MainMenu
struct MainMenu_t4009084430;
// Item
struct Item_t2440468191;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// ShipsEquipment
struct ShipsEquipment_t1003200637;
// Marketplace
struct Marketplace_t506058393;
// ShipsDatabase
struct ShipsDatabase_t1101834110;
// PlayerShip
struct PlayerShip_t2635532215;
// System.String
struct String_t;
// ShipsController
struct ShipsController_t970893927;
// ShipStats
struct ShipStats_t3127462681;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_AbilityData479751090.h"
#include "AssemblyU2DCSharp_AbilityData479751090MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameController3607102586.h"
#include "AssemblyU2DCSharp_GameController3607102586MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_CreditsUpdater2579388263.h"
#include "AssemblyU2DCSharp_CreditsUpdater2579388263MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_InventoryPanel2677987186.h"
#include "AssemblyU2DCSharp_InventoryPanel2677987186MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "AssemblyU2DCSharp_ItemDatabase2502586666.h"
#include "AssemblyU2DCSharp_ItemInventory997429261.h"
#include "AssemblyU2DCSharp_MainMenu4009084430.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209MethodDeclarations.h"
#include "AssemblyU2DCSharp_Item_ItemType1150357105.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1809589323MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1809589323.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Collections_Generic_List_1_gen372321769MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2004653347MethodDeclarations.h"
#include "AssemblyU2DCSharp_ItemInventory997429261MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShipsEquipment1003200637MethodDeclarations.h"
#include "AssemblyU2DCSharp_MainMenu4009084430MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen372321769.h"
#include "AssemblyU2DCSharp_ShipsEquipment1003200637.h"
#include "AssemblyU2DCSharp_ShipsController970893927.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2004653347.h"
#include "AssemblyU2DCSharp_PlayerShip2635532215.h"
#include "AssemblyU2DCSharp_Item2440468191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470MethodDeclarations.h"
#include "AssemblyU2DCSharp_Item_ItemRarity3672222668.h"
#include "AssemblyU2DCSharp_Item_ItemRarity3672222668MethodDeclarations.h"
#include "AssemblyU2DCSharp_Item_ItemType1150357105MethodDeclarations.h"
#include "AssemblyU2DCSharp_Item_WeaponType3894335344.h"
#include "AssemblyU2DCSharp_Item_WeaponType3894335344MethodDeclarations.h"
#include "AssemblyU2DCSharp_ItemDatabase2502586666MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "AssemblyU2DCSharp_Marketplace506058393.h"
#include "AssemblyU2DCSharp_Marketplace506058393MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShipsDatabase1101834110.h"
#include "AssemblyU2DCSharp_ShipsController970893927MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerShip2635532215MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_ShipsDatabase1101834110MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1866979105MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1695958676MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1866979105.h"
#include "mscorlib_System_IO_FileStream1695958676.h"
#include "mscorlib_System_IO_FileMode236403845.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_Stream3255436806MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShipStats3127462681.h"
#include "AssemblyU2DCSharp_ShipStats3127462681MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ItemDatabase>()
#define GameObject_GetComponent_TisItemDatabase_t2502586666_m4220407943(__this, method) ((  ItemDatabase_t2502586666 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ItemInventory>()
#define GameObject_GetComponent_TisItemInventory_t997429261_m1673908360(__this, method) ((  ItemInventory_t997429261 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MainMenu>()
#define GameObject_GetComponent_TisMainMenu_t4009084430_m1885906887(__this, method) ((  MainMenu_t4009084430 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(__this, method) ((  RectTransform_t3349966182 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m681991875_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Transform_t3275118058 * p1, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m681991875(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m681991875_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
#define Object_Instantiate_TisGameObject_t1756533147_m3066053529(__this /* static, unused */, p0, p1, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m681991875_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<InventoryPanel>()
#define GameObject_GetComponent_TisInventoryPanel_t2677987186_m4124782355(__this, method) ((  InventoryPanel_t2677987186 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ShipsDatabase>()
#define GameObject_GetComponent_TisShipsDatabase_t1101834110_m3999776727(__this, method) ((  ShipsDatabase_t1101834110 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<ItemInventory>()
#define Component_GetComponent_TisItemInventory_t997429261_m281227978(__this, method) ((  ItemInventory_t997429261 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AbilityData::.ctor()
extern "C"  void AbilityData__ctor_m3308524385 (AbilityData_t479751090 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AbilityData::CheckShipId()
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern const uint32_t AbilityData_CheckShipId_m1615972874_MetadataUsageId;
extern "C"  void AbilityData_CheckShipId_m1615972874 (AbilityData_t479751090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AbilityData_CheckShipId_m1615972874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameController_t3607102586 * L_0 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_shipID_3();
		if (L_1)
		{
			goto IL_005c;
		}
	}
	{
		__this->set_shipMaxArmor_2((10.0f));
		__this->set_shipRegenArmor_3((1.0f));
		__this->set_shipMaxShield_4((10.0f));
		__this->set_shipRegenShield_5((1.0f));
		__this->set_shipSpeed_6((0.1f));
		__this->set_shipFireRate_7((5.0f));
		__this->set_shipDamage_8((10.0f));
	}

IL_005c:
	{
		GameController_t3607102586 * L_2 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_shipID_3();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_00b9;
		}
	}
	{
		__this->set_shipMaxArmor_2((20.0f));
		__this->set_shipRegenArmor_3((1.0f));
		__this->set_shipMaxShield_4((20.0f));
		__this->set_shipRegenShield_5((1.0f));
		__this->set_shipSpeed_6((0.2f));
		__this->set_shipFireRate_7((4.0f));
		__this->set_shipDamage_8((10.0f));
	}

IL_00b9:
	{
		GameController_t3607102586 * L_4 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_shipID_3();
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0116;
		}
	}
	{
		__this->set_shipMaxArmor_2((30.0f));
		__this->set_shipRegenArmor_3((1.0f));
		__this->set_shipMaxShield_4((30.0f));
		__this->set_shipRegenShield_5((1.0f));
		__this->set_shipSpeed_6((0.2f));
		__this->set_shipFireRate_7((5.0f));
		__this->set_shipDamage_8((15.0f));
	}

IL_0116:
	{
		GameController_t3607102586 * L_6 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_shipID_3();
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0173;
		}
	}
	{
		__this->set_shipMaxArmor_2((40.0f));
		__this->set_shipRegenArmor_3((1.0f));
		__this->set_shipMaxShield_4((40.0f));
		__this->set_shipRegenShield_5((1.0f));
		__this->set_shipSpeed_6((0.2f));
		__this->set_shipFireRate_7((6.0f));
		__this->set_shipDamage_8((20.0f));
	}

IL_0173:
	{
		return;
	}
}
// System.Void CreditsUpdater::.ctor()
extern "C"  void CreditsUpdater__ctor_m4210419418 (CreditsUpdater_t2579388263 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreditsUpdater::Update()
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3082634090;
extern const uint32_t CreditsUpdater_Update_m536369203_MetadataUsageId;
extern "C"  void CreditsUpdater_Update_m536369203 (CreditsUpdater_t2579388263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CreditsUpdater_Update_m536369203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_creditsText_2();
		GameController_t3607102586 * L_1 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_credits_4();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3082634090, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		return;
	}
}
// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m1439649957 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Awake()
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t GameController_Awake_m2066391706_MetadataUsageId;
extern "C"  void GameController_Awake_m2066391706 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Awake_m2066391706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameController_t3607102586 * L_0 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->set_current_2(__this);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_0041;
	}

IL_0026:
	{
		GameController_t3607102586 * L_3 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, __this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void InventoryPanel::.ctor()
extern "C"  void InventoryPanel__ctor_m1511447747 (InventoryPanel_t2677987186 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InventoryPanel::Start()
extern const MethodInfo* GameObject_GetComponent_TisItemDatabase_t2502586666_m4220407943_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisItemInventory_t997429261_m1673908360_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMainMenu_t4009084430_m1885906887_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3230665184;
extern Il2CppCodeGenString* _stringLiteral781750174;
extern const uint32_t InventoryPanel_Start_m3640293759_MetadataUsageId;
extern "C"  void InventoryPanel_Start_m3640293759 (InventoryPanel_t2677987186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InventoryPanel_Start_m3640293759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3230665184, /*hidden argument*/NULL);
		__this->set_itemDatabase_2(L_0);
		GameObject_t1756533147 * L_1 = __this->get_itemDatabase_2();
		NullCheck(L_1);
		ItemDatabase_t2502586666 * L_2 = GameObject_GetComponent_TisItemDatabase_t2502586666_m4220407943(L_1, /*hidden argument*/GameObject_GetComponent_TisItemDatabase_t2502586666_m4220407943_MethodInfo_var);
		__this->set_data_3(L_2);
		GameObject_t1756533147 * L_3 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral781750174, /*hidden argument*/NULL);
		__this->set_mainMenu_4(L_3);
		GameObject_t1756533147 * L_4 = __this->get_mainMenu_4();
		NullCheck(L_4);
		ItemInventory_t997429261 * L_5 = GameObject_GetComponent_TisItemInventory_t997429261_m1673908360(L_4, /*hidden argument*/GameObject_GetComponent_TisItemInventory_t997429261_m1673908360_MethodInfo_var);
		__this->set_itemInventory_5(L_5);
		GameObject_t1756533147 * L_6 = __this->get_mainMenu_4();
		NullCheck(L_6);
		MainMenu_t4009084430 * L_7 = GameObject_GetComponent_TisMainMenu_t4009084430_m1885906887(L_6, /*hidden argument*/GameObject_GetComponent_TisMainMenu_t4009084430_m1885906887_MethodInfo_var);
		__this->set_menuScript_6(L_7);
		return;
	}
}
// System.Void InventoryPanel::AddItem(Item)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3162791963;
extern Il2CppCodeGenString* _stringLiteral2391892769;
extern Il2CppCodeGenString* _stringLiteral1639365253;
extern Il2CppCodeGenString* _stringLiteral82728150;
extern Il2CppCodeGenString* _stringLiteral942770690;
extern Il2CppCodeGenString* _stringLiteral2523454328;
extern Il2CppCodeGenString* _stringLiteral2819796620;
extern Il2CppCodeGenString* _stringLiteral1397312244;
extern Il2CppCodeGenString* _stringLiteral174278442;
extern Il2CppCodeGenString* _stringLiteral4216314792;
extern const uint32_t InventoryPanel_AddItem_m2777303932_MetadataUsageId;
extern "C"  void InventoryPanel_AddItem_m2777303932 (InventoryPanel_t2677987186 * __this, Item_t2440468191 * ___itemToAdd0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InventoryPanel_AddItem_m2777303932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Item_t2440468191 * L_0 = ___itemToAdd0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_itemType_16();
		if (L_1)
		{
			goto IL_00d2;
		}
	}
	{
		Text_t356221433 * L_2 = __this->get_itemIdText_7();
		Item_t2440468191 * L_3 = ___itemToAdd0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_itemID_2();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3162791963, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_7);
		Image_t2042527209 * L_8 = __this->get_itemImageSprite_11();
		Item_t2440468191 * L_9 = ___itemToAdd0;
		NullCheck(L_9);
		Sprite_t309593783 * L_10 = L_9->get_itemImage_12();
		NullCheck(L_8);
		Image_set_sprite_m1800056820(L_8, L_10, /*hidden argument*/NULL);
		Text_t356221433 * L_11 = __this->get_itemNameText_8();
		Item_t2440468191 * L_12 = ___itemToAdd0;
		NullCheck(L_12);
		String_t* L_13 = L_12->get_itemName_3();
		String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2391892769, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_14);
		Text_t356221433 * L_15 = __this->get_itemBonusText_9();
		ObjectU5BU5D_t3614634134* L_16 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral1639365253);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1639365253);
		ObjectU5BU5D_t3614634134* L_17 = L_16;
		Item_t2440468191 * L_18 = ___itemToAdd0;
		NullCheck(L_18);
		float L_19 = L_18->get_fireRate_9();
		float L_20 = L_19;
		Il2CppObject * L_21 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_21);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_21);
		ObjectU5BU5D_t3614634134* L_22 = L_17;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral82728150);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral82728150);
		ObjectU5BU5D_t3614634134* L_23 = L_22;
		Item_t2440468191 * L_24 = ___itemToAdd0;
		NullCheck(L_24);
		float L_25 = L_24->get_shieldDamage_10();
		float L_26 = L_25;
		Il2CppObject * L_27 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_27);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_27);
		ObjectU5BU5D_t3614634134* L_28 = L_23;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral942770690);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral942770690);
		ObjectU5BU5D_t3614634134* L_29 = L_28;
		Item_t2440468191 * L_30 = ___itemToAdd0;
		NullCheck(L_30);
		float L_31 = L_30->get_armorDamage_11();
		float L_32 = L_31;
		Il2CppObject * L_33 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_33);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_33);
		String_t* L_34 = String_Concat_m3881798623(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_15, L_34);
		Text_t356221433 * L_35 = __this->get_itemTypeText_10();
		Item_t2440468191 * L_36 = ___itemToAdd0;
		NullCheck(L_36);
		String_t* L_37 = L_36->get_itemShipType_18();
		String_t* L_38 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2523454328, L_37, /*hidden argument*/NULL);
		NullCheck(L_35);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_35, L_38);
		Item_t2440468191 * L_39 = ___itemToAdd0;
		__this->set_itemAdded_12(L_39);
		return;
	}

IL_00d2:
	{
		Item_t2440468191 * L_40 = ___itemToAdd0;
		NullCheck(L_40);
		int32_t L_41 = L_40->get_itemType_16();
		if ((!(((uint32_t)L_41) == ((uint32_t)2))))
		{
			goto IL_016d;
		}
	}
	{
		Text_t356221433 * L_42 = __this->get_itemIdText_7();
		Item_t2440468191 * L_43 = ___itemToAdd0;
		NullCheck(L_43);
		int32_t L_44 = L_43->get_itemID_2();
		int32_t L_45 = L_44;
		Il2CppObject * L_46 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_45);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3162791963, L_46, /*hidden argument*/NULL);
		NullCheck(L_42);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_42, L_47);
		Image_t2042527209 * L_48 = __this->get_itemImageSprite_11();
		Item_t2440468191 * L_49 = ___itemToAdd0;
		NullCheck(L_49);
		Sprite_t309593783 * L_50 = L_49->get_itemImage_12();
		NullCheck(L_48);
		Image_set_sprite_m1800056820(L_48, L_50, /*hidden argument*/NULL);
		Text_t356221433 * L_51 = __this->get_itemNameText_8();
		Item_t2440468191 * L_52 = ___itemToAdd0;
		NullCheck(L_52);
		String_t* L_53 = L_52->get_itemName_3();
		String_t* L_54 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2391892769, L_53, /*hidden argument*/NULL);
		NullCheck(L_51);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_51, L_54);
		Text_t356221433 * L_55 = __this->get_itemBonusText_9();
		Item_t2440468191 * L_56 = ___itemToAdd0;
		NullCheck(L_56);
		float L_57 = L_56->get_armor_5();
		float L_58 = L_57;
		Il2CppObject * L_59 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_58);
		String_t* L_60 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2819796620, L_59, /*hidden argument*/NULL);
		NullCheck(L_55);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_55, L_60);
		Text_t356221433 * L_61 = __this->get_itemTypeText_10();
		Item_t2440468191 * L_62 = ___itemToAdd0;
		NullCheck(L_62);
		String_t* L_63 = L_62->get_itemShipType_18();
		String_t* L_64 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2523454328, L_63, /*hidden argument*/NULL);
		NullCheck(L_61);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_61, L_64);
		Item_t2440468191 * L_65 = ___itemToAdd0;
		__this->set_itemAdded_12(L_65);
		return;
	}

IL_016d:
	{
		Item_t2440468191 * L_66 = ___itemToAdd0;
		NullCheck(L_66);
		int32_t L_67 = L_66->get_itemType_16();
		if ((!(((uint32_t)L_67) == ((uint32_t)1))))
		{
			goto IL_022a;
		}
	}
	{
		Text_t356221433 * L_68 = __this->get_itemIdText_7();
		Item_t2440468191 * L_69 = ___itemToAdd0;
		NullCheck(L_69);
		int32_t L_70 = L_69->get_itemID_2();
		int32_t L_71 = L_70;
		Il2CppObject * L_72 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_71);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_73 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3162791963, L_72, /*hidden argument*/NULL);
		NullCheck(L_68);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_68, L_73);
		Image_t2042527209 * L_74 = __this->get_itemImageSprite_11();
		Item_t2440468191 * L_75 = ___itemToAdd0;
		NullCheck(L_75);
		Sprite_t309593783 * L_76 = L_75->get_itemImage_12();
		NullCheck(L_74);
		Image_set_sprite_m1800056820(L_74, L_76, /*hidden argument*/NULL);
		Text_t356221433 * L_77 = __this->get_itemNameText_8();
		Item_t2440468191 * L_78 = ___itemToAdd0;
		NullCheck(L_78);
		String_t* L_79 = L_78->get_itemName_3();
		String_t* L_80 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2391892769, L_79, /*hidden argument*/NULL);
		NullCheck(L_77);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_77, L_80);
		Text_t356221433 * L_81 = __this->get_itemBonusText_9();
		ObjectU5BU5D_t3614634134* L_82 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_82);
		ArrayElementTypeCheck (L_82, _stringLiteral1397312244);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1397312244);
		ObjectU5BU5D_t3614634134* L_83 = L_82;
		Item_t2440468191 * L_84 = ___itemToAdd0;
		NullCheck(L_84);
		float L_85 = L_84->get_shield_6();
		float L_86 = L_85;
		Il2CppObject * L_87 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_86);
		NullCheck(L_83);
		ArrayElementTypeCheck (L_83, L_87);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_87);
		ObjectU5BU5D_t3614634134* L_88 = L_83;
		NullCheck(L_88);
		ArrayElementTypeCheck (L_88, _stringLiteral174278442);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral174278442);
		ObjectU5BU5D_t3614634134* L_89 = L_88;
		Item_t2440468191 * L_90 = ___itemToAdd0;
		NullCheck(L_90);
		float L_91 = L_90->get_shieldRegen_7();
		float L_92 = L_91;
		Il2CppObject * L_93 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_92);
		NullCheck(L_89);
		ArrayElementTypeCheck (L_89, L_93);
		(L_89)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_93);
		String_t* L_94 = String_Concat_m3881798623(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		NullCheck(L_81);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_81, L_94);
		Text_t356221433 * L_95 = __this->get_itemTypeText_10();
		Item_t2440468191 * L_96 = ___itemToAdd0;
		NullCheck(L_96);
		String_t* L_97 = L_96->get_itemShipType_18();
		String_t* L_98 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2523454328, L_97, /*hidden argument*/NULL);
		NullCheck(L_95);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_95, L_98);
		Item_t2440468191 * L_99 = ___itemToAdd0;
		__this->set_itemAdded_12(L_99);
		return;
	}

IL_022a:
	{
		Item_t2440468191 * L_100 = ___itemToAdd0;
		NullCheck(L_100);
		int32_t L_101 = L_100->get_itemType_16();
		if ((!(((uint32_t)L_101) == ((uint32_t)3))))
		{
			goto IL_02c5;
		}
	}
	{
		Text_t356221433 * L_102 = __this->get_itemIdText_7();
		Item_t2440468191 * L_103 = ___itemToAdd0;
		NullCheck(L_103);
		int32_t L_104 = L_103->get_itemID_2();
		int32_t L_105 = L_104;
		Il2CppObject * L_106 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_105);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_107 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3162791963, L_106, /*hidden argument*/NULL);
		NullCheck(L_102);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_102, L_107);
		Image_t2042527209 * L_108 = __this->get_itemImageSprite_11();
		Item_t2440468191 * L_109 = ___itemToAdd0;
		NullCheck(L_109);
		Sprite_t309593783 * L_110 = L_109->get_itemImage_12();
		NullCheck(L_108);
		Image_set_sprite_m1800056820(L_108, L_110, /*hidden argument*/NULL);
		Text_t356221433 * L_111 = __this->get_itemNameText_8();
		Item_t2440468191 * L_112 = ___itemToAdd0;
		NullCheck(L_112);
		String_t* L_113 = L_112->get_itemName_3();
		String_t* L_114 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2391892769, L_113, /*hidden argument*/NULL);
		NullCheck(L_111);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_111, L_114);
		Text_t356221433 * L_115 = __this->get_itemBonusText_9();
		Item_t2440468191 * L_116 = ___itemToAdd0;
		NullCheck(L_116);
		float L_117 = L_116->get_speed_8();
		float L_118 = L_117;
		Il2CppObject * L_119 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_118);
		String_t* L_120 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral4216314792, L_119, /*hidden argument*/NULL);
		NullCheck(L_115);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_115, L_120);
		Text_t356221433 * L_121 = __this->get_itemTypeText_10();
		Item_t2440468191 * L_122 = ___itemToAdd0;
		NullCheck(L_122);
		String_t* L_123 = L_122->get_itemShipType_18();
		String_t* L_124 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2523454328, L_123, /*hidden argument*/NULL);
		NullCheck(L_121);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_121, L_124);
		Item_t2440468191 * L_125 = ___itemToAdd0;
		__this->set_itemAdded_12(L_125);
		return;
	}

IL_02c5:
	{
		return;
	}
}
// System.Void InventoryPanel::BuyFromMarket()
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const uint32_t InventoryPanel_BuyFromMarket_m2715282707_MetadataUsageId;
extern "C"  void InventoryPanel_BuyFromMarket_m2715282707 (InventoryPanel_t2677987186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InventoryPanel_BuyFromMarket_m2715282707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	int32_t V_2 = 0;
	GameObject_t1756533147 * V_3 = NULL;
	int32_t V_4 = 0;
	GameObject_t1756533147 * V_5 = NULL;
	int32_t V_6 = 0;
	GameObject_t1756533147 * V_7 = NULL;
	{
		GameController_t3607102586 * L_0 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_credits_4();
		Item_t2440468191 * L_2 = __this->get_itemAdded_12();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_itemPrice_4();
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_0502;
		}
	}
	{
		GameController_t3607102586 * L_4 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		GameController_t3607102586 * L_5 = L_4;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_credits_4();
		Item_t2440468191 * L_7 = __this->get_itemAdded_12();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_itemPrice_4();
		NullCheck(L_5);
		L_5->set_credits_4(((int32_t)((int32_t)L_6-(int32_t)L_8)));
		V_0 = 0;
		goto IL_014f;
	}

IL_003d:
	{
		ItemDatabase_t2502586666 * L_9 = __this->get_data_3();
		NullCheck(L_9);
		List_1_t1809589323 * L_10 = L_9->get_weaponsDB_2();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		Item_t2440468191 * L_12 = List_1_get_Item_m4027977911(L_10, L_11, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_12);
		int32_t L_13 = L_12->get_itemID_2();
		Item_t2440468191 * L_14 = __this->get_itemAdded_12();
		NullCheck(L_14);
		int32_t L_15 = L_14->get_itemID_2();
		if ((!(((uint32_t)L_13) == ((uint32_t)L_15))))
		{
			goto IL_014b;
		}
	}
	{
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_SetActive_m2887581199(L_16, (bool)0, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_17 = __this->get_itemInventory_5();
		NullCheck(L_17);
		InventoryPanel_t2677987186 * L_18 = L_17->get_inventoryPanel_20();
		Item_t2440468191 * L_19 = __this->get_itemAdded_12();
		NullCheck(L_18);
		InventoryPanel_AddItem_m2777303932(L_18, L_19, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_20 = __this->get_itemInventory_5();
		NullCheck(L_20);
		GameObject_t1756533147 * L_21 = L_20->get_weaponsSlotMenu_9();
		NullCheck(L_21);
		RectTransform_t3349966182 * L_22 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_21, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_23 = L_22;
		NullCheck(L_23);
		Vector2_t2243707579  L_24 = RectTransform_get_sizeDelta_m2157326342(L_23, /*hidden argument*/NULL);
		Vector2_t2243707579  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector2__ctor_m3067419446(&L_25, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_26 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		RectTransform_set_sizeDelta_m2319668137(L_23, L_26, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_27 = __this->get_itemInventory_5();
		NullCheck(L_27);
		GameObject_t1756533147 * L_28 = L_27->get_weaponHolder_13();
		NullCheck(L_28);
		RectTransform_t3349966182 * L_29 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_28, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_30 = L_29;
		NullCheck(L_30);
		Vector2_t2243707579  L_31 = RectTransform_get_sizeDelta_m2157326342(L_30, /*hidden argument*/NULL);
		Vector2_t2243707579  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Vector2__ctor_m3067419446(&L_32, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_33 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_sizeDelta_m2319668137(L_30, L_33, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_34 = __this->get_itemInventory_5();
		NullCheck(L_34);
		GameObject_t1756533147 * L_35 = L_34->get_inventoryPanelEquip_18();
		ItemInventory_t997429261 * L_36 = __this->get_itemInventory_5();
		NullCheck(L_36);
		GameObject_t1756533147 * L_37 = L_36->get_weaponHolder_13();
		NullCheck(L_37);
		Transform_t3275118058 * L_38 = GameObject_get_transform_m909382139(L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_39 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_35, L_38, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_1 = L_39;
		GameObject_t1756533147 * L_40 = V_1;
		NullCheck(L_40);
		Transform_t3275118058 * L_41 = GameObject_get_transform_m909382139(L_40, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_42 = __this->get_itemInventory_5();
		NullCheck(L_42);
		GameObject_t1756533147 * L_43 = L_42->get_weaponHolder_13();
		NullCheck(L_43);
		Transform_t3275118058 * L_44 = GameObject_get_transform_m909382139(L_43, /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_SetParent_m4124909910(L_41, L_44, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_45 = __this->get_itemInventory_5();
		NullCheck(L_45);
		List_1_t1440998580 * L_46 = L_45->get_inventoryByID_8();
		ItemInventory_t997429261 * L_47 = __this->get_itemInventory_5();
		NullCheck(L_47);
		ItemDatabase_t2502586666 * L_48 = L_47->get_data_2();
		NullCheck(L_48);
		List_1_t1809589323 * L_49 = L_48->get_weaponsDB_2();
		int32_t L_50 = V_0;
		NullCheck(L_49);
		Item_t2440468191 * L_51 = List_1_get_Item_m4027977911(L_49, L_50, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_51);
		int32_t L_52 = L_51->get_itemID_2();
		NullCheck(L_46);
		List_1_Add_m2828939739(L_46, L_52, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		return;
	}

IL_014b:
	{
		int32_t L_53 = V_0;
		V_0 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_014f:
	{
		int32_t L_54 = V_0;
		ItemDatabase_t2502586666 * L_55 = __this->get_data_3();
		NullCheck(L_55);
		List_1_t1809589323 * L_56 = L_55->get_weaponsDB_2();
		NullCheck(L_56);
		int32_t L_57 = List_1_get_Count_m2506740058(L_56, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_54) < ((int32_t)L_57)))
		{
			goto IL_003d;
		}
	}
	{
		V_2 = 0;
		goto IL_027e;
	}

IL_016c:
	{
		ItemDatabase_t2502586666 * L_58 = __this->get_data_3();
		NullCheck(L_58);
		List_1_t1809589323 * L_59 = L_58->get_armorsDB_3();
		int32_t L_60 = V_2;
		NullCheck(L_59);
		Item_t2440468191 * L_61 = List_1_get_Item_m4027977911(L_59, L_60, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_61);
		int32_t L_62 = L_61->get_itemID_2();
		Item_t2440468191 * L_63 = __this->get_itemAdded_12();
		NullCheck(L_63);
		int32_t L_64 = L_63->get_itemID_2();
		if ((!(((uint32_t)L_62) == ((uint32_t)L_64))))
		{
			goto IL_027a;
		}
	}
	{
		GameObject_t1756533147 * L_65 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_65);
		GameObject_SetActive_m2887581199(L_65, (bool)0, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_66 = __this->get_itemInventory_5();
		NullCheck(L_66);
		InventoryPanel_t2677987186 * L_67 = L_66->get_inventoryPanel_20();
		Item_t2440468191 * L_68 = __this->get_itemAdded_12();
		NullCheck(L_67);
		InventoryPanel_AddItem_m2777303932(L_67, L_68, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_69 = __this->get_itemInventory_5();
		NullCheck(L_69);
		GameObject_t1756533147 * L_70 = L_69->get_armorSlotMenu_10();
		NullCheck(L_70);
		RectTransform_t3349966182 * L_71 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_70, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_72 = L_71;
		NullCheck(L_72);
		Vector2_t2243707579  L_73 = RectTransform_get_sizeDelta_m2157326342(L_72, /*hidden argument*/NULL);
		Vector2_t2243707579  L_74;
		memset(&L_74, 0, sizeof(L_74));
		Vector2__ctor_m3067419446(&L_74, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_75 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_73, L_74, /*hidden argument*/NULL);
		NullCheck(L_72);
		RectTransform_set_sizeDelta_m2319668137(L_72, L_75, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_76 = __this->get_itemInventory_5();
		NullCheck(L_76);
		GameObject_t1756533147 * L_77 = L_76->get_armorHolder_14();
		NullCheck(L_77);
		RectTransform_t3349966182 * L_78 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_77, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_79 = L_78;
		NullCheck(L_79);
		Vector2_t2243707579  L_80 = RectTransform_get_sizeDelta_m2157326342(L_79, /*hidden argument*/NULL);
		Vector2_t2243707579  L_81;
		memset(&L_81, 0, sizeof(L_81));
		Vector2__ctor_m3067419446(&L_81, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_82 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_80, L_81, /*hidden argument*/NULL);
		NullCheck(L_79);
		RectTransform_set_sizeDelta_m2319668137(L_79, L_82, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_83 = __this->get_itemInventory_5();
		NullCheck(L_83);
		GameObject_t1756533147 * L_84 = L_83->get_inventoryPanelEquip_18();
		ItemInventory_t997429261 * L_85 = __this->get_itemInventory_5();
		NullCheck(L_85);
		GameObject_t1756533147 * L_86 = L_85->get_armorHolder_14();
		NullCheck(L_86);
		Transform_t3275118058 * L_87 = GameObject_get_transform_m909382139(L_86, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_88 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_84, L_87, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_3 = L_88;
		GameObject_t1756533147 * L_89 = V_3;
		NullCheck(L_89);
		Transform_t3275118058 * L_90 = GameObject_get_transform_m909382139(L_89, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_91 = __this->get_itemInventory_5();
		NullCheck(L_91);
		GameObject_t1756533147 * L_92 = L_91->get_armorHolder_14();
		NullCheck(L_92);
		Transform_t3275118058 * L_93 = GameObject_get_transform_m909382139(L_92, /*hidden argument*/NULL);
		NullCheck(L_90);
		Transform_SetParent_m4124909910(L_90, L_93, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_94 = __this->get_itemInventory_5();
		NullCheck(L_94);
		List_1_t1440998580 * L_95 = L_94->get_inventoryByID_8();
		ItemInventory_t997429261 * L_96 = __this->get_itemInventory_5();
		NullCheck(L_96);
		ItemDatabase_t2502586666 * L_97 = L_96->get_data_2();
		NullCheck(L_97);
		List_1_t1809589323 * L_98 = L_97->get_armorsDB_3();
		int32_t L_99 = V_2;
		NullCheck(L_98);
		Item_t2440468191 * L_100 = List_1_get_Item_m4027977911(L_98, L_99, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_100);
		int32_t L_101 = L_100->get_itemID_2();
		NullCheck(L_95);
		List_1_Add_m2828939739(L_95, L_101, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		return;
	}

IL_027a:
	{
		int32_t L_102 = V_2;
		V_2 = ((int32_t)((int32_t)L_102+(int32_t)1));
	}

IL_027e:
	{
		int32_t L_103 = V_2;
		ItemDatabase_t2502586666 * L_104 = __this->get_data_3();
		NullCheck(L_104);
		List_1_t1809589323 * L_105 = L_104->get_armorsDB_3();
		NullCheck(L_105);
		int32_t L_106 = List_1_get_Count_m2506740058(L_105, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_103) < ((int32_t)L_106)))
		{
			goto IL_016c;
		}
	}
	{
		V_4 = 0;
		goto IL_03b4;
	}

IL_029c:
	{
		ItemDatabase_t2502586666 * L_107 = __this->get_data_3();
		NullCheck(L_107);
		List_1_t1809589323 * L_108 = L_107->get_shieldsDB_4();
		int32_t L_109 = V_4;
		NullCheck(L_108);
		Item_t2440468191 * L_110 = List_1_get_Item_m4027977911(L_108, L_109, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_110);
		int32_t L_111 = L_110->get_itemID_2();
		Item_t2440468191 * L_112 = __this->get_itemAdded_12();
		NullCheck(L_112);
		int32_t L_113 = L_112->get_itemID_2();
		if ((!(((uint32_t)L_111) == ((uint32_t)L_113))))
		{
			goto IL_03ae;
		}
	}
	{
		GameObject_t1756533147 * L_114 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_114);
		GameObject_SetActive_m2887581199(L_114, (bool)0, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_115 = __this->get_itemInventory_5();
		NullCheck(L_115);
		InventoryPanel_t2677987186 * L_116 = L_115->get_inventoryPanel_20();
		Item_t2440468191 * L_117 = __this->get_itemAdded_12();
		NullCheck(L_116);
		InventoryPanel_AddItem_m2777303932(L_116, L_117, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_118 = __this->get_itemInventory_5();
		NullCheck(L_118);
		GameObject_t1756533147 * L_119 = L_118->get_shieldSlotMenu_11();
		NullCheck(L_119);
		RectTransform_t3349966182 * L_120 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_119, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_121 = L_120;
		NullCheck(L_121);
		Vector2_t2243707579  L_122 = RectTransform_get_sizeDelta_m2157326342(L_121, /*hidden argument*/NULL);
		Vector2_t2243707579  L_123;
		memset(&L_123, 0, sizeof(L_123));
		Vector2__ctor_m3067419446(&L_123, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_124 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_122, L_123, /*hidden argument*/NULL);
		NullCheck(L_121);
		RectTransform_set_sizeDelta_m2319668137(L_121, L_124, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_125 = __this->get_itemInventory_5();
		NullCheck(L_125);
		GameObject_t1756533147 * L_126 = L_125->get_shieldHolder_15();
		NullCheck(L_126);
		RectTransform_t3349966182 * L_127 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_126, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_128 = L_127;
		NullCheck(L_128);
		Vector2_t2243707579  L_129 = RectTransform_get_sizeDelta_m2157326342(L_128, /*hidden argument*/NULL);
		Vector2_t2243707579  L_130;
		memset(&L_130, 0, sizeof(L_130));
		Vector2__ctor_m3067419446(&L_130, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_131 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_129, L_130, /*hidden argument*/NULL);
		NullCheck(L_128);
		RectTransform_set_sizeDelta_m2319668137(L_128, L_131, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_132 = __this->get_itemInventory_5();
		NullCheck(L_132);
		GameObject_t1756533147 * L_133 = L_132->get_inventoryPanelEquip_18();
		ItemInventory_t997429261 * L_134 = __this->get_itemInventory_5();
		NullCheck(L_134);
		GameObject_t1756533147 * L_135 = L_134->get_shieldHolder_15();
		NullCheck(L_135);
		Transform_t3275118058 * L_136 = GameObject_get_transform_m909382139(L_135, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_137 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_133, L_136, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_5 = L_137;
		GameObject_t1756533147 * L_138 = V_5;
		NullCheck(L_138);
		Transform_t3275118058 * L_139 = GameObject_get_transform_m909382139(L_138, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_140 = __this->get_itemInventory_5();
		NullCheck(L_140);
		GameObject_t1756533147 * L_141 = L_140->get_shieldHolder_15();
		NullCheck(L_141);
		Transform_t3275118058 * L_142 = GameObject_get_transform_m909382139(L_141, /*hidden argument*/NULL);
		NullCheck(L_139);
		Transform_SetParent_m4124909910(L_139, L_142, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_143 = __this->get_itemInventory_5();
		NullCheck(L_143);
		List_1_t1440998580 * L_144 = L_143->get_inventoryByID_8();
		ItemInventory_t997429261 * L_145 = __this->get_itemInventory_5();
		NullCheck(L_145);
		ItemDatabase_t2502586666 * L_146 = L_145->get_data_2();
		NullCheck(L_146);
		List_1_t1809589323 * L_147 = L_146->get_shieldsDB_4();
		int32_t L_148 = V_4;
		NullCheck(L_147);
		Item_t2440468191 * L_149 = List_1_get_Item_m4027977911(L_147, L_148, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_149);
		int32_t L_150 = L_149->get_itemID_2();
		NullCheck(L_144);
		List_1_Add_m2828939739(L_144, L_150, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		return;
	}

IL_03ae:
	{
		int32_t L_151 = V_4;
		V_4 = ((int32_t)((int32_t)L_151+(int32_t)1));
	}

IL_03b4:
	{
		int32_t L_152 = V_4;
		ItemDatabase_t2502586666 * L_153 = __this->get_data_3();
		NullCheck(L_153);
		List_1_t1809589323 * L_154 = L_153->get_shieldsDB_4();
		NullCheck(L_154);
		int32_t L_155 = List_1_get_Count_m2506740058(L_154, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_152) < ((int32_t)L_155)))
		{
			goto IL_029c;
		}
	}
	{
		V_6 = 0;
		goto IL_04eb;
	}

IL_03d3:
	{
		ItemDatabase_t2502586666 * L_156 = __this->get_data_3();
		NullCheck(L_156);
		List_1_t1809589323 * L_157 = L_156->get_enginesDB_5();
		int32_t L_158 = V_6;
		NullCheck(L_157);
		Item_t2440468191 * L_159 = List_1_get_Item_m4027977911(L_157, L_158, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_159);
		int32_t L_160 = L_159->get_itemID_2();
		Item_t2440468191 * L_161 = __this->get_itemAdded_12();
		NullCheck(L_161);
		int32_t L_162 = L_161->get_itemID_2();
		if ((!(((uint32_t)L_160) == ((uint32_t)L_162))))
		{
			goto IL_04e5;
		}
	}
	{
		GameObject_t1756533147 * L_163 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_163);
		GameObject_SetActive_m2887581199(L_163, (bool)0, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_164 = __this->get_itemInventory_5();
		NullCheck(L_164);
		InventoryPanel_t2677987186 * L_165 = L_164->get_inventoryPanel_20();
		Item_t2440468191 * L_166 = __this->get_itemAdded_12();
		NullCheck(L_165);
		InventoryPanel_AddItem_m2777303932(L_165, L_166, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_167 = __this->get_itemInventory_5();
		NullCheck(L_167);
		GameObject_t1756533147 * L_168 = L_167->get_engineSlotMenu_12();
		NullCheck(L_168);
		RectTransform_t3349966182 * L_169 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_168, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_170 = L_169;
		NullCheck(L_170);
		Vector2_t2243707579  L_171 = RectTransform_get_sizeDelta_m2157326342(L_170, /*hidden argument*/NULL);
		Vector2_t2243707579  L_172;
		memset(&L_172, 0, sizeof(L_172));
		Vector2__ctor_m3067419446(&L_172, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_173 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_171, L_172, /*hidden argument*/NULL);
		NullCheck(L_170);
		RectTransform_set_sizeDelta_m2319668137(L_170, L_173, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_174 = __this->get_itemInventory_5();
		NullCheck(L_174);
		GameObject_t1756533147 * L_175 = L_174->get_engineHolder_16();
		NullCheck(L_175);
		RectTransform_t3349966182 * L_176 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_175, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_177 = L_176;
		NullCheck(L_177);
		Vector2_t2243707579  L_178 = RectTransform_get_sizeDelta_m2157326342(L_177, /*hidden argument*/NULL);
		Vector2_t2243707579  L_179;
		memset(&L_179, 0, sizeof(L_179));
		Vector2__ctor_m3067419446(&L_179, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_180 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_178, L_179, /*hidden argument*/NULL);
		NullCheck(L_177);
		RectTransform_set_sizeDelta_m2319668137(L_177, L_180, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_181 = __this->get_itemInventory_5();
		NullCheck(L_181);
		GameObject_t1756533147 * L_182 = L_181->get_inventoryPanelEquip_18();
		ItemInventory_t997429261 * L_183 = __this->get_itemInventory_5();
		NullCheck(L_183);
		GameObject_t1756533147 * L_184 = L_183->get_engineHolder_16();
		NullCheck(L_184);
		Transform_t3275118058 * L_185 = GameObject_get_transform_m909382139(L_184, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_186 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_182, L_185, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_7 = L_186;
		GameObject_t1756533147 * L_187 = V_7;
		NullCheck(L_187);
		Transform_t3275118058 * L_188 = GameObject_get_transform_m909382139(L_187, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_189 = __this->get_itemInventory_5();
		NullCheck(L_189);
		GameObject_t1756533147 * L_190 = L_189->get_engineHolder_16();
		NullCheck(L_190);
		Transform_t3275118058 * L_191 = GameObject_get_transform_m909382139(L_190, /*hidden argument*/NULL);
		NullCheck(L_188);
		Transform_SetParent_m4124909910(L_188, L_191, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_192 = __this->get_itemInventory_5();
		NullCheck(L_192);
		List_1_t1440998580 * L_193 = L_192->get_inventoryByID_8();
		ItemInventory_t997429261 * L_194 = __this->get_itemInventory_5();
		NullCheck(L_194);
		ItemDatabase_t2502586666 * L_195 = L_194->get_data_2();
		NullCheck(L_195);
		List_1_t1809589323 * L_196 = L_195->get_enginesDB_5();
		int32_t L_197 = V_6;
		NullCheck(L_196);
		Item_t2440468191 * L_198 = List_1_get_Item_m4027977911(L_196, L_197, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_198);
		int32_t L_199 = L_198->get_itemID_2();
		NullCheck(L_193);
		List_1_Add_m2828939739(L_193, L_199, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		return;
	}

IL_04e5:
	{
		int32_t L_200 = V_6;
		V_6 = ((int32_t)((int32_t)L_200+(int32_t)1));
	}

IL_04eb:
	{
		int32_t L_201 = V_6;
		ItemDatabase_t2502586666 * L_202 = __this->get_data_3();
		NullCheck(L_202);
		List_1_t1809589323 * L_203 = L_202->get_enginesDB_5();
		NullCheck(L_203);
		int32_t L_204 = List_1_get_Count_m2506740058(L_203, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_201) < ((int32_t)L_204)))
		{
			goto IL_03d3;
		}
	}

IL_0502:
	{
		return;
	}
}
// System.Void InventoryPanel::SellItemFromInventory()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m2710652734_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m852068579_MethodInfo_var;
extern const uint32_t InventoryPanel_SellItemFromInventory_m1399147720_MetadataUsageId;
extern "C"  void InventoryPanel_SellItemFromInventory_m1399147720 (InventoryPanel_t2677987186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InventoryPanel_SellItemFromInventory_m1399147720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Item_t2440468191 * L_1 = __this->get_itemAdded_12();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_itemType_16();
		if (L_2)
		{
			goto IL_00bb;
		}
	}
	{
		ItemInventory_t997429261 * L_3 = __this->get_itemInventory_5();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = L_3->get_weaponHolder_13();
		NullCheck(L_4);
		RectTransform_t3349966182 * L_5 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_4, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_6 = L_5;
		NullCheck(L_6);
		Vector2_t2243707579  L_7 = RectTransform_get_sizeDelta_m2157326342(L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3067419446(&L_8, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectTransform_set_sizeDelta_m2319668137(L_6, L_9, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_00a5;
	}

IL_0051:
	{
		ItemInventory_t997429261 * L_10 = __this->get_itemInventory_5();
		NullCheck(L_10);
		List_1_t1440998580 * L_11 = L_10->get_inventoryByID_8();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = List_1_get_Item_m1921196075(L_11, L_12, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_14 = __this->get_itemAdded_12();
		NullCheck(L_14);
		int32_t L_15 = L_14->get_itemID_2();
		if ((!(((uint32_t)L_13) == ((uint32_t)L_15))))
		{
			goto IL_00a1;
		}
	}
	{
		ItemInventory_t997429261 * L_16 = __this->get_itemInventory_5();
		NullCheck(L_16);
		List_1_t1440998580 * L_17 = L_16->get_inventoryByID_8();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		List_1_RemoveAt_m2710652734(L_17, L_18, /*hidden argument*/List_1_RemoveAt_m2710652734_MethodInfo_var);
		GameController_t3607102586 * L_19 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		GameController_t3607102586 * L_20 = L_19;
		NullCheck(L_20);
		int32_t L_21 = L_20->get_credits_4();
		Item_t2440468191 * L_22 = __this->get_itemAdded_12();
		NullCheck(L_22);
		int32_t L_23 = L_22->get_itemPrice_4();
		NullCheck(L_20);
		L_20->set_credits_4(((int32_t)((int32_t)L_21+(int32_t)((int32_t)((int32_t)L_23/(int32_t)2)))));
	}

IL_00a1:
	{
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_0;
		ItemInventory_t997429261 * L_26 = __this->get_itemInventory_5();
		NullCheck(L_26);
		List_1_t1440998580 * L_27 = L_26->get_inventoryByID_8();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m852068579(L_27, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_25) < ((int32_t)L_28)))
		{
			goto IL_0051;
		}
	}

IL_00bb:
	{
		Item_t2440468191 * L_29 = __this->get_itemAdded_12();
		NullCheck(L_29);
		int32_t L_30 = L_29->get_itemType_16();
		if ((!(((uint32_t)L_30) == ((uint32_t)2))))
		{
			goto IL_016c;
		}
	}
	{
		ItemInventory_t997429261 * L_31 = __this->get_itemInventory_5();
		NullCheck(L_31);
		GameObject_t1756533147 * L_32 = L_31->get_armorHolder_14();
		NullCheck(L_32);
		RectTransform_t3349966182 * L_33 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_32, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_34 = L_33;
		NullCheck(L_34);
		Vector2_t2243707579  L_35 = RectTransform_get_sizeDelta_m2157326342(L_34, /*hidden argument*/NULL);
		Vector2_t2243707579  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector2__ctor_m3067419446(&L_36, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_37 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		RectTransform_set_sizeDelta_m2319668137(L_34, L_37, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_0156;
	}

IL_0102:
	{
		ItemInventory_t997429261 * L_38 = __this->get_itemInventory_5();
		NullCheck(L_38);
		List_1_t1440998580 * L_39 = L_38->get_inventoryByID_8();
		int32_t L_40 = V_1;
		NullCheck(L_39);
		int32_t L_41 = List_1_get_Item_m1921196075(L_39, L_40, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_42 = __this->get_itemAdded_12();
		NullCheck(L_42);
		int32_t L_43 = L_42->get_itemID_2();
		if ((!(((uint32_t)L_41) == ((uint32_t)L_43))))
		{
			goto IL_0152;
		}
	}
	{
		ItemInventory_t997429261 * L_44 = __this->get_itemInventory_5();
		NullCheck(L_44);
		List_1_t1440998580 * L_45 = L_44->get_inventoryByID_8();
		int32_t L_46 = V_1;
		NullCheck(L_45);
		List_1_RemoveAt_m2710652734(L_45, L_46, /*hidden argument*/List_1_RemoveAt_m2710652734_MethodInfo_var);
		GameController_t3607102586 * L_47 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		GameController_t3607102586 * L_48 = L_47;
		NullCheck(L_48);
		int32_t L_49 = L_48->get_credits_4();
		Item_t2440468191 * L_50 = __this->get_itemAdded_12();
		NullCheck(L_50);
		int32_t L_51 = L_50->get_itemPrice_4();
		NullCheck(L_48);
		L_48->set_credits_4(((int32_t)((int32_t)L_49+(int32_t)((int32_t)((int32_t)L_51/(int32_t)2)))));
	}

IL_0152:
	{
		int32_t L_52 = V_1;
		V_1 = ((int32_t)((int32_t)L_52+(int32_t)1));
	}

IL_0156:
	{
		int32_t L_53 = V_1;
		ItemInventory_t997429261 * L_54 = __this->get_itemInventory_5();
		NullCheck(L_54);
		List_1_t1440998580 * L_55 = L_54->get_inventoryByID_8();
		NullCheck(L_55);
		int32_t L_56 = List_1_get_Count_m852068579(L_55, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_53) < ((int32_t)L_56)))
		{
			goto IL_0102;
		}
	}

IL_016c:
	{
		Item_t2440468191 * L_57 = __this->get_itemAdded_12();
		NullCheck(L_57);
		int32_t L_58 = L_57->get_itemType_16();
		if ((!(((uint32_t)L_58) == ((uint32_t)1))))
		{
			goto IL_021d;
		}
	}
	{
		ItemInventory_t997429261 * L_59 = __this->get_itemInventory_5();
		NullCheck(L_59);
		GameObject_t1756533147 * L_60 = L_59->get_shieldHolder_15();
		NullCheck(L_60);
		RectTransform_t3349966182 * L_61 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_60, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_62 = L_61;
		NullCheck(L_62);
		Vector2_t2243707579  L_63 = RectTransform_get_sizeDelta_m2157326342(L_62, /*hidden argument*/NULL);
		Vector2_t2243707579  L_64;
		memset(&L_64, 0, sizeof(L_64));
		Vector2__ctor_m3067419446(&L_64, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_65 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
		NullCheck(L_62);
		RectTransform_set_sizeDelta_m2319668137(L_62, L_65, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0207;
	}

IL_01b3:
	{
		ItemInventory_t997429261 * L_66 = __this->get_itemInventory_5();
		NullCheck(L_66);
		List_1_t1440998580 * L_67 = L_66->get_inventoryByID_8();
		int32_t L_68 = V_2;
		NullCheck(L_67);
		int32_t L_69 = List_1_get_Item_m1921196075(L_67, L_68, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_70 = __this->get_itemAdded_12();
		NullCheck(L_70);
		int32_t L_71 = L_70->get_itemID_2();
		if ((!(((uint32_t)L_69) == ((uint32_t)L_71))))
		{
			goto IL_0203;
		}
	}
	{
		ItemInventory_t997429261 * L_72 = __this->get_itemInventory_5();
		NullCheck(L_72);
		List_1_t1440998580 * L_73 = L_72->get_inventoryByID_8();
		int32_t L_74 = V_2;
		NullCheck(L_73);
		List_1_RemoveAt_m2710652734(L_73, L_74, /*hidden argument*/List_1_RemoveAt_m2710652734_MethodInfo_var);
		GameController_t3607102586 * L_75 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		GameController_t3607102586 * L_76 = L_75;
		NullCheck(L_76);
		int32_t L_77 = L_76->get_credits_4();
		Item_t2440468191 * L_78 = __this->get_itemAdded_12();
		NullCheck(L_78);
		int32_t L_79 = L_78->get_itemPrice_4();
		NullCheck(L_76);
		L_76->set_credits_4(((int32_t)((int32_t)L_77+(int32_t)((int32_t)((int32_t)L_79/(int32_t)2)))));
	}

IL_0203:
	{
		int32_t L_80 = V_2;
		V_2 = ((int32_t)((int32_t)L_80+(int32_t)1));
	}

IL_0207:
	{
		int32_t L_81 = V_2;
		ItemInventory_t997429261 * L_82 = __this->get_itemInventory_5();
		NullCheck(L_82);
		List_1_t1440998580 * L_83 = L_82->get_inventoryByID_8();
		NullCheck(L_83);
		int32_t L_84 = List_1_get_Count_m852068579(L_83, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_81) < ((int32_t)L_84)))
		{
			goto IL_01b3;
		}
	}

IL_021d:
	{
		Item_t2440468191 * L_85 = __this->get_itemAdded_12();
		NullCheck(L_85);
		int32_t L_86 = L_85->get_itemType_16();
		if ((!(((uint32_t)L_86) == ((uint32_t)3))))
		{
			goto IL_02ce;
		}
	}
	{
		ItemInventory_t997429261 * L_87 = __this->get_itemInventory_5();
		NullCheck(L_87);
		GameObject_t1756533147 * L_88 = L_87->get_engineHolder_16();
		NullCheck(L_88);
		RectTransform_t3349966182 * L_89 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_88, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_90 = L_89;
		NullCheck(L_90);
		Vector2_t2243707579  L_91 = RectTransform_get_sizeDelta_m2157326342(L_90, /*hidden argument*/NULL);
		Vector2_t2243707579  L_92;
		memset(&L_92, 0, sizeof(L_92));
		Vector2__ctor_m3067419446(&L_92, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_93 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
		NullCheck(L_90);
		RectTransform_set_sizeDelta_m2319668137(L_90, L_93, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_02b8;
	}

IL_0264:
	{
		ItemInventory_t997429261 * L_94 = __this->get_itemInventory_5();
		NullCheck(L_94);
		List_1_t1440998580 * L_95 = L_94->get_inventoryByID_8();
		int32_t L_96 = V_3;
		NullCheck(L_95);
		int32_t L_97 = List_1_get_Item_m1921196075(L_95, L_96, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_98 = __this->get_itemAdded_12();
		NullCheck(L_98);
		int32_t L_99 = L_98->get_itemID_2();
		if ((!(((uint32_t)L_97) == ((uint32_t)L_99))))
		{
			goto IL_02b4;
		}
	}
	{
		ItemInventory_t997429261 * L_100 = __this->get_itemInventory_5();
		NullCheck(L_100);
		List_1_t1440998580 * L_101 = L_100->get_inventoryByID_8();
		int32_t L_102 = V_3;
		NullCheck(L_101);
		List_1_RemoveAt_m2710652734(L_101, L_102, /*hidden argument*/List_1_RemoveAt_m2710652734_MethodInfo_var);
		GameController_t3607102586 * L_103 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		GameController_t3607102586 * L_104 = L_103;
		NullCheck(L_104);
		int32_t L_105 = L_104->get_credits_4();
		Item_t2440468191 * L_106 = __this->get_itemAdded_12();
		NullCheck(L_106);
		int32_t L_107 = L_106->get_itemPrice_4();
		NullCheck(L_104);
		L_104->set_credits_4(((int32_t)((int32_t)L_105+(int32_t)((int32_t)((int32_t)L_107/(int32_t)2)))));
	}

IL_02b4:
	{
		int32_t L_108 = V_3;
		V_3 = ((int32_t)((int32_t)L_108+(int32_t)1));
	}

IL_02b8:
	{
		int32_t L_109 = V_3;
		ItemInventory_t997429261 * L_110 = __this->get_itemInventory_5();
		NullCheck(L_110);
		List_1_t1440998580 * L_111 = L_110->get_inventoryByID_8();
		NullCheck(L_111);
		int32_t L_112 = List_1_get_Count_m852068579(L_111, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_109) < ((int32_t)L_112)))
		{
			goto IL_0264;
		}
	}

IL_02ce:
	{
		return;
	}
}
// System.Void InventoryPanel::EquipItems()
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2727922973_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2773528611_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m2710652734_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m852068579_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m283942784_MethodInfo_var;
extern const uint32_t InventoryPanel_EquipItems_m3713614609_MetadataUsageId;
extern "C"  void InventoryPanel_EquipItems_m3713614609 (InventoryPanel_t2677987186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InventoryPanel_EquipItems_m3713614609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		Item_t2440468191 * L_0 = __this->get_itemAdded_12();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_itemType_16();
		if (L_1)
		{
			goto IL_01af;
		}
	}
	{
		V_0 = 0;
		goto IL_0183;
	}

IL_0017:
	{
		GameController_t3607102586 * L_2 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_shipID_3();
		ItemInventory_t997429261 * L_4 = __this->get_itemInventory_5();
		NullCheck(L_4);
		List_1_t372321769 * L_5 = L_4->get_shipsEquipment_3();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		ShipsEquipment_t1003200637 * L_7 = List_1_get_Item_m2727922973(L_5, L_6, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_7);
		int32_t L_8 = L_7->get_shipID_3();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_8))))
		{
			goto IL_017f;
		}
	}
	{
		Item_t2440468191 * L_9 = __this->get_itemAdded_12();
		NullCheck(L_9);
		String_t* L_10 = L_9->get_itemShipType_18();
		ItemInventory_t997429261 * L_11 = __this->get_itemInventory_5();
		NullCheck(L_11);
		ShipsController_t970893927 * L_12 = L_11->get_shipController_21();
		NullCheck(L_12);
		List_1_t2004653347 * L_13 = L_12->get_allShips_3();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		PlayerShip_t2635532215 * L_15 = List_1_get_Item_m2773528611(L_13, L_14, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_15);
		String_t* L_16 = L_15->get_shipType_12();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_10, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_006d;
		}
	}
	{
		return;
	}

IL_006d:
	{
		ItemInventory_t997429261 * L_18 = __this->get_itemInventory_5();
		ItemInventory_t997429261 * L_19 = __this->get_itemInventory_5();
		NullCheck(L_19);
		List_1_t372321769 * L_20 = L_19->get_shipsEquipment_3();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		ShipsEquipment_t1003200637 * L_22 = List_1_get_Item_m2727922973(L_20, L_21, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		Item_t2440468191 * L_23 = __this->get_itemAdded_12();
		NullCheck(L_18);
		ItemInventory_SwitchEquipedItems_m1895146111(L_18, L_22, L_23, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_24 = __this->get_itemInventory_5();
		NullCheck(L_24);
		List_1_t372321769 * L_25 = L_24->get_shipsEquipment_3();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		ShipsEquipment_t1003200637 * L_27 = List_1_get_Item_m2727922973(L_25, L_26, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_27);
		Item_t2440468191 * L_28 = L_27->get_weapon_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_28, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00df;
		}
	}
	{
		ItemInventory_t997429261 * L_30 = __this->get_itemInventory_5();
		NullCheck(L_30);
		GameObject_t1756533147 * L_31 = L_30->get_weaponHolder_13();
		NullCheck(L_31);
		RectTransform_t3349966182 * L_32 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_31, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_33 = L_32;
		NullCheck(L_33);
		Vector2_t2243707579  L_34 = RectTransform_get_sizeDelta_m2157326342(L_33, /*hidden argument*/NULL);
		Vector2_t2243707579  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector2__ctor_m3067419446(&L_35, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_36 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		RectTransform_set_sizeDelta_m2319668137(L_33, L_36, /*hidden argument*/NULL);
	}

IL_00df:
	{
		ItemInventory_t997429261 * L_37 = __this->get_itemInventory_5();
		NullCheck(L_37);
		List_1_t372321769 * L_38 = L_37->get_shipsEquipment_3();
		int32_t L_39 = V_0;
		NullCheck(L_38);
		ShipsEquipment_t1003200637 * L_40 = List_1_get_Item_m2727922973(L_38, L_39, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		Item_t2440468191 * L_41 = __this->get_itemAdded_12();
		NullCheck(L_40);
		L_40->set_weapon_5(L_41);
		ItemInventory_t997429261 * L_42 = __this->get_itemInventory_5();
		NullCheck(L_42);
		Image_t2042527209 * L_43 = L_42->get_weaponEquipImage_26();
		Item_t2440468191 * L_44 = __this->get_itemAdded_12();
		NullCheck(L_44);
		Sprite_t309593783 * L_45 = L_44->get_itemImage_12();
		NullCheck(L_43);
		Image_set_sprite_m1800056820(L_43, L_45, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_46 = __this->get_itemInventory_5();
		NullCheck(L_46);
		List_1_t372321769 * L_47 = L_46->get_shipsEquipment_3();
		int32_t L_48 = V_0;
		NullCheck(L_47);
		ShipsEquipment_t1003200637 * L_49 = List_1_get_Item_m2727922973(L_47, L_48, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_49);
		ShipsEquipment_SetEquipmentIDs_m3042426158(L_49, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_0169;
	}

IL_0133:
	{
		ItemInventory_t997429261 * L_50 = __this->get_itemInventory_5();
		NullCheck(L_50);
		List_1_t1440998580 * L_51 = L_50->get_inventoryByID_8();
		int32_t L_52 = V_1;
		NullCheck(L_51);
		int32_t L_53 = List_1_get_Item_m1921196075(L_51, L_52, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_54 = __this->get_itemAdded_12();
		NullCheck(L_54);
		int32_t L_55 = L_54->get_itemID_2();
		if ((!(((uint32_t)L_53) == ((uint32_t)L_55))))
		{
			goto IL_0165;
		}
	}
	{
		ItemInventory_t997429261 * L_56 = __this->get_itemInventory_5();
		NullCheck(L_56);
		List_1_t1440998580 * L_57 = L_56->get_inventoryByID_8();
		int32_t L_58 = V_1;
		NullCheck(L_57);
		List_1_RemoveAt_m2710652734(L_57, L_58, /*hidden argument*/List_1_RemoveAt_m2710652734_MethodInfo_var);
	}

IL_0165:
	{
		int32_t L_59 = V_1;
		V_1 = ((int32_t)((int32_t)L_59+(int32_t)1));
	}

IL_0169:
	{
		int32_t L_60 = V_1;
		ItemInventory_t997429261 * L_61 = __this->get_itemInventory_5();
		NullCheck(L_61);
		List_1_t1440998580 * L_62 = L_61->get_inventoryByID_8();
		NullCheck(L_62);
		int32_t L_63 = List_1_get_Count_m852068579(L_62, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_60) < ((int32_t)L_63)))
		{
			goto IL_0133;
		}
	}

IL_017f:
	{
		int32_t L_64 = V_0;
		V_0 = ((int32_t)((int32_t)L_64+(int32_t)1));
	}

IL_0183:
	{
		int32_t L_65 = V_0;
		ItemInventory_t997429261 * L_66 = __this->get_itemInventory_5();
		NullCheck(L_66);
		List_1_t372321769 * L_67 = L_66->get_shipsEquipment_3();
		NullCheck(L_67);
		int32_t L_68 = List_1_get_Count_m283942784(L_67, /*hidden argument*/List_1_get_Count_m283942784_MethodInfo_var);
		if ((((int32_t)L_65) < ((int32_t)L_68)))
		{
			goto IL_0017;
		}
	}
	{
		GameObject_t1756533147 * L_69 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		MainMenu_t4009084430 * L_70 = __this->get_menuScript_6();
		NullCheck(L_70);
		MainMenu_WeaponInventoryMenuClose_m1434171012(L_70, /*hidden argument*/NULL);
	}

IL_01af:
	{
		Item_t2440468191 * L_71 = __this->get_itemAdded_12();
		NullCheck(L_71);
		int32_t L_72 = L_71->get_itemType_16();
		if ((!(((uint32_t)L_72) == ((uint32_t)2))))
		{
			goto IL_035f;
		}
	}
	{
		V_2 = 0;
		goto IL_0333;
	}

IL_01c7:
	{
		GameController_t3607102586 * L_73 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_73);
		int32_t L_74 = L_73->get_shipID_3();
		ItemInventory_t997429261 * L_75 = __this->get_itemInventory_5();
		NullCheck(L_75);
		List_1_t372321769 * L_76 = L_75->get_shipsEquipment_3();
		int32_t L_77 = V_2;
		NullCheck(L_76);
		ShipsEquipment_t1003200637 * L_78 = List_1_get_Item_m2727922973(L_76, L_77, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_78);
		int32_t L_79 = L_78->get_shipID_3();
		if ((!(((uint32_t)L_74) == ((uint32_t)L_79))))
		{
			goto IL_032f;
		}
	}
	{
		Item_t2440468191 * L_80 = __this->get_itemAdded_12();
		NullCheck(L_80);
		String_t* L_81 = L_80->get_itemShipType_18();
		ItemInventory_t997429261 * L_82 = __this->get_itemInventory_5();
		NullCheck(L_82);
		ShipsController_t970893927 * L_83 = L_82->get_shipController_21();
		NullCheck(L_83);
		List_1_t2004653347 * L_84 = L_83->get_allShips_3();
		int32_t L_85 = V_2;
		NullCheck(L_84);
		PlayerShip_t2635532215 * L_86 = List_1_get_Item_m2773528611(L_84, L_85, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_86);
		String_t* L_87 = L_86->get_shipType_12();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_88 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_81, L_87, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_021d;
		}
	}
	{
		return;
	}

IL_021d:
	{
		ItemInventory_t997429261 * L_89 = __this->get_itemInventory_5();
		ItemInventory_t997429261 * L_90 = __this->get_itemInventory_5();
		NullCheck(L_90);
		List_1_t372321769 * L_91 = L_90->get_shipsEquipment_3();
		int32_t L_92 = V_2;
		NullCheck(L_91);
		ShipsEquipment_t1003200637 * L_93 = List_1_get_Item_m2727922973(L_91, L_92, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		Item_t2440468191 * L_94 = __this->get_itemAdded_12();
		NullCheck(L_89);
		ItemInventory_SwitchEquipedItems_m1895146111(L_89, L_93, L_94, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_95 = __this->get_itemInventory_5();
		NullCheck(L_95);
		List_1_t372321769 * L_96 = L_95->get_shipsEquipment_3();
		int32_t L_97 = V_2;
		NullCheck(L_96);
		ShipsEquipment_t1003200637 * L_98 = List_1_get_Item_m2727922973(L_96, L_97, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_98);
		Item_t2440468191 * L_99 = L_98->get_armor_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_100 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_99, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_028f;
		}
	}
	{
		ItemInventory_t997429261 * L_101 = __this->get_itemInventory_5();
		NullCheck(L_101);
		GameObject_t1756533147 * L_102 = L_101->get_armorHolder_14();
		NullCheck(L_102);
		RectTransform_t3349966182 * L_103 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_102, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_104 = L_103;
		NullCheck(L_104);
		Vector2_t2243707579  L_105 = RectTransform_get_sizeDelta_m2157326342(L_104, /*hidden argument*/NULL);
		Vector2_t2243707579  L_106;
		memset(&L_106, 0, sizeof(L_106));
		Vector2__ctor_m3067419446(&L_106, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_107 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_105, L_106, /*hidden argument*/NULL);
		NullCheck(L_104);
		RectTransform_set_sizeDelta_m2319668137(L_104, L_107, /*hidden argument*/NULL);
	}

IL_028f:
	{
		ItemInventory_t997429261 * L_108 = __this->get_itemInventory_5();
		NullCheck(L_108);
		List_1_t372321769 * L_109 = L_108->get_shipsEquipment_3();
		int32_t L_110 = V_2;
		NullCheck(L_109);
		ShipsEquipment_t1003200637 * L_111 = List_1_get_Item_m2727922973(L_109, L_110, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		Item_t2440468191 * L_112 = __this->get_itemAdded_12();
		NullCheck(L_111);
		L_111->set_armor_6(L_112);
		ItemInventory_t997429261 * L_113 = __this->get_itemInventory_5();
		NullCheck(L_113);
		Image_t2042527209 * L_114 = L_113->get_armorEquipImage_27();
		Item_t2440468191 * L_115 = __this->get_itemAdded_12();
		NullCheck(L_115);
		Sprite_t309593783 * L_116 = L_115->get_itemImage_12();
		NullCheck(L_114);
		Image_set_sprite_m1800056820(L_114, L_116, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_117 = __this->get_itemInventory_5();
		NullCheck(L_117);
		List_1_t372321769 * L_118 = L_117->get_shipsEquipment_3();
		int32_t L_119 = V_2;
		NullCheck(L_118);
		ShipsEquipment_t1003200637 * L_120 = List_1_get_Item_m2727922973(L_118, L_119, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_120);
		ShipsEquipment_SetEquipmentIDs_m3042426158(L_120, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0319;
	}

IL_02e3:
	{
		ItemInventory_t997429261 * L_121 = __this->get_itemInventory_5();
		NullCheck(L_121);
		List_1_t1440998580 * L_122 = L_121->get_inventoryByID_8();
		int32_t L_123 = V_3;
		NullCheck(L_122);
		int32_t L_124 = List_1_get_Item_m1921196075(L_122, L_123, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_125 = __this->get_itemAdded_12();
		NullCheck(L_125);
		int32_t L_126 = L_125->get_itemID_2();
		if ((!(((uint32_t)L_124) == ((uint32_t)L_126))))
		{
			goto IL_0315;
		}
	}
	{
		ItemInventory_t997429261 * L_127 = __this->get_itemInventory_5();
		NullCheck(L_127);
		List_1_t1440998580 * L_128 = L_127->get_inventoryByID_8();
		int32_t L_129 = V_3;
		NullCheck(L_128);
		List_1_RemoveAt_m2710652734(L_128, L_129, /*hidden argument*/List_1_RemoveAt_m2710652734_MethodInfo_var);
	}

IL_0315:
	{
		int32_t L_130 = V_3;
		V_3 = ((int32_t)((int32_t)L_130+(int32_t)1));
	}

IL_0319:
	{
		int32_t L_131 = V_3;
		ItemInventory_t997429261 * L_132 = __this->get_itemInventory_5();
		NullCheck(L_132);
		List_1_t1440998580 * L_133 = L_132->get_inventoryByID_8();
		NullCheck(L_133);
		int32_t L_134 = List_1_get_Count_m852068579(L_133, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_131) < ((int32_t)L_134)))
		{
			goto IL_02e3;
		}
	}

IL_032f:
	{
		int32_t L_135 = V_2;
		V_2 = ((int32_t)((int32_t)L_135+(int32_t)1));
	}

IL_0333:
	{
		int32_t L_136 = V_2;
		ItemInventory_t997429261 * L_137 = __this->get_itemInventory_5();
		NullCheck(L_137);
		List_1_t372321769 * L_138 = L_137->get_shipsEquipment_3();
		NullCheck(L_138);
		int32_t L_139 = List_1_get_Count_m283942784(L_138, /*hidden argument*/List_1_get_Count_m283942784_MethodInfo_var);
		if ((((int32_t)L_136) < ((int32_t)L_139)))
		{
			goto IL_01c7;
		}
	}
	{
		GameObject_t1756533147 * L_140 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_140, /*hidden argument*/NULL);
		MainMenu_t4009084430 * L_141 = __this->get_menuScript_6();
		NullCheck(L_141);
		MainMenu_ArmorInventoryMenuClose_m1327901971(L_141, /*hidden argument*/NULL);
	}

IL_035f:
	{
		Item_t2440468191 * L_142 = __this->get_itemAdded_12();
		NullCheck(L_142);
		int32_t L_143 = L_142->get_itemType_16();
		if ((!(((uint32_t)L_143) == ((uint32_t)1))))
		{
			goto IL_051f;
		}
	}
	{
		V_4 = 0;
		goto IL_04f2;
	}

IL_0378:
	{
		GameController_t3607102586 * L_144 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_144);
		int32_t L_145 = L_144->get_shipID_3();
		ItemInventory_t997429261 * L_146 = __this->get_itemInventory_5();
		NullCheck(L_146);
		List_1_t372321769 * L_147 = L_146->get_shipsEquipment_3();
		int32_t L_148 = V_4;
		NullCheck(L_147);
		ShipsEquipment_t1003200637 * L_149 = List_1_get_Item_m2727922973(L_147, L_148, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_149);
		int32_t L_150 = L_149->get_shipID_3();
		if ((!(((uint32_t)L_145) == ((uint32_t)L_150))))
		{
			goto IL_04ec;
		}
	}
	{
		Item_t2440468191 * L_151 = __this->get_itemAdded_12();
		NullCheck(L_151);
		String_t* L_152 = L_151->get_itemShipType_18();
		ItemInventory_t997429261 * L_153 = __this->get_itemInventory_5();
		NullCheck(L_153);
		ShipsController_t970893927 * L_154 = L_153->get_shipController_21();
		NullCheck(L_154);
		List_1_t2004653347 * L_155 = L_154->get_allShips_3();
		int32_t L_156 = V_4;
		NullCheck(L_155);
		PlayerShip_t2635532215 * L_157 = List_1_get_Item_m2773528611(L_155, L_156, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_157);
		String_t* L_158 = L_157->get_shipType_12();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_159 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_152, L_158, /*hidden argument*/NULL);
		if (!L_159)
		{
			goto IL_03d0;
		}
	}
	{
		return;
	}

IL_03d0:
	{
		ItemInventory_t997429261 * L_160 = __this->get_itemInventory_5();
		ItemInventory_t997429261 * L_161 = __this->get_itemInventory_5();
		NullCheck(L_161);
		List_1_t372321769 * L_162 = L_161->get_shipsEquipment_3();
		int32_t L_163 = V_4;
		NullCheck(L_162);
		ShipsEquipment_t1003200637 * L_164 = List_1_get_Item_m2727922973(L_162, L_163, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		Item_t2440468191 * L_165 = __this->get_itemAdded_12();
		NullCheck(L_160);
		ItemInventory_SwitchEquipedItems_m1895146111(L_160, L_164, L_165, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_166 = __this->get_itemInventory_5();
		NullCheck(L_166);
		List_1_t372321769 * L_167 = L_166->get_shipsEquipment_3();
		int32_t L_168 = V_4;
		NullCheck(L_167);
		ShipsEquipment_t1003200637 * L_169 = List_1_get_Item_m2727922973(L_167, L_168, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_169);
		Item_t2440468191 * L_170 = L_169->get_shield_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_171 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_170, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_171)
		{
			goto IL_0444;
		}
	}
	{
		ItemInventory_t997429261 * L_172 = __this->get_itemInventory_5();
		NullCheck(L_172);
		GameObject_t1756533147 * L_173 = L_172->get_shieldHolder_15();
		NullCheck(L_173);
		RectTransform_t3349966182 * L_174 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_173, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_175 = L_174;
		NullCheck(L_175);
		Vector2_t2243707579  L_176 = RectTransform_get_sizeDelta_m2157326342(L_175, /*hidden argument*/NULL);
		Vector2_t2243707579  L_177;
		memset(&L_177, 0, sizeof(L_177));
		Vector2__ctor_m3067419446(&L_177, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_178 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_176, L_177, /*hidden argument*/NULL);
		NullCheck(L_175);
		RectTransform_set_sizeDelta_m2319668137(L_175, L_178, /*hidden argument*/NULL);
	}

IL_0444:
	{
		ItemInventory_t997429261 * L_179 = __this->get_itemInventory_5();
		NullCheck(L_179);
		List_1_t372321769 * L_180 = L_179->get_shipsEquipment_3();
		int32_t L_181 = V_4;
		NullCheck(L_180);
		ShipsEquipment_t1003200637 * L_182 = List_1_get_Item_m2727922973(L_180, L_181, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		Item_t2440468191 * L_183 = __this->get_itemAdded_12();
		NullCheck(L_182);
		L_182->set_shield_7(L_183);
		ItemInventory_t997429261 * L_184 = __this->get_itemInventory_5();
		NullCheck(L_184);
		Image_t2042527209 * L_185 = L_184->get_shieldEquipImage_28();
		Item_t2440468191 * L_186 = __this->get_itemAdded_12();
		NullCheck(L_186);
		Sprite_t309593783 * L_187 = L_186->get_itemImage_12();
		NullCheck(L_185);
		Image_set_sprite_m1800056820(L_185, L_187, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_188 = __this->get_itemInventory_5();
		NullCheck(L_188);
		List_1_t372321769 * L_189 = L_188->get_shipsEquipment_3();
		int32_t L_190 = V_4;
		NullCheck(L_189);
		ShipsEquipment_t1003200637 * L_191 = List_1_get_Item_m2727922973(L_189, L_190, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_191);
		ShipsEquipment_SetEquipmentIDs_m3042426158(L_191, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_04d5;
	}

IL_049b:
	{
		ItemInventory_t997429261 * L_192 = __this->get_itemInventory_5();
		NullCheck(L_192);
		List_1_t1440998580 * L_193 = L_192->get_inventoryByID_8();
		int32_t L_194 = V_5;
		NullCheck(L_193);
		int32_t L_195 = List_1_get_Item_m1921196075(L_193, L_194, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_196 = __this->get_itemAdded_12();
		NullCheck(L_196);
		int32_t L_197 = L_196->get_itemID_2();
		if ((!(((uint32_t)L_195) == ((uint32_t)L_197))))
		{
			goto IL_04cf;
		}
	}
	{
		ItemInventory_t997429261 * L_198 = __this->get_itemInventory_5();
		NullCheck(L_198);
		List_1_t1440998580 * L_199 = L_198->get_inventoryByID_8();
		int32_t L_200 = V_5;
		NullCheck(L_199);
		List_1_RemoveAt_m2710652734(L_199, L_200, /*hidden argument*/List_1_RemoveAt_m2710652734_MethodInfo_var);
	}

IL_04cf:
	{
		int32_t L_201 = V_5;
		V_5 = ((int32_t)((int32_t)L_201+(int32_t)1));
	}

IL_04d5:
	{
		int32_t L_202 = V_5;
		ItemInventory_t997429261 * L_203 = __this->get_itemInventory_5();
		NullCheck(L_203);
		List_1_t1440998580 * L_204 = L_203->get_inventoryByID_8();
		NullCheck(L_204);
		int32_t L_205 = List_1_get_Count_m852068579(L_204, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_202) < ((int32_t)L_205)))
		{
			goto IL_049b;
		}
	}

IL_04ec:
	{
		int32_t L_206 = V_4;
		V_4 = ((int32_t)((int32_t)L_206+(int32_t)1));
	}

IL_04f2:
	{
		int32_t L_207 = V_4;
		ItemInventory_t997429261 * L_208 = __this->get_itemInventory_5();
		NullCheck(L_208);
		List_1_t372321769 * L_209 = L_208->get_shipsEquipment_3();
		NullCheck(L_209);
		int32_t L_210 = List_1_get_Count_m283942784(L_209, /*hidden argument*/List_1_get_Count_m283942784_MethodInfo_var);
		if ((((int32_t)L_207) < ((int32_t)L_210)))
		{
			goto IL_0378;
		}
	}
	{
		GameObject_t1756533147 * L_211 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_211, /*hidden argument*/NULL);
		MainMenu_t4009084430 * L_212 = __this->get_menuScript_6();
		NullCheck(L_212);
		MainMenu_ShieldInventoryMenuClose_m1486938927(L_212, /*hidden argument*/NULL);
	}

IL_051f:
	{
		Item_t2440468191 * L_213 = __this->get_itemAdded_12();
		NullCheck(L_213);
		int32_t L_214 = L_213->get_itemType_16();
		if ((!(((uint32_t)L_214) == ((uint32_t)3))))
		{
			goto IL_06df;
		}
	}
	{
		V_6 = 0;
		goto IL_06b2;
	}

IL_0538:
	{
		GameController_t3607102586 * L_215 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_215);
		int32_t L_216 = L_215->get_shipID_3();
		ItemInventory_t997429261 * L_217 = __this->get_itemInventory_5();
		NullCheck(L_217);
		List_1_t372321769 * L_218 = L_217->get_shipsEquipment_3();
		int32_t L_219 = V_6;
		NullCheck(L_218);
		ShipsEquipment_t1003200637 * L_220 = List_1_get_Item_m2727922973(L_218, L_219, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_220);
		int32_t L_221 = L_220->get_shipID_3();
		if ((!(((uint32_t)L_216) == ((uint32_t)L_221))))
		{
			goto IL_06ac;
		}
	}
	{
		Item_t2440468191 * L_222 = __this->get_itemAdded_12();
		NullCheck(L_222);
		String_t* L_223 = L_222->get_itemShipType_18();
		ItemInventory_t997429261 * L_224 = __this->get_itemInventory_5();
		NullCheck(L_224);
		ShipsController_t970893927 * L_225 = L_224->get_shipController_21();
		NullCheck(L_225);
		List_1_t2004653347 * L_226 = L_225->get_allShips_3();
		int32_t L_227 = V_6;
		NullCheck(L_226);
		PlayerShip_t2635532215 * L_228 = List_1_get_Item_m2773528611(L_226, L_227, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_228);
		String_t* L_229 = L_228->get_shipType_12();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_230 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_223, L_229, /*hidden argument*/NULL);
		if (!L_230)
		{
			goto IL_0590;
		}
	}
	{
		return;
	}

IL_0590:
	{
		ItemInventory_t997429261 * L_231 = __this->get_itemInventory_5();
		ItemInventory_t997429261 * L_232 = __this->get_itemInventory_5();
		NullCheck(L_232);
		List_1_t372321769 * L_233 = L_232->get_shipsEquipment_3();
		int32_t L_234 = V_6;
		NullCheck(L_233);
		ShipsEquipment_t1003200637 * L_235 = List_1_get_Item_m2727922973(L_233, L_234, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		Item_t2440468191 * L_236 = __this->get_itemAdded_12();
		NullCheck(L_231);
		ItemInventory_SwitchEquipedItems_m1895146111(L_231, L_235, L_236, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_237 = __this->get_itemInventory_5();
		NullCheck(L_237);
		List_1_t372321769 * L_238 = L_237->get_shipsEquipment_3();
		int32_t L_239 = V_6;
		NullCheck(L_238);
		ShipsEquipment_t1003200637 * L_240 = List_1_get_Item_m2727922973(L_238, L_239, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_240);
		Item_t2440468191 * L_241 = L_240->get_engine_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_242 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_241, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_242)
		{
			goto IL_0604;
		}
	}
	{
		ItemInventory_t997429261 * L_243 = __this->get_itemInventory_5();
		NullCheck(L_243);
		GameObject_t1756533147 * L_244 = L_243->get_engineHolder_16();
		NullCheck(L_244);
		RectTransform_t3349966182 * L_245 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_244, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_246 = L_245;
		NullCheck(L_246);
		Vector2_t2243707579  L_247 = RectTransform_get_sizeDelta_m2157326342(L_246, /*hidden argument*/NULL);
		Vector2_t2243707579  L_248;
		memset(&L_248, 0, sizeof(L_248));
		Vector2__ctor_m3067419446(&L_248, (0.0f), (-320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_249 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_247, L_248, /*hidden argument*/NULL);
		NullCheck(L_246);
		RectTransform_set_sizeDelta_m2319668137(L_246, L_249, /*hidden argument*/NULL);
	}

IL_0604:
	{
		ItemInventory_t997429261 * L_250 = __this->get_itemInventory_5();
		NullCheck(L_250);
		List_1_t372321769 * L_251 = L_250->get_shipsEquipment_3();
		int32_t L_252 = V_6;
		NullCheck(L_251);
		ShipsEquipment_t1003200637 * L_253 = List_1_get_Item_m2727922973(L_251, L_252, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		Item_t2440468191 * L_254 = __this->get_itemAdded_12();
		NullCheck(L_253);
		L_253->set_engine_8(L_254);
		ItemInventory_t997429261 * L_255 = __this->get_itemInventory_5();
		NullCheck(L_255);
		Image_t2042527209 * L_256 = L_255->get_engineEquipImage_29();
		Item_t2440468191 * L_257 = __this->get_itemAdded_12();
		NullCheck(L_257);
		Sprite_t309593783 * L_258 = L_257->get_itemImage_12();
		NullCheck(L_256);
		Image_set_sprite_m1800056820(L_256, L_258, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_259 = __this->get_itemInventory_5();
		NullCheck(L_259);
		List_1_t372321769 * L_260 = L_259->get_shipsEquipment_3();
		int32_t L_261 = V_6;
		NullCheck(L_260);
		ShipsEquipment_t1003200637 * L_262 = List_1_get_Item_m2727922973(L_260, L_261, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_262);
		ShipsEquipment_SetEquipmentIDs_m3042426158(L_262, /*hidden argument*/NULL);
		V_7 = 0;
		goto IL_0695;
	}

IL_065b:
	{
		ItemInventory_t997429261 * L_263 = __this->get_itemInventory_5();
		NullCheck(L_263);
		List_1_t1440998580 * L_264 = L_263->get_inventoryByID_8();
		int32_t L_265 = V_7;
		NullCheck(L_264);
		int32_t L_266 = List_1_get_Item_m1921196075(L_264, L_265, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_267 = __this->get_itemAdded_12();
		NullCheck(L_267);
		int32_t L_268 = L_267->get_itemID_2();
		if ((!(((uint32_t)L_266) == ((uint32_t)L_268))))
		{
			goto IL_068f;
		}
	}
	{
		ItemInventory_t997429261 * L_269 = __this->get_itemInventory_5();
		NullCheck(L_269);
		List_1_t1440998580 * L_270 = L_269->get_inventoryByID_8();
		int32_t L_271 = V_7;
		NullCheck(L_270);
		List_1_RemoveAt_m2710652734(L_270, L_271, /*hidden argument*/List_1_RemoveAt_m2710652734_MethodInfo_var);
	}

IL_068f:
	{
		int32_t L_272 = V_7;
		V_7 = ((int32_t)((int32_t)L_272+(int32_t)1));
	}

IL_0695:
	{
		int32_t L_273 = V_7;
		ItemInventory_t997429261 * L_274 = __this->get_itemInventory_5();
		NullCheck(L_274);
		List_1_t1440998580 * L_275 = L_274->get_inventoryByID_8();
		NullCheck(L_275);
		int32_t L_276 = List_1_get_Count_m852068579(L_275, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_273) < ((int32_t)L_276)))
		{
			goto IL_065b;
		}
	}

IL_06ac:
	{
		int32_t L_277 = V_6;
		V_6 = ((int32_t)((int32_t)L_277+(int32_t)1));
	}

IL_06b2:
	{
		int32_t L_278 = V_6;
		ItemInventory_t997429261 * L_279 = __this->get_itemInventory_5();
		NullCheck(L_279);
		List_1_t372321769 * L_280 = L_279->get_shipsEquipment_3();
		NullCheck(L_280);
		int32_t L_281 = List_1_get_Count_m283942784(L_280, /*hidden argument*/List_1_get_Count_m283942784_MethodInfo_var);
		if ((((int32_t)L_278) < ((int32_t)L_281)))
		{
			goto IL_0538;
		}
	}
	{
		GameObject_t1756533147 * L_282 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_282, /*hidden argument*/NULL);
		MainMenu_t4009084430 * L_283 = __this->get_menuScript_6();
		NullCheck(L_283);
		MainMenu_EngineInventoryMenuClose_m1809813282(L_283, /*hidden argument*/NULL);
	}

IL_06df:
	{
		return;
	}
}
// System.Void Item::.ctor()
extern "C"  void Item__ctor_m2013999946 (Item_t2440468191 * __this, const MethodInfo* method)
{
	{
		ScriptableObject__ctor_m2671490429(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ItemDatabase::.ctor()
extern Il2CppClass* List_1_t1809589323_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3545134812_MethodInfo_var;
extern const uint32_t ItemDatabase__ctor_m1971751031_MetadataUsageId;
extern "C"  void ItemDatabase__ctor_m1971751031 (ItemDatabase_t2502586666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemDatabase__ctor_m1971751031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1809589323 * L_0 = (List_1_t1809589323 *)il2cpp_codegen_object_new(List_1_t1809589323_il2cpp_TypeInfo_var);
		List_1__ctor_m3545134812(L_0, /*hidden argument*/List_1__ctor_m3545134812_MethodInfo_var);
		__this->set_weaponsDB_2(L_0);
		List_1_t1809589323 * L_1 = (List_1_t1809589323 *)il2cpp_codegen_object_new(List_1_t1809589323_il2cpp_TypeInfo_var);
		List_1__ctor_m3545134812(L_1, /*hidden argument*/List_1__ctor_m3545134812_MethodInfo_var);
		__this->set_armorsDB_3(L_1);
		List_1_t1809589323 * L_2 = (List_1_t1809589323 *)il2cpp_codegen_object_new(List_1_t1809589323_il2cpp_TypeInfo_var);
		List_1__ctor_m3545134812(L_2, /*hidden argument*/List_1__ctor_m3545134812_MethodInfo_var);
		__this->set_shieldsDB_4(L_2);
		List_1_t1809589323 * L_3 = (List_1_t1809589323 *)il2cpp_codegen_object_new(List_1_t1809589323_il2cpp_TypeInfo_var);
		List_1__ctor_m3545134812(L_3, /*hidden argument*/List_1__ctor_m3545134812_MethodInfo_var);
		__this->set_enginesDB_5(L_3);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ItemDatabase::Start()
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const uint32_t ItemDatabase_Start_m3776298315_MetadataUsageId;
extern "C"  void ItemDatabase_Start_m3776298315 (ItemDatabase_t2502586666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemDatabase_Start_m3776298315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		V_0 = 0;
		goto IL_0053;
	}

IL_0007:
	{
		List_1_t1809589323 * L_0 = __this->get_weaponsDB_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Item_t2440468191 * L_2 = List_1_get_Item_m4027977911(L_0, L_1, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_itemID_2();
		if (L_3)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = Random_Range_m694320887(NULL /*static, unused*/, 1, ((int32_t)100), /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		List_1_t1809589323 * L_6 = __this->get_weaponsDB_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Item_t2440468191 * L_8 = List_1_get_Item_m4027977911(L_6, L_7, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_itemID_2();
		if ((((int32_t)L_5) == ((int32_t)L_9)))
		{
			goto IL_004f;
		}
	}
	{
		List_1_t1809589323 * L_10 = __this->get_weaponsDB_2();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		Item_t2440468191 * L_12 = List_1_get_Item_m4027977911(L_10, L_11, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		int32_t L_13 = V_1;
		NullCheck(L_12);
		L_12->set_itemID_2(L_13);
	}

IL_004f:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0053:
	{
		int32_t L_15 = V_0;
		List_1_t1809589323 * L_16 = __this->get_weaponsDB_2();
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m2506740058(L_16, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0007;
		}
	}
	{
		V_2 = 0;
		goto IL_00bb;
	}

IL_006b:
	{
		List_1_t1809589323 * L_18 = __this->get_armorsDB_3();
		int32_t L_19 = V_2;
		NullCheck(L_18);
		Item_t2440468191 * L_20 = List_1_get_Item_m4027977911(L_18, L_19, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_20);
		int32_t L_21 = L_20->get_itemID_2();
		if (L_21)
		{
			goto IL_00b7;
		}
	}
	{
		int32_t L_22 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)101), ((int32_t)200), /*hidden argument*/NULL);
		V_3 = L_22;
		int32_t L_23 = V_3;
		List_1_t1809589323 * L_24 = __this->get_armorsDB_3();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		Item_t2440468191 * L_26 = List_1_get_Item_m4027977911(L_24, L_25, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_26);
		int32_t L_27 = L_26->get_itemID_2();
		if ((((int32_t)L_23) == ((int32_t)L_27)))
		{
			goto IL_00b7;
		}
	}
	{
		List_1_t1809589323 * L_28 = __this->get_armorsDB_3();
		int32_t L_29 = V_2;
		NullCheck(L_28);
		Item_t2440468191 * L_30 = List_1_get_Item_m4027977911(L_28, L_29, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		int32_t L_31 = V_3;
		NullCheck(L_30);
		L_30->set_itemID_2(L_31);
	}

IL_00b7:
	{
		int32_t L_32 = V_2;
		V_2 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00bb:
	{
		int32_t L_33 = V_2;
		List_1_t1809589323 * L_34 = __this->get_armorsDB_3();
		NullCheck(L_34);
		int32_t L_35 = List_1_get_Count_m2506740058(L_34, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_33) < ((int32_t)L_35)))
		{
			goto IL_006b;
		}
	}
	{
		V_4 = 0;
		goto IL_012f;
	}

IL_00d4:
	{
		List_1_t1809589323 * L_36 = __this->get_shieldsDB_4();
		int32_t L_37 = V_4;
		NullCheck(L_36);
		Item_t2440468191 * L_38 = List_1_get_Item_m4027977911(L_36, L_37, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_38);
		int32_t L_39 = L_38->get_itemID_2();
		if (L_39)
		{
			goto IL_0129;
		}
	}
	{
		int32_t L_40 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)201), ((int32_t)300), /*hidden argument*/NULL);
		V_5 = L_40;
		int32_t L_41 = V_5;
		List_1_t1809589323 * L_42 = __this->get_shieldsDB_4();
		int32_t L_43 = V_4;
		NullCheck(L_42);
		Item_t2440468191 * L_44 = List_1_get_Item_m4027977911(L_42, L_43, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_44);
		int32_t L_45 = L_44->get_itemID_2();
		if ((((int32_t)L_41) == ((int32_t)L_45)))
		{
			goto IL_0129;
		}
	}
	{
		List_1_t1809589323 * L_46 = __this->get_shieldsDB_4();
		int32_t L_47 = V_4;
		NullCheck(L_46);
		Item_t2440468191 * L_48 = List_1_get_Item_m4027977911(L_46, L_47, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		int32_t L_49 = V_5;
		NullCheck(L_48);
		L_48->set_itemID_2(L_49);
	}

IL_0129:
	{
		int32_t L_50 = V_4;
		V_4 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_012f:
	{
		int32_t L_51 = V_4;
		List_1_t1809589323 * L_52 = __this->get_shieldsDB_4();
		NullCheck(L_52);
		int32_t L_53 = List_1_get_Count_m2506740058(L_52, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_51) < ((int32_t)L_53)))
		{
			goto IL_00d4;
		}
	}
	{
		V_6 = 0;
		goto IL_01a4;
	}

IL_0149:
	{
		List_1_t1809589323 * L_54 = __this->get_enginesDB_5();
		int32_t L_55 = V_6;
		NullCheck(L_54);
		Item_t2440468191 * L_56 = List_1_get_Item_m4027977911(L_54, L_55, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_56);
		int32_t L_57 = L_56->get_itemID_2();
		if (L_57)
		{
			goto IL_019e;
		}
	}
	{
		int32_t L_58 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)301), ((int32_t)400), /*hidden argument*/NULL);
		V_7 = L_58;
		int32_t L_59 = V_7;
		List_1_t1809589323 * L_60 = __this->get_enginesDB_5();
		int32_t L_61 = V_6;
		NullCheck(L_60);
		Item_t2440468191 * L_62 = List_1_get_Item_m4027977911(L_60, L_61, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_62);
		int32_t L_63 = L_62->get_itemID_2();
		if ((((int32_t)L_59) == ((int32_t)L_63)))
		{
			goto IL_019e;
		}
	}
	{
		List_1_t1809589323 * L_64 = __this->get_enginesDB_5();
		int32_t L_65 = V_6;
		NullCheck(L_64);
		Item_t2440468191 * L_66 = List_1_get_Item_m4027977911(L_64, L_65, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		int32_t L_67 = V_7;
		NullCheck(L_66);
		L_66->set_itemID_2(L_67);
	}

IL_019e:
	{
		int32_t L_68 = V_6;
		V_6 = ((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_01a4:
	{
		int32_t L_69 = V_6;
		List_1_t1809589323 * L_70 = __this->get_enginesDB_5();
		NullCheck(L_70);
		int32_t L_71 = List_1_get_Count_m2506740058(L_70, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_69) < ((int32_t)L_71)))
		{
			goto IL_0149;
		}
	}
	{
		return;
	}
}
// System.Void ItemInventory::.ctor()
extern Il2CppClass* List_1_t372321769_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1809589323_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m649369026_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3545134812_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const uint32_t ItemInventory__ctor_m1344467504_MetadataUsageId;
extern "C"  void ItemInventory__ctor_m1344467504 (ItemInventory_t997429261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory__ctor_m1344467504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t372321769 * L_0 = (List_1_t372321769 *)il2cpp_codegen_object_new(List_1_t372321769_il2cpp_TypeInfo_var);
		List_1__ctor_m649369026(L_0, /*hidden argument*/List_1__ctor_m649369026_MethodInfo_var);
		__this->set_shipsEquipment_3(L_0);
		List_1_t1809589323 * L_1 = (List_1_t1809589323 *)il2cpp_codegen_object_new(List_1_t1809589323_il2cpp_TypeInfo_var);
		List_1__ctor_m3545134812(L_1, /*hidden argument*/List_1__ctor_m3545134812_MethodInfo_var);
		__this->set_weaponItems_4(L_1);
		List_1_t1809589323 * L_2 = (List_1_t1809589323 *)il2cpp_codegen_object_new(List_1_t1809589323_il2cpp_TypeInfo_var);
		List_1__ctor_m3545134812(L_2, /*hidden argument*/List_1__ctor_m3545134812_MethodInfo_var);
		__this->set_armorItems_5(L_2);
		List_1_t1809589323 * L_3 = (List_1_t1809589323 *)il2cpp_codegen_object_new(List_1_t1809589323_il2cpp_TypeInfo_var);
		List_1__ctor_m3545134812(L_3, /*hidden argument*/List_1__ctor_m3545134812_MethodInfo_var);
		__this->set_shieldItems_6(L_3);
		List_1_t1809589323 * L_4 = (List_1_t1809589323 *)il2cpp_codegen_object_new(List_1_t1809589323_il2cpp_TypeInfo_var);
		List_1__ctor_m3545134812(L_4, /*hidden argument*/List_1__ctor_m3545134812_MethodInfo_var);
		__this->set_engineItems_7(L_4);
		List_1_t1440998580 * L_5 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_5, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		__this->set_inventoryByID_8(L_5);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ItemInventory::Start()
extern const MethodInfo* GameObject_GetComponent_TisInventoryPanel_t2677987186_m4124782355_MethodInfo_var;
extern const uint32_t ItemInventory_Start_m720955688_MetadataUsageId;
extern "C"  void ItemInventory_Start_m720955688 (ItemInventory_t997429261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_Start_m720955688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_inventoryPanelBuy_17();
		NullCheck(L_0);
		InventoryPanel_t2677987186 * L_1 = GameObject_GetComponent_TisInventoryPanel_t2677987186_m4124782355(L_0, /*hidden argument*/GameObject_GetComponent_TisInventoryPanel_t2677987186_m4124782355_MethodInfo_var);
		__this->set_marketInventoryPanel_19(L_1);
		GameObject_t1756533147 * L_2 = __this->get_inventoryPanelEquip_18();
		NullCheck(L_2);
		InventoryPanel_t2677987186 * L_3 = GameObject_GetComponent_TisInventoryPanel_t2677987186_m4124782355(L_2, /*hidden argument*/GameObject_GetComponent_TisInventoryPanel_t2677987186_m4124782355_MethodInfo_var);
		__this->set_inventoryPanel_20(L_3);
		ItemInventory_ItemsToList_m3796793559(__this, /*hidden argument*/NULL);
		ItemInventory_ChekInventoryAtStartUp_m2259022607(__this, /*hidden argument*/NULL);
		ItemInventory_ChekEquipmentAtStartUp_m3977349727(__this, /*hidden argument*/NULL);
		ItemInventory_ControlItemsList_m574720063(__this, /*hidden argument*/NULL);
		ItemInventory_SpawnMarketItems_m131373105(__this, /*hidden argument*/NULL);
		ItemInventory_InstantiateWeapon_m2838280588(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ItemInventory::ItemsToList()
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3917865408_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const uint32_t ItemInventory_ItemsToList_m3796793559_MetadataUsageId;
extern "C"  void ItemInventory_ItemsToList_m3796793559 (ItemInventory_t997429261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_ItemsToList_m3796793559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		V_0 = 0;
		goto IL_0027;
	}

IL_0007:
	{
		List_1_t1809589323 * L_0 = __this->get_weaponItems_4();
		ItemDatabase_t2502586666 * L_1 = __this->get_data_2();
		NullCheck(L_1);
		List_1_t1809589323 * L_2 = L_1->get_weaponsDB_2();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Item_t2440468191 * L_4 = List_1_get_Item_m4027977911(L_2, L_3, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_0);
		List_1_Add_m3917865408(L_0, L_4, /*hidden argument*/List_1_Add_m3917865408_MethodInfo_var);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_6 = V_0;
		ItemDatabase_t2502586666 * L_7 = __this->get_data_2();
		NullCheck(L_7);
		List_1_t1809589323 * L_8 = L_7->get_weaponsDB_2();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m2506740058(L_8, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_6) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		V_1 = 0;
		goto IL_0064;
	}

IL_0044:
	{
		List_1_t1809589323 * L_10 = __this->get_armorItems_5();
		ItemDatabase_t2502586666 * L_11 = __this->get_data_2();
		NullCheck(L_11);
		List_1_t1809589323 * L_12 = L_11->get_armorsDB_3();
		int32_t L_13 = V_1;
		NullCheck(L_12);
		Item_t2440468191 * L_14 = List_1_get_Item_m4027977911(L_12, L_13, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_10);
		List_1_Add_m3917865408(L_10, L_14, /*hidden argument*/List_1_Add_m3917865408_MethodInfo_var);
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_16 = V_1;
		ItemDatabase_t2502586666 * L_17 = __this->get_data_2();
		NullCheck(L_17);
		List_1_t1809589323 * L_18 = L_17->get_armorsDB_3();
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m2506740058(L_18, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_16) < ((int32_t)L_19)))
		{
			goto IL_0044;
		}
	}
	{
		V_2 = 0;
		goto IL_00a1;
	}

IL_0081:
	{
		List_1_t1809589323 * L_20 = __this->get_shieldItems_6();
		ItemDatabase_t2502586666 * L_21 = __this->get_data_2();
		NullCheck(L_21);
		List_1_t1809589323 * L_22 = L_21->get_shieldsDB_4();
		int32_t L_23 = V_2;
		NullCheck(L_22);
		Item_t2440468191 * L_24 = List_1_get_Item_m4027977911(L_22, L_23, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_20);
		List_1_Add_m3917865408(L_20, L_24, /*hidden argument*/List_1_Add_m3917865408_MethodInfo_var);
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00a1:
	{
		int32_t L_26 = V_2;
		ItemDatabase_t2502586666 * L_27 = __this->get_data_2();
		NullCheck(L_27);
		List_1_t1809589323 * L_28 = L_27->get_shieldsDB_4();
		NullCheck(L_28);
		int32_t L_29 = List_1_get_Count_m2506740058(L_28, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_26) < ((int32_t)L_29)))
		{
			goto IL_0081;
		}
	}
	{
		V_3 = 0;
		goto IL_00de;
	}

IL_00be:
	{
		List_1_t1809589323 * L_30 = __this->get_engineItems_7();
		ItemDatabase_t2502586666 * L_31 = __this->get_data_2();
		NullCheck(L_31);
		List_1_t1809589323 * L_32 = L_31->get_enginesDB_5();
		int32_t L_33 = V_3;
		NullCheck(L_32);
		Item_t2440468191 * L_34 = List_1_get_Item_m4027977911(L_32, L_33, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_30);
		List_1_Add_m3917865408(L_30, L_34, /*hidden argument*/List_1_Add_m3917865408_MethodInfo_var);
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00de:
	{
		int32_t L_36 = V_3;
		ItemDatabase_t2502586666 * L_37 = __this->get_data_2();
		NullCheck(L_37);
		List_1_t1809589323 * L_38 = L_37->get_enginesDB_5();
		NullCheck(L_38);
		int32_t L_39 = List_1_get_Count_m2506740058(L_38, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_36) < ((int32_t)L_39)))
		{
			goto IL_00be;
		}
	}
	{
		return;
	}
}
// System.Void ItemInventory::ControlItemsList()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2727922973_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m3735251282_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m283942784_MethodInfo_var;
extern const uint32_t ItemInventory_ControlItemsList_m574720063_MetadataUsageId;
extern "C"  void ItemInventory_ControlItemsList_m574720063 (ItemInventory_t997429261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_ControlItemsList_m574720063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Item_t2440468191 * V_1 = NULL;
	Item_t2440468191 * V_2 = NULL;
	Item_t2440468191 * V_3 = NULL;
	Item_t2440468191 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		V_0 = 0;
		goto IL_01ca;
	}

IL_0007:
	{
		List_1_t372321769 * L_0 = __this->get_shipsEquipment_3();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		ShipsEquipment_t1003200637 * L_2 = List_1_get_Item_m2727922973(L_0, L_1, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_2);
		List_1_t1440998580 * L_3 = L_2->get_equipment_4();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Item_m1921196075(L_3, 0, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_5 = ItemInventory_GetWeaponItem_m1346040764(__this, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t372321769 * L_6 = __this->get_shipsEquipment_3();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		ShipsEquipment_t1003200637 * L_8 = List_1_get_Item_m2727922973(L_6, L_7, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_8);
		List_1_t1440998580 * L_9 = L_8->get_equipment_4();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Item_m1921196075(L_9, 1, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_11 = ItemInventory_GetArmorItem_m820844179(__this, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		List_1_t372321769 * L_12 = __this->get_shipsEquipment_3();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		ShipsEquipment_t1003200637 * L_14 = List_1_get_Item_m2727922973(L_12, L_13, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_14);
		List_1_t1440998580 * L_15 = L_14->get_equipment_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Item_m1921196075(L_15, 2, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_17 = ItemInventory_GetShieldItem_m3227698875(__this, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		List_1_t372321769 * L_18 = __this->get_shipsEquipment_3();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		ShipsEquipment_t1003200637 * L_20 = List_1_get_Item_m2727922973(L_18, L_19, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_20);
		List_1_t1440998580 * L_21 = L_20->get_equipment_4();
		NullCheck(L_21);
		int32_t L_22 = List_1_get_Item_m1921196075(L_21, 3, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_23 = ItemInventory_GetEngineItem_m2654585570(__this, L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Item_t2440468191 * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_24, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d1;
		}
	}
	{
		V_5 = 0;
		goto IL_00bf;
	}

IL_0094:
	{
		Item_t2440468191 * L_26 = V_1;
		List_1_t1809589323 * L_27 = __this->get_weaponItems_4();
		int32_t L_28 = V_5;
		NullCheck(L_27);
		Item_t2440468191 * L_29 = List_1_get_Item_m4027977911(L_27, L_28, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_26, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00b9;
		}
	}
	{
		List_1_t1809589323 * L_31 = __this->get_weaponItems_4();
		int32_t L_32 = V_5;
		NullCheck(L_31);
		List_1_RemoveAt_m3735251282(L_31, L_32, /*hidden argument*/List_1_RemoveAt_m3735251282_MethodInfo_var);
	}

IL_00b9:
	{
		int32_t L_33 = V_5;
		V_5 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00bf:
	{
		int32_t L_34 = V_5;
		List_1_t1809589323 * L_35 = __this->get_weaponItems_4();
		NullCheck(L_35);
		int32_t L_36 = List_1_get_Count_m2506740058(L_35, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_34) < ((int32_t)L_36)))
		{
			goto IL_0094;
		}
	}

IL_00d1:
	{
		Item_t2440468191 * L_37 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_37, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0122;
		}
	}
	{
		V_6 = 0;
		goto IL_0110;
	}

IL_00e5:
	{
		Item_t2440468191 * L_39 = V_2;
		List_1_t1809589323 * L_40 = __this->get_armorItems_5();
		int32_t L_41 = V_6;
		NullCheck(L_40);
		Item_t2440468191 * L_42 = List_1_get_Item_m4027977911(L_40, L_41, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_43 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_39, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_010a;
		}
	}
	{
		List_1_t1809589323 * L_44 = __this->get_armorItems_5();
		int32_t L_45 = V_6;
		NullCheck(L_44);
		List_1_RemoveAt_m3735251282(L_44, L_45, /*hidden argument*/List_1_RemoveAt_m3735251282_MethodInfo_var);
	}

IL_010a:
	{
		int32_t L_46 = V_6;
		V_6 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_0110:
	{
		int32_t L_47 = V_6;
		List_1_t1809589323 * L_48 = __this->get_armorItems_5();
		NullCheck(L_48);
		int32_t L_49 = List_1_get_Count_m2506740058(L_48, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_47) < ((int32_t)L_49)))
		{
			goto IL_00e5;
		}
	}

IL_0122:
	{
		Item_t2440468191 * L_50 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_51 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_50, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_0173;
		}
	}
	{
		V_7 = 0;
		goto IL_0161;
	}

IL_0136:
	{
		Item_t2440468191 * L_52 = V_3;
		List_1_t1809589323 * L_53 = __this->get_shieldItems_6();
		int32_t L_54 = V_7;
		NullCheck(L_53);
		Item_t2440468191 * L_55 = List_1_get_Item_m4027977911(L_53, L_54, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_56 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_52, L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_015b;
		}
	}
	{
		List_1_t1809589323 * L_57 = __this->get_shieldItems_6();
		int32_t L_58 = V_7;
		NullCheck(L_57);
		List_1_RemoveAt_m3735251282(L_57, L_58, /*hidden argument*/List_1_RemoveAt_m3735251282_MethodInfo_var);
	}

IL_015b:
	{
		int32_t L_59 = V_7;
		V_7 = ((int32_t)((int32_t)L_59+(int32_t)1));
	}

IL_0161:
	{
		int32_t L_60 = V_7;
		List_1_t1809589323 * L_61 = __this->get_shieldItems_6();
		NullCheck(L_61);
		int32_t L_62 = List_1_get_Count_m2506740058(L_61, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_60) < ((int32_t)L_62)))
		{
			goto IL_0136;
		}
	}

IL_0173:
	{
		Item_t2440468191 * L_63 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_64 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_63, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01c6;
		}
	}
	{
		V_8 = 0;
		goto IL_01b4;
	}

IL_0188:
	{
		Item_t2440468191 * L_65 = V_4;
		List_1_t1809589323 * L_66 = __this->get_engineItems_7();
		int32_t L_67 = V_8;
		NullCheck(L_66);
		Item_t2440468191 * L_68 = List_1_get_Item_m4027977911(L_66, L_67, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_69 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_65, L_68, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_01ae;
		}
	}
	{
		List_1_t1809589323 * L_70 = __this->get_engineItems_7();
		int32_t L_71 = V_8;
		NullCheck(L_70);
		List_1_RemoveAt_m3735251282(L_70, L_71, /*hidden argument*/List_1_RemoveAt_m3735251282_MethodInfo_var);
	}

IL_01ae:
	{
		int32_t L_72 = V_8;
		V_8 = ((int32_t)((int32_t)L_72+(int32_t)1));
	}

IL_01b4:
	{
		int32_t L_73 = V_8;
		List_1_t1809589323 * L_74 = __this->get_engineItems_7();
		NullCheck(L_74);
		int32_t L_75 = List_1_get_Count_m2506740058(L_74, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_73) < ((int32_t)L_75)))
		{
			goto IL_0188;
		}
	}

IL_01c6:
	{
		int32_t L_76 = V_0;
		V_0 = ((int32_t)((int32_t)L_76+(int32_t)1));
	}

IL_01ca:
	{
		int32_t L_77 = V_0;
		List_1_t372321769 * L_78 = __this->get_shipsEquipment_3();
		NullCheck(L_78);
		int32_t L_79 = List_1_get_Count_m283942784(L_78, /*hidden argument*/List_1_get_Count_m283942784_MethodInfo_var);
		if ((((int32_t)L_77) < ((int32_t)L_79)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ItemInventory::ChekEquipmentAtStartUp()
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2727922973_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m283942784_MethodInfo_var;
extern const uint32_t ItemInventory_ChekEquipmentAtStartUp_m3977349727_MetadataUsageId;
extern "C"  void ItemInventory_ChekEquipmentAtStartUp_m3977349727 (ItemInventory_t997429261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_ChekEquipmentAtStartUp_m3977349727_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Item_t2440468191 * V_1 = NULL;
	Item_t2440468191 * V_2 = NULL;
	Item_t2440468191 * V_3 = NULL;
	Item_t2440468191 * V_4 = NULL;
	{
		V_0 = 0;
		goto IL_018f;
	}

IL_0007:
	{
		List_1_t372321769 * L_0 = __this->get_shipsEquipment_3();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		ShipsEquipment_t1003200637 * L_2 = List_1_get_Item_m2727922973(L_0, L_1, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_2);
		List_1_t1440998580 * L_3 = L_2->get_equipment_4();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Item_m1921196075(L_3, 0, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_5 = ItemInventory_GetWeaponItem_m1346040764(__this, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t372321769 * L_6 = __this->get_shipsEquipment_3();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		ShipsEquipment_t1003200637 * L_8 = List_1_get_Item_m2727922973(L_6, L_7, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_8);
		List_1_t1440998580 * L_9 = L_8->get_equipment_4();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Item_m1921196075(L_9, 1, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_11 = ItemInventory_GetArmorItem_m820844179(__this, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		List_1_t372321769 * L_12 = __this->get_shipsEquipment_3();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		ShipsEquipment_t1003200637 * L_14 = List_1_get_Item_m2727922973(L_12, L_13, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_14);
		List_1_t1440998580 * L_15 = L_14->get_equipment_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Item_m1921196075(L_15, 2, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_17 = ItemInventory_GetShieldItem_m3227698875(__this, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		List_1_t372321769 * L_18 = __this->get_shipsEquipment_3();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		ShipsEquipment_t1003200637 * L_20 = List_1_get_Item_m2727922973(L_18, L_19, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_20);
		List_1_t1440998580 * L_21 = L_20->get_equipment_4();
		NullCheck(L_21);
		int32_t L_22 = List_1_get_Item_m1921196075(L_21, 3, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_23 = ItemInventory_GetEngineItem_m2654585570(__this, L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		GameController_t3607102586 * L_24 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_shipID_3();
		List_1_t372321769 * L_26 = __this->get_shipsEquipment_3();
		int32_t L_27 = V_0;
		NullCheck(L_26);
		ShipsEquipment_t1003200637 * L_28 = List_1_get_Item_m2727922973(L_26, L_27, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_28);
		int32_t L_29 = L_28->get_shipID_3();
		if ((!(((uint32_t)L_25) == ((uint32_t)L_29))))
		{
			goto IL_018b;
		}
	}
	{
		Item_t2440468191 * L_30 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_31 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_30, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00c2;
		}
	}
	{
		Image_t2042527209 * L_32 = __this->get_weaponEquipImage_26();
		Item_t2440468191 * L_33 = V_1;
		NullCheck(L_33);
		Sprite_t309593783 * L_34 = L_33->get_itemImage_12();
		NullCheck(L_32);
		Image_set_sprite_m1800056820(L_32, L_34, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00c2:
	{
		Item_t2440468191 * L_35 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_35, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00da;
		}
	}
	{
		Image_t2042527209 * L_37 = __this->get_weaponEquipImage_26();
		NullCheck(L_37);
		Image_set_sprite_m1800056820(L_37, (Sprite_t309593783 *)NULL, /*hidden argument*/NULL);
	}

IL_00da:
	{
		Item_t2440468191 * L_38 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_39 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_38, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00fc;
		}
	}
	{
		Image_t2042527209 * L_40 = __this->get_armorEquipImage_27();
		Item_t2440468191 * L_41 = V_2;
		NullCheck(L_41);
		Sprite_t309593783 * L_42 = L_41->get_itemImage_12();
		NullCheck(L_40);
		Image_set_sprite_m1800056820(L_40, L_42, /*hidden argument*/NULL);
		goto IL_0114;
	}

IL_00fc:
	{
		Item_t2440468191 * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_44 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_43, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0114;
		}
	}
	{
		Image_t2042527209 * L_45 = __this->get_armorEquipImage_27();
		NullCheck(L_45);
		Image_set_sprite_m1800056820(L_45, (Sprite_t309593783 *)NULL, /*hidden argument*/NULL);
	}

IL_0114:
	{
		Item_t2440468191 * L_46 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_46, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0136;
		}
	}
	{
		Image_t2042527209 * L_48 = __this->get_shieldEquipImage_28();
		Item_t2440468191 * L_49 = V_3;
		NullCheck(L_49);
		Sprite_t309593783 * L_50 = L_49->get_itemImage_12();
		NullCheck(L_48);
		Image_set_sprite_m1800056820(L_48, L_50, /*hidden argument*/NULL);
		goto IL_014e;
	}

IL_0136:
	{
		Item_t2440468191 * L_51 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_52 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_51, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_014e;
		}
	}
	{
		Image_t2042527209 * L_53 = __this->get_shieldEquipImage_28();
		NullCheck(L_53);
		Image_set_sprite_m1800056820(L_53, (Sprite_t309593783 *)NULL, /*hidden argument*/NULL);
	}

IL_014e:
	{
		Item_t2440468191 * L_54 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_55 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_54, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0172;
		}
	}
	{
		Image_t2042527209 * L_56 = __this->get_engineEquipImage_29();
		Item_t2440468191 * L_57 = V_4;
		NullCheck(L_57);
		Sprite_t309593783 * L_58 = L_57->get_itemImage_12();
		NullCheck(L_56);
		Image_set_sprite_m1800056820(L_56, L_58, /*hidden argument*/NULL);
		goto IL_018b;
	}

IL_0172:
	{
		Item_t2440468191 * L_59 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_60 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_59, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_018b;
		}
	}
	{
		Image_t2042527209 * L_61 = __this->get_engineEquipImage_29();
		NullCheck(L_61);
		Image_set_sprite_m1800056820(L_61, (Sprite_t309593783 *)NULL, /*hidden argument*/NULL);
	}

IL_018b:
	{
		int32_t L_62 = V_0;
		V_0 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_018f:
	{
		int32_t L_63 = V_0;
		List_1_t372321769 * L_64 = __this->get_shipsEquipment_3();
		NullCheck(L_64);
		int32_t L_65 = List_1_get_Count_m283942784(L_64, /*hidden argument*/List_1_get_Count_m283942784_MethodInfo_var);
		if ((((int32_t)L_63) < ((int32_t)L_65)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ItemInventory::ChekInventoryAtStartUp()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m3735251282_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m852068579_MethodInfo_var;
extern const uint32_t ItemInventory_ChekInventoryAtStartUp_m2259022607_MetadataUsageId;
extern "C"  void ItemInventory_ChekInventoryAtStartUp_m2259022607 (ItemInventory_t997429261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_ChekInventoryAtStartUp_m2259022607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Item_t2440468191 * V_1 = NULL;
	Item_t2440468191 * V_2 = NULL;
	Item_t2440468191 * V_3 = NULL;
	Item_t2440468191 * V_4 = NULL;
	GameObject_t1756533147 * V_5 = NULL;
	int32_t V_6 = 0;
	GameObject_t1756533147 * V_7 = NULL;
	int32_t V_8 = 0;
	GameObject_t1756533147 * V_9 = NULL;
	int32_t V_10 = 0;
	GameObject_t1756533147 * V_11 = NULL;
	int32_t V_12 = 0;
	{
		V_0 = 0;
		goto IL_0333;
	}

IL_0007:
	{
		List_1_t1440998580 * L_0 = __this->get_inventoryByID_8();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = List_1_get_Item_m1921196075(L_0, L_1, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_3 = ItemInventory_GetWeaponItem_m1346040764(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		List_1_t1440998580 * L_4 = __this->get_inventoryByID_8();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = List_1_get_Item_m1921196075(L_4, L_5, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_7 = ItemInventory_GetArmorItem_m820844179(__this, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		List_1_t1440998580 * L_8 = __this->get_inventoryByID_8();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = List_1_get_Item_m1921196075(L_8, L_9, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_11 = ItemInventory_GetShieldItem_m3227698875(__this, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		List_1_t1440998580 * L_12 = __this->get_inventoryByID_8();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = List_1_get_Item_m1921196075(L_12, L_13, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		Item_t2440468191 * L_15 = ItemInventory_GetEngineItem_m2654585570(__this, L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		Item_t2440468191 * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_010a;
		}
	}
	{
		InventoryPanel_t2677987186 * L_18 = __this->get_inventoryPanel_20();
		Item_t2440468191 * L_19 = V_1;
		NullCheck(L_18);
		InventoryPanel_AddItem_m2777303932(L_18, L_19, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = __this->get_weaponHolder_13();
		NullCheck(L_20);
		RectTransform_t3349966182 * L_21 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_20, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_22 = L_21;
		NullCheck(L_22);
		Vector2_t2243707579  L_23 = RectTransform_get_sizeDelta_m2157326342(L_22, /*hidden argument*/NULL);
		Vector2_t2243707579  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector2__ctor_m3067419446(&L_24, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_25 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_sizeDelta_m2319668137(L_22, L_25, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_26 = __this->get_inventoryPanelEquip_18();
		GameObject_t1756533147 * L_27 = __this->get_weaponHolder_13();
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = GameObject_get_transform_m909382139(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_29 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_5 = L_29;
		GameObject_t1756533147 * L_30 = V_5;
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = GameObject_get_transform_m909382139(L_30, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_32 = __this->get_weaponHolder_13();
		NullCheck(L_32);
		Transform_t3275118058 * L_33 = GameObject_get_transform_m909382139(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_SetParent_m4124909910(L_31, L_33, /*hidden argument*/NULL);
		V_6 = 0;
		goto IL_00f8;
	}

IL_00cd:
	{
		Item_t2440468191 * L_34 = V_1;
		List_1_t1809589323 * L_35 = __this->get_weaponItems_4();
		int32_t L_36 = V_6;
		NullCheck(L_35);
		Item_t2440468191 * L_37 = List_1_get_Item_m4027977911(L_35, L_36, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_34, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00f2;
		}
	}
	{
		List_1_t1809589323 * L_39 = __this->get_weaponItems_4();
		int32_t L_40 = V_6;
		NullCheck(L_39);
		List_1_RemoveAt_m3735251282(L_39, L_40, /*hidden argument*/List_1_RemoveAt_m3735251282_MethodInfo_var);
	}

IL_00f2:
	{
		int32_t L_41 = V_6;
		V_6 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00f8:
	{
		int32_t L_42 = V_6;
		List_1_t1809589323 * L_43 = __this->get_weaponItems_4();
		NullCheck(L_43);
		int32_t L_44 = List_1_get_Count_m2506740058(L_43, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_42) < ((int32_t)L_44)))
		{
			goto IL_00cd;
		}
	}

IL_010a:
	{
		Item_t2440468191 * L_45 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_46 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_45, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_01c0;
		}
	}
	{
		InventoryPanel_t2677987186 * L_47 = __this->get_inventoryPanel_20();
		Item_t2440468191 * L_48 = V_2;
		NullCheck(L_47);
		InventoryPanel_AddItem_m2777303932(L_47, L_48, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_49 = __this->get_weaponHolder_13();
		NullCheck(L_49);
		RectTransform_t3349966182 * L_50 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_49, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_51 = L_50;
		NullCheck(L_51);
		Vector2_t2243707579  L_52 = RectTransform_get_sizeDelta_m2157326342(L_51, /*hidden argument*/NULL);
		Vector2_t2243707579  L_53;
		memset(&L_53, 0, sizeof(L_53));
		Vector2__ctor_m3067419446(&L_53, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_54 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_51);
		RectTransform_set_sizeDelta_m2319668137(L_51, L_54, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_55 = __this->get_inventoryPanelEquip_18();
		GameObject_t1756533147 * L_56 = __this->get_armorHolder_14();
		NullCheck(L_56);
		Transform_t3275118058 * L_57 = GameObject_get_transform_m909382139(L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_58 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_55, L_57, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_7 = L_58;
		GameObject_t1756533147 * L_59 = V_7;
		NullCheck(L_59);
		Transform_t3275118058 * L_60 = GameObject_get_transform_m909382139(L_59, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_61 = __this->get_armorHolder_14();
		NullCheck(L_61);
		Transform_t3275118058 * L_62 = GameObject_get_transform_m909382139(L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		Transform_SetParent_m4124909910(L_60, L_62, /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_01ae;
	}

IL_0183:
	{
		Item_t2440468191 * L_63 = V_2;
		List_1_t1809589323 * L_64 = __this->get_armorItems_5();
		int32_t L_65 = V_8;
		NullCheck(L_64);
		Item_t2440468191 * L_66 = List_1_get_Item_m4027977911(L_64, L_65, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_67 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_63, L_66, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_01a8;
		}
	}
	{
		List_1_t1809589323 * L_68 = __this->get_armorItems_5();
		int32_t L_69 = V_8;
		NullCheck(L_68);
		List_1_RemoveAt_m3735251282(L_68, L_69, /*hidden argument*/List_1_RemoveAt_m3735251282_MethodInfo_var);
	}

IL_01a8:
	{
		int32_t L_70 = V_8;
		V_8 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_01ae:
	{
		int32_t L_71 = V_8;
		List_1_t1809589323 * L_72 = __this->get_armorItems_5();
		NullCheck(L_72);
		int32_t L_73 = List_1_get_Count_m2506740058(L_72, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_71) < ((int32_t)L_73)))
		{
			goto IL_0183;
		}
	}

IL_01c0:
	{
		Item_t2440468191 * L_74 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_75 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_74, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_0276;
		}
	}
	{
		InventoryPanel_t2677987186 * L_76 = __this->get_inventoryPanel_20();
		Item_t2440468191 * L_77 = V_3;
		NullCheck(L_76);
		InventoryPanel_AddItem_m2777303932(L_76, L_77, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_78 = __this->get_weaponHolder_13();
		NullCheck(L_78);
		RectTransform_t3349966182 * L_79 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_78, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_80 = L_79;
		NullCheck(L_80);
		Vector2_t2243707579  L_81 = RectTransform_get_sizeDelta_m2157326342(L_80, /*hidden argument*/NULL);
		Vector2_t2243707579  L_82;
		memset(&L_82, 0, sizeof(L_82));
		Vector2__ctor_m3067419446(&L_82, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_83 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_81, L_82, /*hidden argument*/NULL);
		NullCheck(L_80);
		RectTransform_set_sizeDelta_m2319668137(L_80, L_83, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_84 = __this->get_inventoryPanelEquip_18();
		GameObject_t1756533147 * L_85 = __this->get_shieldHolder_15();
		NullCheck(L_85);
		Transform_t3275118058 * L_86 = GameObject_get_transform_m909382139(L_85, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_87 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_84, L_86, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_9 = L_87;
		GameObject_t1756533147 * L_88 = V_9;
		NullCheck(L_88);
		Transform_t3275118058 * L_89 = GameObject_get_transform_m909382139(L_88, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_90 = __this->get_shieldHolder_15();
		NullCheck(L_90);
		Transform_t3275118058 * L_91 = GameObject_get_transform_m909382139(L_90, /*hidden argument*/NULL);
		NullCheck(L_89);
		Transform_SetParent_m4124909910(L_89, L_91, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_0264;
	}

IL_0239:
	{
		Item_t2440468191 * L_92 = V_3;
		List_1_t1809589323 * L_93 = __this->get_shieldItems_6();
		int32_t L_94 = V_10;
		NullCheck(L_93);
		Item_t2440468191 * L_95 = List_1_get_Item_m4027977911(L_93, L_94, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_96 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_92, L_95, /*hidden argument*/NULL);
		if (!L_96)
		{
			goto IL_025e;
		}
	}
	{
		List_1_t1809589323 * L_97 = __this->get_shieldItems_6();
		int32_t L_98 = V_10;
		NullCheck(L_97);
		List_1_RemoveAt_m3735251282(L_97, L_98, /*hidden argument*/List_1_RemoveAt_m3735251282_MethodInfo_var);
	}

IL_025e:
	{
		int32_t L_99 = V_10;
		V_10 = ((int32_t)((int32_t)L_99+(int32_t)1));
	}

IL_0264:
	{
		int32_t L_100 = V_10;
		List_1_t1809589323 * L_101 = __this->get_shieldItems_6();
		NullCheck(L_101);
		int32_t L_102 = List_1_get_Count_m2506740058(L_101, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_100) < ((int32_t)L_102)))
		{
			goto IL_0239;
		}
	}

IL_0276:
	{
		Item_t2440468191 * L_103 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_104 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_103, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_104)
		{
			goto IL_032f;
		}
	}
	{
		InventoryPanel_t2677987186 * L_105 = __this->get_inventoryPanel_20();
		Item_t2440468191 * L_106 = V_4;
		NullCheck(L_105);
		InventoryPanel_AddItem_m2777303932(L_105, L_106, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_107 = __this->get_weaponHolder_13();
		NullCheck(L_107);
		RectTransform_t3349966182 * L_108 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_107, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_109 = L_108;
		NullCheck(L_109);
		Vector2_t2243707579  L_110 = RectTransform_get_sizeDelta_m2157326342(L_109, /*hidden argument*/NULL);
		Vector2_t2243707579  L_111;
		memset(&L_111, 0, sizeof(L_111));
		Vector2__ctor_m3067419446(&L_111, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_112 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_110, L_111, /*hidden argument*/NULL);
		NullCheck(L_109);
		RectTransform_set_sizeDelta_m2319668137(L_109, L_112, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_113 = __this->get_inventoryPanelEquip_18();
		GameObject_t1756533147 * L_114 = __this->get_engineHolder_16();
		NullCheck(L_114);
		Transform_t3275118058 * L_115 = GameObject_get_transform_m909382139(L_114, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_116 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_113, L_115, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_11 = L_116;
		GameObject_t1756533147 * L_117 = V_11;
		NullCheck(L_117);
		Transform_t3275118058 * L_118 = GameObject_get_transform_m909382139(L_117, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_119 = __this->get_engineHolder_16();
		NullCheck(L_119);
		Transform_t3275118058 * L_120 = GameObject_get_transform_m909382139(L_119, /*hidden argument*/NULL);
		NullCheck(L_118);
		Transform_SetParent_m4124909910(L_118, L_120, /*hidden argument*/NULL);
		V_12 = 0;
		goto IL_031d;
	}

IL_02f1:
	{
		Item_t2440468191 * L_121 = V_4;
		List_1_t1809589323 * L_122 = __this->get_engineItems_7();
		int32_t L_123 = V_12;
		NullCheck(L_122);
		Item_t2440468191 * L_124 = List_1_get_Item_m4027977911(L_122, L_123, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_125 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_121, L_124, /*hidden argument*/NULL);
		if (!L_125)
		{
			goto IL_0317;
		}
	}
	{
		List_1_t1809589323 * L_126 = __this->get_engineItems_7();
		int32_t L_127 = V_12;
		NullCheck(L_126);
		List_1_RemoveAt_m3735251282(L_126, L_127, /*hidden argument*/List_1_RemoveAt_m3735251282_MethodInfo_var);
	}

IL_0317:
	{
		int32_t L_128 = V_12;
		V_12 = ((int32_t)((int32_t)L_128+(int32_t)1));
	}

IL_031d:
	{
		int32_t L_129 = V_12;
		List_1_t1809589323 * L_130 = __this->get_engineItems_7();
		NullCheck(L_130);
		int32_t L_131 = List_1_get_Count_m2506740058(L_130, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_129) < ((int32_t)L_131)))
		{
			goto IL_02f1;
		}
	}

IL_032f:
	{
		int32_t L_132 = V_0;
		V_0 = ((int32_t)((int32_t)L_132+(int32_t)1));
	}

IL_0333:
	{
		int32_t L_133 = V_0;
		List_1_t1440998580 * L_134 = __this->get_inventoryByID_8();
		NullCheck(L_134);
		int32_t L_135 = List_1_get_Count_m852068579(L_134, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_133) < ((int32_t)L_135)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// Item ItemInventory::GetWeaponItem(System.Int32)
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const uint32_t ItemInventory_GetWeaponItem_m1346040764_MetadataUsageId;
extern "C"  Item_t2440468191 * ItemInventory_GetWeaponItem_m1346040764 (ItemInventory_t997429261 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_GetWeaponItem_m1346040764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0039;
	}

IL_0007:
	{
		ItemDatabase_t2502586666 * L_0 = __this->get_data_2();
		NullCheck(L_0);
		List_1_t1809589323 * L_1 = L_0->get_weaponsDB_2();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Item_t2440468191 * L_3 = List_1_get_Item_m4027977911(L_1, L_2, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_itemID_2();
		int32_t L_5 = ___id0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0035;
		}
	}
	{
		ItemDatabase_t2502586666 * L_6 = __this->get_data_2();
		NullCheck(L_6);
		List_1_t1809589323 * L_7 = L_6->get_weaponsDB_2();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		Item_t2440468191 * L_9 = List_1_get_Item_m4027977911(L_7, L_8, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		return L_9;
	}

IL_0035:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_11 = V_0;
		ItemDatabase_t2502586666 * L_12 = __this->get_data_2();
		NullCheck(L_12);
		List_1_t1809589323 * L_13 = L_12->get_weaponsDB_2();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m2506740058(L_13, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return (Item_t2440468191 *)NULL;
	}
}
// Item ItemInventory::GetArmorItem(System.Int32)
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const uint32_t ItemInventory_GetArmorItem_m820844179_MetadataUsageId;
extern "C"  Item_t2440468191 * ItemInventory_GetArmorItem_m820844179 (ItemInventory_t997429261 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_GetArmorItem_m820844179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0039;
	}

IL_0007:
	{
		ItemDatabase_t2502586666 * L_0 = __this->get_data_2();
		NullCheck(L_0);
		List_1_t1809589323 * L_1 = L_0->get_armorsDB_3();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Item_t2440468191 * L_3 = List_1_get_Item_m4027977911(L_1, L_2, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_itemID_2();
		int32_t L_5 = ___id0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0035;
		}
	}
	{
		ItemDatabase_t2502586666 * L_6 = __this->get_data_2();
		NullCheck(L_6);
		List_1_t1809589323 * L_7 = L_6->get_armorsDB_3();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		Item_t2440468191 * L_9 = List_1_get_Item_m4027977911(L_7, L_8, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		return L_9;
	}

IL_0035:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_11 = V_0;
		ItemDatabase_t2502586666 * L_12 = __this->get_data_2();
		NullCheck(L_12);
		List_1_t1809589323 * L_13 = L_12->get_armorsDB_3();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m2506740058(L_13, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return (Item_t2440468191 *)NULL;
	}
}
// Item ItemInventory::GetShieldItem(System.Int32)
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const uint32_t ItemInventory_GetShieldItem_m3227698875_MetadataUsageId;
extern "C"  Item_t2440468191 * ItemInventory_GetShieldItem_m3227698875 (ItemInventory_t997429261 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_GetShieldItem_m3227698875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0039;
	}

IL_0007:
	{
		ItemDatabase_t2502586666 * L_0 = __this->get_data_2();
		NullCheck(L_0);
		List_1_t1809589323 * L_1 = L_0->get_shieldsDB_4();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Item_t2440468191 * L_3 = List_1_get_Item_m4027977911(L_1, L_2, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_itemID_2();
		int32_t L_5 = ___id0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0035;
		}
	}
	{
		ItemDatabase_t2502586666 * L_6 = __this->get_data_2();
		NullCheck(L_6);
		List_1_t1809589323 * L_7 = L_6->get_shieldsDB_4();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		Item_t2440468191 * L_9 = List_1_get_Item_m4027977911(L_7, L_8, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		return L_9;
	}

IL_0035:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_11 = V_0;
		ItemDatabase_t2502586666 * L_12 = __this->get_data_2();
		NullCheck(L_12);
		List_1_t1809589323 * L_13 = L_12->get_shieldsDB_4();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m2506740058(L_13, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return (Item_t2440468191 *)NULL;
	}
}
// Item ItemInventory::GetEngineItem(System.Int32)
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const uint32_t ItemInventory_GetEngineItem_m2654585570_MetadataUsageId;
extern "C"  Item_t2440468191 * ItemInventory_GetEngineItem_m2654585570 (ItemInventory_t997429261 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_GetEngineItem_m2654585570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0039;
	}

IL_0007:
	{
		ItemDatabase_t2502586666 * L_0 = __this->get_data_2();
		NullCheck(L_0);
		List_1_t1809589323 * L_1 = L_0->get_enginesDB_5();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Item_t2440468191 * L_3 = List_1_get_Item_m4027977911(L_1, L_2, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_itemID_2();
		int32_t L_5 = ___id0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0035;
		}
	}
	{
		ItemDatabase_t2502586666 * L_6 = __this->get_data_2();
		NullCheck(L_6);
		List_1_t1809589323 * L_7 = L_6->get_enginesDB_5();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		Item_t2440468191 * L_9 = List_1_get_Item_m4027977911(L_7, L_8, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		return L_9;
	}

IL_0035:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_11 = V_0;
		ItemDatabase_t2502586666 * L_12 = __this->get_data_2();
		NullCheck(L_12);
		List_1_t1809589323 * L_13 = L_12->get_enginesDB_5();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m2506740058(L_13, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return (Item_t2440468191 *)NULL;
	}
}
// System.Void ItemInventory::SpawnMarketItems()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const uint32_t ItemInventory_SpawnMarketItems_m131373105_MetadataUsageId;
extern "C"  void ItemInventory_SpawnMarketItems_m131373105 (ItemInventory_t997429261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_SpawnMarketItems_m131373105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	int32_t V_2 = 0;
	GameObject_t1756533147 * V_3 = NULL;
	int32_t V_4 = 0;
	GameObject_t1756533147 * V_5 = NULL;
	int32_t V_6 = 0;
	GameObject_t1756533147 * V_7 = NULL;
	{
		V_0 = 0;
		goto IL_0079;
	}

IL_0007:
	{
		InventoryPanel_t2677987186 * L_0 = __this->get_marketInventoryPanel_19();
		List_1_t1809589323 * L_1 = __this->get_weaponItems_4();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Item_t2440468191 * L_3 = List_1_get_Item_m4027977911(L_1, L_2, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_0);
		InventoryPanel_AddItem_m2777303932(L_0, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_weaponsSlotMenu_9();
		NullCheck(L_4);
		RectTransform_t3349966182 * L_5 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_4, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_6 = L_5;
		NullCheck(L_6);
		Vector2_t2243707579  L_7 = RectTransform_get_sizeDelta_m2157326342(L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3067419446(&L_8, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectTransform_set_sizeDelta_m2319668137(L_6, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = __this->get_inventoryPanelBuy_17();
		GameObject_t1756533147 * L_11 = __this->get_weaponsSlotMenu_9();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_13 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_1 = L_13;
		GameObject_t1756533147 * L_14 = V_1;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = __this->get_weaponsSlotMenu_9();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_SetParent_m4124909910(L_15, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_19 = V_0;
		List_1_t1809589323 * L_20 = __this->get_weaponItems_4();
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m2506740058(L_20, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0007;
		}
	}
	{
		V_2 = 0;
		goto IL_0103;
	}

IL_0091:
	{
		InventoryPanel_t2677987186 * L_22 = __this->get_marketInventoryPanel_19();
		List_1_t1809589323 * L_23 = __this->get_armorItems_5();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		Item_t2440468191 * L_25 = List_1_get_Item_m4027977911(L_23, L_24, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_22);
		InventoryPanel_AddItem_m2777303932(L_22, L_25, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_26 = __this->get_armorSlotMenu_10();
		NullCheck(L_26);
		RectTransform_t3349966182 * L_27 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_26, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_28 = L_27;
		NullCheck(L_28);
		Vector2_t2243707579  L_29 = RectTransform_get_sizeDelta_m2157326342(L_28, /*hidden argument*/NULL);
		Vector2_t2243707579  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector2__ctor_m3067419446(&L_30, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_31 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		RectTransform_set_sizeDelta_m2319668137(L_28, L_31, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_32 = __this->get_inventoryPanelBuy_17();
		GameObject_t1756533147 * L_33 = __this->get_armorSlotMenu_10();
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = GameObject_get_transform_m909382139(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_35 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_3 = L_35;
		GameObject_t1756533147 * L_36 = V_3;
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = GameObject_get_transform_m909382139(L_36, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_38 = __this->get_armorSlotMenu_10();
		NullCheck(L_38);
		Transform_t3275118058 * L_39 = GameObject_get_transform_m909382139(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_SetParent_m4124909910(L_37, L_39, /*hidden argument*/NULL);
		int32_t L_40 = V_2;
		V_2 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_0103:
	{
		int32_t L_41 = V_2;
		List_1_t1809589323 * L_42 = __this->get_armorItems_5();
		NullCheck(L_42);
		int32_t L_43 = List_1_get_Count_m2506740058(L_42, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_41) < ((int32_t)L_43)))
		{
			goto IL_0091;
		}
	}
	{
		V_4 = 0;
		goto IL_0193;
	}

IL_011c:
	{
		InventoryPanel_t2677987186 * L_44 = __this->get_marketInventoryPanel_19();
		List_1_t1809589323 * L_45 = __this->get_shieldItems_6();
		int32_t L_46 = V_4;
		NullCheck(L_45);
		Item_t2440468191 * L_47 = List_1_get_Item_m4027977911(L_45, L_46, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_44);
		InventoryPanel_AddItem_m2777303932(L_44, L_47, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_48 = __this->get_shieldSlotMenu_11();
		NullCheck(L_48);
		RectTransform_t3349966182 * L_49 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_48, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_50 = L_49;
		NullCheck(L_50);
		Vector2_t2243707579  L_51 = RectTransform_get_sizeDelta_m2157326342(L_50, /*hidden argument*/NULL);
		Vector2_t2243707579  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector2__ctor_m3067419446(&L_52, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_53 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		NullCheck(L_50);
		RectTransform_set_sizeDelta_m2319668137(L_50, L_53, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_54 = __this->get_inventoryPanelBuy_17();
		GameObject_t1756533147 * L_55 = __this->get_shieldSlotMenu_11();
		NullCheck(L_55);
		Transform_t3275118058 * L_56 = GameObject_get_transform_m909382139(L_55, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_57 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_54, L_56, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_5 = L_57;
		GameObject_t1756533147 * L_58 = V_5;
		NullCheck(L_58);
		Transform_t3275118058 * L_59 = GameObject_get_transform_m909382139(L_58, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_60 = __this->get_shieldSlotMenu_11();
		NullCheck(L_60);
		Transform_t3275118058 * L_61 = GameObject_get_transform_m909382139(L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		Transform_SetParent_m4124909910(L_59, L_61, /*hidden argument*/NULL);
		int32_t L_62 = V_4;
		V_4 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_0193:
	{
		int32_t L_63 = V_4;
		List_1_t1809589323 * L_64 = __this->get_shieldItems_6();
		NullCheck(L_64);
		int32_t L_65 = List_1_get_Count_m2506740058(L_64, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_63) < ((int32_t)L_65)))
		{
			goto IL_011c;
		}
	}
	{
		V_6 = 0;
		goto IL_0224;
	}

IL_01ad:
	{
		InventoryPanel_t2677987186 * L_66 = __this->get_marketInventoryPanel_19();
		List_1_t1809589323 * L_67 = __this->get_engineItems_7();
		int32_t L_68 = V_6;
		NullCheck(L_67);
		Item_t2440468191 * L_69 = List_1_get_Item_m4027977911(L_67, L_68, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_66);
		InventoryPanel_AddItem_m2777303932(L_66, L_69, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_70 = __this->get_engineSlotMenu_12();
		NullCheck(L_70);
		RectTransform_t3349966182 * L_71 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_70, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		RectTransform_t3349966182 * L_72 = L_71;
		NullCheck(L_72);
		Vector2_t2243707579  L_73 = RectTransform_get_sizeDelta_m2157326342(L_72, /*hidden argument*/NULL);
		Vector2_t2243707579  L_74;
		memset(&L_74, 0, sizeof(L_74));
		Vector2__ctor_m3067419446(&L_74, (0.0f), (320.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_75 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_73, L_74, /*hidden argument*/NULL);
		NullCheck(L_72);
		RectTransform_set_sizeDelta_m2319668137(L_72, L_75, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_76 = __this->get_inventoryPanelBuy_17();
		GameObject_t1756533147 * L_77 = __this->get_engineSlotMenu_12();
		NullCheck(L_77);
		Transform_t3275118058 * L_78 = GameObject_get_transform_m909382139(L_77, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_79 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_76, L_78, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_7 = L_79;
		GameObject_t1756533147 * L_80 = V_7;
		NullCheck(L_80);
		Transform_t3275118058 * L_81 = GameObject_get_transform_m909382139(L_80, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_82 = __this->get_engineSlotMenu_12();
		NullCheck(L_82);
		Transform_t3275118058 * L_83 = GameObject_get_transform_m909382139(L_82, /*hidden argument*/NULL);
		NullCheck(L_81);
		Transform_SetParent_m4124909910(L_81, L_83, /*hidden argument*/NULL);
		int32_t L_84 = V_6;
		V_6 = ((int32_t)((int32_t)L_84+(int32_t)1));
	}

IL_0224:
	{
		int32_t L_85 = V_6;
		List_1_t1809589323 * L_86 = __this->get_engineItems_7();
		NullCheck(L_86);
		int32_t L_87 = List_1_get_Count_m2506740058(L_86, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_85) < ((int32_t)L_87)))
		{
			goto IL_01ad;
		}
	}
	{
		return;
	}
}
// System.Void ItemInventory::SwitchEquipedItems(ShipsEquipment,Item)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern const uint32_t ItemInventory_SwitchEquipedItems_m1895146111_MetadataUsageId;
extern "C"  void ItemInventory_SwitchEquipedItems_m1895146111 (ItemInventory_t997429261 * __this, ShipsEquipment_t1003200637 * ___ship0, Item_t2440468191 * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_SwitchEquipedItems_m1895146111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	GameObject_t1756533147 * V_3 = NULL;
	{
		Item_t2440468191 * L_0 = ___item1;
		ItemInventory_SwitchEquipedItemsVisual_m1546765688(__this, L_0, /*hidden argument*/NULL);
		Item_t2440468191 * L_1 = ___item1;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_itemType_16();
		if (L_2)
		{
			goto IL_0077;
		}
	}
	{
		ShipsEquipment_t1003200637 * L_3 = ___ship0;
		NullCheck(L_3);
		Item_t2440468191 * L_4 = L_3->get_weapon_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0077;
		}
	}
	{
		InventoryPanel_t2677987186 * L_6 = __this->get_inventoryPanel_20();
		ShipsEquipment_t1003200637 * L_7 = ___ship0;
		NullCheck(L_7);
		Item_t2440468191 * L_8 = L_7->get_weapon_5();
		NullCheck(L_6);
		InventoryPanel_AddItem_m2777303932(L_6, L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = __this->get_inventoryPanelEquip_18();
		GameObject_t1756533147 * L_10 = __this->get_weaponHolder_13();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_12 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_0 = L_12;
		GameObject_t1756533147 * L_13 = V_0;
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = GameObject_get_transform_m909382139(L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = __this->get_weaponHolder_13();
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_SetParent_m4124909910(L_14, L_16, /*hidden argument*/NULL);
		List_1_t1440998580 * L_17 = __this->get_inventoryByID_8();
		ShipsEquipment_t1003200637 * L_18 = ___ship0;
		NullCheck(L_18);
		Item_t2440468191 * L_19 = L_18->get_weapon_5();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_itemID_2();
		NullCheck(L_17);
		List_1_Add_m2828939739(L_17, L_20, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
	}

IL_0077:
	{
		Item_t2440468191 * L_21 = ___item1;
		NullCheck(L_21);
		int32_t L_22 = L_21->get_itemType_16();
		if ((!(((uint32_t)L_22) == ((uint32_t)2))))
		{
			goto IL_00e8;
		}
	}
	{
		ShipsEquipment_t1003200637 * L_23 = ___ship0;
		NullCheck(L_23);
		Item_t2440468191 * L_24 = L_23->get_armor_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_24, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e8;
		}
	}
	{
		InventoryPanel_t2677987186 * L_26 = __this->get_inventoryPanel_20();
		ShipsEquipment_t1003200637 * L_27 = ___ship0;
		NullCheck(L_27);
		Item_t2440468191 * L_28 = L_27->get_armor_6();
		NullCheck(L_26);
		InventoryPanel_AddItem_m2777303932(L_26, L_28, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_29 = __this->get_inventoryPanelEquip_18();
		GameObject_t1756533147 * L_30 = __this->get_armorHolder_14();
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = GameObject_get_transform_m909382139(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_32 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_29, L_31, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_1 = L_32;
		GameObject_t1756533147 * L_33 = V_1;
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = GameObject_get_transform_m909382139(L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = __this->get_armorHolder_14();
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_SetParent_m4124909910(L_34, L_36, /*hidden argument*/NULL);
		List_1_t1440998580 * L_37 = __this->get_inventoryByID_8();
		ShipsEquipment_t1003200637 * L_38 = ___ship0;
		NullCheck(L_38);
		Item_t2440468191 * L_39 = L_38->get_armor_6();
		NullCheck(L_39);
		int32_t L_40 = L_39->get_itemID_2();
		NullCheck(L_37);
		List_1_Add_m2828939739(L_37, L_40, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
	}

IL_00e8:
	{
		Item_t2440468191 * L_41 = ___item1;
		NullCheck(L_41);
		int32_t L_42 = L_41->get_itemType_16();
		if ((!(((uint32_t)L_42) == ((uint32_t)1))))
		{
			goto IL_0159;
		}
	}
	{
		ShipsEquipment_t1003200637 * L_43 = ___ship0;
		NullCheck(L_43);
		Item_t2440468191 * L_44 = L_43->get_shield_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_45 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_44, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0159;
		}
	}
	{
		InventoryPanel_t2677987186 * L_46 = __this->get_inventoryPanel_20();
		ShipsEquipment_t1003200637 * L_47 = ___ship0;
		NullCheck(L_47);
		Item_t2440468191 * L_48 = L_47->get_shield_7();
		NullCheck(L_46);
		InventoryPanel_AddItem_m2777303932(L_46, L_48, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_49 = __this->get_inventoryPanelEquip_18();
		GameObject_t1756533147 * L_50 = __this->get_shieldHolder_15();
		NullCheck(L_50);
		Transform_t3275118058 * L_51 = GameObject_get_transform_m909382139(L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_52 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_49, L_51, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_2 = L_52;
		GameObject_t1756533147 * L_53 = V_2;
		NullCheck(L_53);
		Transform_t3275118058 * L_54 = GameObject_get_transform_m909382139(L_53, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_55 = __this->get_shieldHolder_15();
		NullCheck(L_55);
		Transform_t3275118058 * L_56 = GameObject_get_transform_m909382139(L_55, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_SetParent_m4124909910(L_54, L_56, /*hidden argument*/NULL);
		List_1_t1440998580 * L_57 = __this->get_inventoryByID_8();
		ShipsEquipment_t1003200637 * L_58 = ___ship0;
		NullCheck(L_58);
		Item_t2440468191 * L_59 = L_58->get_shield_7();
		NullCheck(L_59);
		int32_t L_60 = L_59->get_itemID_2();
		NullCheck(L_57);
		List_1_Add_m2828939739(L_57, L_60, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
	}

IL_0159:
	{
		Item_t2440468191 * L_61 = ___item1;
		NullCheck(L_61);
		int32_t L_62 = L_61->get_itemType_16();
		if ((!(((uint32_t)L_62) == ((uint32_t)3))))
		{
			goto IL_01ca;
		}
	}
	{
		ShipsEquipment_t1003200637 * L_63 = ___ship0;
		NullCheck(L_63);
		Item_t2440468191 * L_64 = L_63->get_engine_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_65 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_64, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_01ca;
		}
	}
	{
		InventoryPanel_t2677987186 * L_66 = __this->get_inventoryPanel_20();
		ShipsEquipment_t1003200637 * L_67 = ___ship0;
		NullCheck(L_67);
		Item_t2440468191 * L_68 = L_67->get_engine_8();
		NullCheck(L_66);
		InventoryPanel_AddItem_m2777303932(L_66, L_68, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_69 = __this->get_inventoryPanelEquip_18();
		GameObject_t1756533147 * L_70 = __this->get_engineHolder_16();
		NullCheck(L_70);
		Transform_t3275118058 * L_71 = GameObject_get_transform_m909382139(L_70, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_72 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_69, L_71, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_3 = L_72;
		GameObject_t1756533147 * L_73 = V_3;
		NullCheck(L_73);
		Transform_t3275118058 * L_74 = GameObject_get_transform_m909382139(L_73, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_75 = __this->get_engineHolder_16();
		NullCheck(L_75);
		Transform_t3275118058 * L_76 = GameObject_get_transform_m909382139(L_75, /*hidden argument*/NULL);
		NullCheck(L_74);
		Transform_SetParent_m4124909910(L_74, L_76, /*hidden argument*/NULL);
		List_1_t1440998580 * L_77 = __this->get_inventoryByID_8();
		ShipsEquipment_t1003200637 * L_78 = ___ship0;
		NullCheck(L_78);
		Item_t2440468191 * L_79 = L_78->get_engine_8();
		NullCheck(L_79);
		int32_t L_80 = L_79->get_itemID_2();
		NullCheck(L_77);
		List_1_Add_m2828939739(L_77, L_80, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
	}

IL_01ca:
	{
		return;
	}
}
// System.Void ItemInventory::InstantiateWeapon()
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2727922973_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m283942784_MethodInfo_var;
extern const uint32_t ItemInventory_InstantiateWeapon_m2838280588_MetadataUsageId;
extern "C"  void ItemInventory_InstantiateWeapon_m2838280588 (ItemInventory_t997429261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_InstantiateWeapon_m2838280588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	GameObject_t1756533147 * V_3 = NULL;
	GameObject_t1756533147 * V_4 = NULL;
	{
		V_0 = 0;
		goto IL_01e9;
	}

IL_0007:
	{
		GameController_t3607102586 * L_0 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_shipID_3();
		List_1_t372321769 * L_2 = __this->get_shipsEquipment_3();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		ShipsEquipment_t1003200637 * L_4 = List_1_get_Item_m2727922973(L_2, L_3, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_shipID_3();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_5))))
		{
			goto IL_01e5;
		}
	}
	{
		List_1_t372321769 * L_6 = __this->get_shipsEquipment_3();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		ShipsEquipment_t1003200637 * L_8 = List_1_get_Item_m2727922973(L_6, L_7, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_8);
		Item_t2440468191 * L_9 = L_8->get_weapon_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0096;
		}
	}
	{
		List_1_t372321769 * L_11 = __this->get_shipsEquipment_3();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		ShipsEquipment_t1003200637 * L_13 = List_1_get_Item_m2727922973(L_11, L_12, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_13);
		Item_t2440468191 * L_14 = L_13->get_weapon_5();
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = L_14->get_itemVisual_13();
		ShipsController_t970893927 * L_16 = __this->get_shipController_21();
		NullCheck(L_16);
		List_1_t1125654279 * L_17 = L_16->get_purchasedShips_4();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		GameObject_t1756533147 * L_19 = List_1_get_Item_m939767277(L_17, L_18, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_19);
		Transform_t3275118058 * L_20 = GameObject_get_transform_m909382139(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_21 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_15, L_20, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_1 = L_21;
		GameObject_t1756533147 * L_22 = V_1;
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = GameObject_get_transform_m909382139(L_22, /*hidden argument*/NULL);
		ShipsController_t970893927 * L_24 = __this->get_shipController_21();
		NullCheck(L_24);
		List_1_t1125654279 * L_25 = L_24->get_purchasedShips_4();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		GameObject_t1756533147 * L_27 = List_1_get_Item_m939767277(L_25, L_26, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = GameObject_get_transform_m909382139(L_27, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_SetParent_m4124909910(L_23, L_28, /*hidden argument*/NULL);
	}

IL_0096:
	{
		List_1_t372321769 * L_29 = __this->get_shipsEquipment_3();
		int32_t L_30 = V_0;
		NullCheck(L_29);
		ShipsEquipment_t1003200637 * L_31 = List_1_get_Item_m2727922973(L_29, L_30, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_31);
		Item_t2440468191 * L_32 = L_31->get_armor_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_32, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0105;
		}
	}
	{
		List_1_t372321769 * L_34 = __this->get_shipsEquipment_3();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		ShipsEquipment_t1003200637 * L_36 = List_1_get_Item_m2727922973(L_34, L_35, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_36);
		Item_t2440468191 * L_37 = L_36->get_armor_6();
		NullCheck(L_37);
		GameObject_t1756533147 * L_38 = L_37->get_itemVisual_13();
		ShipsController_t970893927 * L_39 = __this->get_shipController_21();
		NullCheck(L_39);
		List_1_t1125654279 * L_40 = L_39->get_purchasedShips_4();
		int32_t L_41 = V_0;
		NullCheck(L_40);
		GameObject_t1756533147 * L_42 = List_1_get_Item_m939767277(L_40, L_41, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_42);
		Transform_t3275118058 * L_43 = GameObject_get_transform_m909382139(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_44 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_38, L_43, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_2 = L_44;
		GameObject_t1756533147 * L_45 = V_2;
		NullCheck(L_45);
		Transform_t3275118058 * L_46 = GameObject_get_transform_m909382139(L_45, /*hidden argument*/NULL);
		ShipsController_t970893927 * L_47 = __this->get_shipController_21();
		NullCheck(L_47);
		List_1_t1125654279 * L_48 = L_47->get_purchasedShips_4();
		int32_t L_49 = V_0;
		NullCheck(L_48);
		GameObject_t1756533147 * L_50 = List_1_get_Item_m939767277(L_48, L_49, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_50);
		Transform_t3275118058 * L_51 = GameObject_get_transform_m909382139(L_50, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_SetParent_m4124909910(L_46, L_51, /*hidden argument*/NULL);
	}

IL_0105:
	{
		List_1_t372321769 * L_52 = __this->get_shipsEquipment_3();
		int32_t L_53 = V_0;
		NullCheck(L_52);
		ShipsEquipment_t1003200637 * L_54 = List_1_get_Item_m2727922973(L_52, L_53, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_54);
		Item_t2440468191 * L_55 = L_54->get_shield_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_56 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_55, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_0174;
		}
	}
	{
		List_1_t372321769 * L_57 = __this->get_shipsEquipment_3();
		int32_t L_58 = V_0;
		NullCheck(L_57);
		ShipsEquipment_t1003200637 * L_59 = List_1_get_Item_m2727922973(L_57, L_58, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_59);
		Item_t2440468191 * L_60 = L_59->get_shield_7();
		NullCheck(L_60);
		GameObject_t1756533147 * L_61 = L_60->get_itemVisual_13();
		ShipsController_t970893927 * L_62 = __this->get_shipController_21();
		NullCheck(L_62);
		List_1_t1125654279 * L_63 = L_62->get_purchasedShips_4();
		int32_t L_64 = V_0;
		NullCheck(L_63);
		GameObject_t1756533147 * L_65 = List_1_get_Item_m939767277(L_63, L_64, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_65);
		Transform_t3275118058 * L_66 = GameObject_get_transform_m909382139(L_65, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_67 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_61, L_66, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_3 = L_67;
		GameObject_t1756533147 * L_68 = V_3;
		NullCheck(L_68);
		Transform_t3275118058 * L_69 = GameObject_get_transform_m909382139(L_68, /*hidden argument*/NULL);
		ShipsController_t970893927 * L_70 = __this->get_shipController_21();
		NullCheck(L_70);
		List_1_t1125654279 * L_71 = L_70->get_purchasedShips_4();
		int32_t L_72 = V_0;
		NullCheck(L_71);
		GameObject_t1756533147 * L_73 = List_1_get_Item_m939767277(L_71, L_72, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_73);
		Transform_t3275118058 * L_74 = GameObject_get_transform_m909382139(L_73, /*hidden argument*/NULL);
		NullCheck(L_69);
		Transform_SetParent_m4124909910(L_69, L_74, /*hidden argument*/NULL);
	}

IL_0174:
	{
		List_1_t372321769 * L_75 = __this->get_shipsEquipment_3();
		int32_t L_76 = V_0;
		NullCheck(L_75);
		ShipsEquipment_t1003200637 * L_77 = List_1_get_Item_m2727922973(L_75, L_76, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_77);
		Item_t2440468191 * L_78 = L_77->get_engine_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_79 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_78, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_01e5;
		}
	}
	{
		List_1_t372321769 * L_80 = __this->get_shipsEquipment_3();
		int32_t L_81 = V_0;
		NullCheck(L_80);
		ShipsEquipment_t1003200637 * L_82 = List_1_get_Item_m2727922973(L_80, L_81, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_82);
		Item_t2440468191 * L_83 = L_82->get_engine_8();
		NullCheck(L_83);
		GameObject_t1756533147 * L_84 = L_83->get_itemVisual_13();
		ShipsController_t970893927 * L_85 = __this->get_shipController_21();
		NullCheck(L_85);
		List_1_t1125654279 * L_86 = L_85->get_purchasedShips_4();
		int32_t L_87 = V_0;
		NullCheck(L_86);
		GameObject_t1756533147 * L_88 = List_1_get_Item_m939767277(L_86, L_87, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_88);
		Transform_t3275118058 * L_89 = GameObject_get_transform_m909382139(L_88, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_90 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_84, L_89, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_4 = L_90;
		GameObject_t1756533147 * L_91 = V_4;
		NullCheck(L_91);
		Transform_t3275118058 * L_92 = GameObject_get_transform_m909382139(L_91, /*hidden argument*/NULL);
		ShipsController_t970893927 * L_93 = __this->get_shipController_21();
		NullCheck(L_93);
		List_1_t1125654279 * L_94 = L_93->get_purchasedShips_4();
		int32_t L_95 = V_0;
		NullCheck(L_94);
		GameObject_t1756533147 * L_96 = List_1_get_Item_m939767277(L_94, L_95, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_96);
		Transform_t3275118058 * L_97 = GameObject_get_transform_m909382139(L_96, /*hidden argument*/NULL);
		NullCheck(L_92);
		Transform_SetParent_m4124909910(L_92, L_97, /*hidden argument*/NULL);
	}

IL_01e5:
	{
		int32_t L_98 = V_0;
		V_0 = ((int32_t)((int32_t)L_98+(int32_t)1));
	}

IL_01e9:
	{
		int32_t L_99 = V_0;
		List_1_t372321769 * L_100 = __this->get_shipsEquipment_3();
		NullCheck(L_100);
		int32_t L_101 = List_1_get_Count_m283942784(L_100, /*hidden argument*/List_1_get_Count_m283942784_MethodInfo_var);
		if ((((int32_t)L_99) < ((int32_t)L_101)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ItemInventory::SwitchEquipedItemsVisual(Item)
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2727922973_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m283942784_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral948549331;
extern Il2CppCodeGenString* _stringLiteral514865510;
extern Il2CppCodeGenString* _stringLiteral1530155656;
extern Il2CppCodeGenString* _stringLiteral2511249851;
extern const uint32_t ItemInventory_SwitchEquipedItemsVisual_m1546765688_MetadataUsageId;
extern "C"  void ItemInventory_SwitchEquipedItemsVisual_m1546765688 (ItemInventory_t997429261 * __this, Item_t2440468191 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemInventory_SwitchEquipedItemsVisual_m1546765688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	GameObject_t1756533147 * V_3 = NULL;
	GameObject_t1756533147 * V_4 = NULL;
	GameObject_t1756533147 * V_5 = NULL;
	GameObject_t1756533147 * V_6 = NULL;
	GameObject_t1756533147 * V_7 = NULL;
	GameObject_t1756533147 * V_8 = NULL;
	{
		V_0 = 0;
		goto IL_0224;
	}

IL_0007:
	{
		GameController_t3607102586 * L_0 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_shipID_3();
		List_1_t372321769 * L_2 = __this->get_shipsEquipment_3();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		ShipsEquipment_t1003200637 * L_4 = List_1_get_Item_m2727922973(L_2, L_3, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_shipID_3();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_5))))
		{
			goto IL_0220;
		}
	}
	{
		Item_t2440468191 * L_6 = ___item0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_itemType_16();
		if (L_7)
		{
			goto IL_00a2;
		}
	}
	{
		List_1_t372321769 * L_8 = __this->get_shipsEquipment_3();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		ShipsEquipment_t1003200637 * L_10 = List_1_get_Item_m2727922973(L_8, L_9, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_10);
		Item_t2440468191 * L_11 = L_10->get_weapon_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_11, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005f;
		}
	}
	{
		GameObject_t1756533147 * L_13 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral948549331, /*hidden argument*/NULL);
		V_1 = L_13;
		GameObject_t1756533147 * L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_005f:
	{
		Item_t2440468191 * L_15 = ___item0;
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = L_15->get_itemVisual_13();
		ShipsController_t970893927 * L_17 = __this->get_shipController_21();
		NullCheck(L_17);
		List_1_t1125654279 * L_18 = L_17->get_purchasedShips_4();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		GameObject_t1756533147 * L_20 = List_1_get_Item_m939767277(L_18, L_19, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_22 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_16, L_21, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_2 = L_22;
		GameObject_t1756533147 * L_23 = V_2;
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		ShipsController_t970893927 * L_25 = __this->get_shipController_21();
		NullCheck(L_25);
		List_1_t1125654279 * L_26 = L_25->get_purchasedShips_4();
		int32_t L_27 = V_0;
		NullCheck(L_26);
		GameObject_t1756533147 * L_28 = List_1_get_Item_m939767277(L_26, L_27, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = GameObject_get_transform_m909382139(L_28, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_SetParent_m4124909910(L_24, L_29, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		Item_t2440468191 * L_30 = ___item0;
		NullCheck(L_30);
		int32_t L_31 = L_30->get_itemType_16();
		if ((!(((uint32_t)L_31) == ((uint32_t)2))))
		{
			goto IL_0120;
		}
	}
	{
		List_1_t372321769 * L_32 = __this->get_shipsEquipment_3();
		int32_t L_33 = V_0;
		NullCheck(L_32);
		ShipsEquipment_t1003200637 * L_34 = List_1_get_Item_m2727922973(L_32, L_33, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_34);
		Item_t2440468191 * L_35 = L_34->get_armor_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_35, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00db;
		}
	}
	{
		GameObject_t1756533147 * L_37 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral514865510, /*hidden argument*/NULL);
		V_3 = L_37;
		GameObject_t1756533147 * L_38 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
	}

IL_00db:
	{
		Item_t2440468191 * L_39 = ___item0;
		NullCheck(L_39);
		GameObject_t1756533147 * L_40 = L_39->get_itemVisual_13();
		ShipsController_t970893927 * L_41 = __this->get_shipController_21();
		NullCheck(L_41);
		List_1_t1125654279 * L_42 = L_41->get_purchasedShips_4();
		int32_t L_43 = V_0;
		NullCheck(L_42);
		GameObject_t1756533147 * L_44 = List_1_get_Item_m939767277(L_42, L_43, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_44);
		Transform_t3275118058 * L_45 = GameObject_get_transform_m909382139(L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_46 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_40, L_45, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_4 = L_46;
		GameObject_t1756533147 * L_47 = V_4;
		NullCheck(L_47);
		Transform_t3275118058 * L_48 = GameObject_get_transform_m909382139(L_47, /*hidden argument*/NULL);
		ShipsController_t970893927 * L_49 = __this->get_shipController_21();
		NullCheck(L_49);
		List_1_t1125654279 * L_50 = L_49->get_purchasedShips_4();
		int32_t L_51 = V_0;
		NullCheck(L_50);
		GameObject_t1756533147 * L_52 = List_1_get_Item_m939767277(L_50, L_51, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_52);
		Transform_t3275118058 * L_53 = GameObject_get_transform_m909382139(L_52, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_SetParent_m4124909910(L_48, L_53, /*hidden argument*/NULL);
	}

IL_0120:
	{
		Item_t2440468191 * L_54 = ___item0;
		NullCheck(L_54);
		int32_t L_55 = L_54->get_itemType_16();
		if ((!(((uint32_t)L_55) == ((uint32_t)1))))
		{
			goto IL_01a0;
		}
	}
	{
		List_1_t372321769 * L_56 = __this->get_shipsEquipment_3();
		int32_t L_57 = V_0;
		NullCheck(L_56);
		ShipsEquipment_t1003200637 * L_58 = List_1_get_Item_m2727922973(L_56, L_57, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_58);
		Item_t2440468191 * L_59 = L_58->get_shield_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_60 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_59, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_015b;
		}
	}
	{
		GameObject_t1756533147 * L_61 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral1530155656, /*hidden argument*/NULL);
		V_5 = L_61;
		GameObject_t1756533147 * L_62 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
	}

IL_015b:
	{
		Item_t2440468191 * L_63 = ___item0;
		NullCheck(L_63);
		GameObject_t1756533147 * L_64 = L_63->get_itemVisual_13();
		ShipsController_t970893927 * L_65 = __this->get_shipController_21();
		NullCheck(L_65);
		List_1_t1125654279 * L_66 = L_65->get_purchasedShips_4();
		int32_t L_67 = V_0;
		NullCheck(L_66);
		GameObject_t1756533147 * L_68 = List_1_get_Item_m939767277(L_66, L_67, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_68);
		Transform_t3275118058 * L_69 = GameObject_get_transform_m909382139(L_68, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_70 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_64, L_69, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_6 = L_70;
		GameObject_t1756533147 * L_71 = V_6;
		NullCheck(L_71);
		Transform_t3275118058 * L_72 = GameObject_get_transform_m909382139(L_71, /*hidden argument*/NULL);
		ShipsController_t970893927 * L_73 = __this->get_shipController_21();
		NullCheck(L_73);
		List_1_t1125654279 * L_74 = L_73->get_purchasedShips_4();
		int32_t L_75 = V_0;
		NullCheck(L_74);
		GameObject_t1756533147 * L_76 = List_1_get_Item_m939767277(L_74, L_75, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_76);
		Transform_t3275118058 * L_77 = GameObject_get_transform_m909382139(L_76, /*hidden argument*/NULL);
		NullCheck(L_72);
		Transform_SetParent_m4124909910(L_72, L_77, /*hidden argument*/NULL);
	}

IL_01a0:
	{
		Item_t2440468191 * L_78 = ___item0;
		NullCheck(L_78);
		int32_t L_79 = L_78->get_itemType_16();
		if ((!(((uint32_t)L_79) == ((uint32_t)3))))
		{
			goto IL_0220;
		}
	}
	{
		List_1_t372321769 * L_80 = __this->get_shipsEquipment_3();
		int32_t L_81 = V_0;
		NullCheck(L_80);
		ShipsEquipment_t1003200637 * L_82 = List_1_get_Item_m2727922973(L_80, L_81, /*hidden argument*/List_1_get_Item_m2727922973_MethodInfo_var);
		NullCheck(L_82);
		Item_t2440468191 * L_83 = L_82->get_engine_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_84 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_83, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_01db;
		}
	}
	{
		GameObject_t1756533147 * L_85 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral2511249851, /*hidden argument*/NULL);
		V_7 = L_85;
		GameObject_t1756533147 * L_86 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
	}

IL_01db:
	{
		Item_t2440468191 * L_87 = ___item0;
		NullCheck(L_87);
		GameObject_t1756533147 * L_88 = L_87->get_itemVisual_13();
		ShipsController_t970893927 * L_89 = __this->get_shipController_21();
		NullCheck(L_89);
		List_1_t1125654279 * L_90 = L_89->get_purchasedShips_4();
		int32_t L_91 = V_0;
		NullCheck(L_90);
		GameObject_t1756533147 * L_92 = List_1_get_Item_m939767277(L_90, L_91, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_92);
		Transform_t3275118058 * L_93 = GameObject_get_transform_m909382139(L_92, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_94 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_88, L_93, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_8 = L_94;
		GameObject_t1756533147 * L_95 = V_8;
		NullCheck(L_95);
		Transform_t3275118058 * L_96 = GameObject_get_transform_m909382139(L_95, /*hidden argument*/NULL);
		ShipsController_t970893927 * L_97 = __this->get_shipController_21();
		NullCheck(L_97);
		List_1_t1125654279 * L_98 = L_97->get_purchasedShips_4();
		int32_t L_99 = V_0;
		NullCheck(L_98);
		GameObject_t1756533147 * L_100 = List_1_get_Item_m939767277(L_98, L_99, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_100);
		Transform_t3275118058 * L_101 = GameObject_get_transform_m909382139(L_100, /*hidden argument*/NULL);
		NullCheck(L_96);
		Transform_SetParent_m4124909910(L_96, L_101, /*hidden argument*/NULL);
	}

IL_0220:
	{
		int32_t L_102 = V_0;
		V_0 = ((int32_t)((int32_t)L_102+(int32_t)1));
	}

IL_0224:
	{
		int32_t L_103 = V_0;
		List_1_t372321769 * L_104 = __this->get_shipsEquipment_3();
		NullCheck(L_104);
		int32_t L_105 = List_1_get_Count_m283942784(L_104, /*hidden argument*/List_1_get_Count_m283942784_MethodInfo_var);
		if ((((int32_t)L_103) < ((int32_t)L_105)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void MainMenu::.ctor()
extern "C"  void MainMenu__ctor_m2536434407 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::MarketPlaceMenuOpen()
extern "C"  void MainMenu_MarketPlaceMenuOpen_m4090598755 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_marketPlaceMenu_5();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::MarketPlaceMenuClose()
extern "C"  void MainMenu_MarketPlaceMenuClose_m2315653753 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_marketPlaceMenu_5();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::InventoryMenuOpen()
extern Il2CppCodeGenString* _stringLiteral3702756701;
extern Il2CppCodeGenString* _stringLiteral4194455590;
extern const uint32_t MainMenu_InventoryMenuOpen_m3098346478_MetadataUsageId;
extern "C"  void MainMenu_InventoryMenuOpen_m3098346478 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_InventoryMenuOpen_m3098346478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_camInventoryAnim_15();
		NullCheck(L_0);
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral3702756701, /*hidden argument*/NULL);
		Animator_t69676727 * L_1 = __this->get_inventoryAnim_16();
		NullCheck(L_1);
		Animator_SetTrigger_m3418492570(L_1, _stringLiteral4194455590, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::InventoryMenuClose()
extern Il2CppCodeGenString* _stringLiteral2646597787;
extern Il2CppCodeGenString* _stringLiteral10157858;
extern const uint32_t MainMenu_InventoryMenuClose_m882318020_MetadataUsageId;
extern "C"  void MainMenu_InventoryMenuClose_m882318020 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_InventoryMenuClose_m882318020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_camInventoryAnim_15();
		NullCheck(L_0);
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral2646597787, /*hidden argument*/NULL);
		Animator_t69676727 * L_1 = __this->get_inventoryAnim_16();
		NullCheck(L_1);
		Animator_SetTrigger_m3418492570(L_1, _stringLiteral10157858, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::AbilityMenuOpen()
extern "C"  void MainMenu_AbilityMenuOpen_m3358290612 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_abilityMenu_3();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::AbilityMenuClose()
extern "C"  void MainMenu_AbilityMenuClose_m1703085502 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_abilityMenu_3();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::MissionMenuOpen()
extern "C"  void MainMenu_MissionMenuOpen_m4198987900 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_missionMenu_4();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::MissionMenuClose()
extern "C"  void MainMenu_MissionMenuClose_m2505153874 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_missionMenu_4();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::WeaponInventoryMenuOpen()
extern "C"  void MainMenu_WeaponInventoryMenuOpen_m1494163322 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_weaponInventoryMenu_6();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::WeaponInventoryMenuClose()
extern "C"  void MainMenu_WeaponInventoryMenuClose_m1434171012 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_weaponInventoryMenu_6();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::ShieldInventoryMenuOpen()
extern "C"  void MainMenu_ShieldInventoryMenuOpen_m1653227129 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_shieldInventoryMenu_7();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::ShieldInventoryMenuClose()
extern "C"  void MainMenu_ShieldInventoryMenuClose_m1486938927 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_shieldInventoryMenu_7();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::ArmorInventoryMenuOpen()
extern "C"  void MainMenu_ArmorInventoryMenuOpen_m197611509 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_armorInventoryMenu_8();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::ArmorInventoryMenuClose()
extern "C"  void MainMenu_ArmorInventoryMenuClose_m1327901971 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_armorInventoryMenu_8();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::EngineInventoryMenuOpen()
extern "C"  void MainMenu_EngineInventoryMenuOpen_m339844472 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_engineInventoryMenu_9();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::EngineInventoryMenuClose()
extern "C"  void MainMenu_EngineInventoryMenuClose_m1809813282 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_engineInventoryMenu_9();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::ShipsSlotMenuOpen()
extern "C"  void MainMenu_ShipsSlotMenuOpen_m1881846937 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_shipsSlotMenu_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_weaponsSlotMenu_11();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_armorSlotMenu_12();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_shieldSlotMenu_13();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_enginesSlotMenu_14();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::WeaponsSlotMenuOpen()
extern "C"  void MainMenu_WeaponsSlotMenuOpen_m991590537 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_shipsSlotMenu_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_weaponsSlotMenu_11();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_armorSlotMenu_12();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_shieldSlotMenu_13();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_enginesSlotMenu_14();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::ArmorSlotMenuOpen()
extern "C"  void MainMenu_ArmorSlotMenuOpen_m3294957115 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_shipsSlotMenu_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_weaponsSlotMenu_11();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_armorSlotMenu_12();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_shieldSlotMenu_13();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_enginesSlotMenu_14();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::ShieldSlotMenuOpen()
extern "C"  void MainMenu_ShieldSlotMenuOpen_m1171592391 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_shipsSlotMenu_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_weaponsSlotMenu_11();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_armorSlotMenu_12();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_shieldSlotMenu_13();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_enginesSlotMenu_14();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::EngnesSlotMenuOpen()
extern "C"  void MainMenu_EngnesSlotMenuOpen_m2975246424 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_shipsSlotMenu_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_weaponsSlotMenu_11();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_armorSlotMenu_12();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_shieldSlotMenu_13();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_enginesSlotMenu_14();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Marketplace::.ctor()
extern "C"  void Marketplace__ctor_m872510910 (Marketplace_t506058393 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Marketplace::Start()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisShipsDatabase_t1101834110_m3999776727_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2773528611_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4098767771;
extern Il2CppCodeGenString* _stringLiteral3975374171;
extern Il2CppCodeGenString* _stringLiteral2391892769;
extern Il2CppCodeGenString* _stringLiteral2240369535;
extern const uint32_t Marketplace_Start_m3630212126_MetadataUsageId;
extern "C"  void Marketplace_Start_m3630212126 (Marketplace_t506058393 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Marketplace_Start_m3630212126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral4098767771, /*hidden argument*/NULL);
		NullCheck(L_0);
		ShipsDatabase_t1101834110 * L_1 = GameObject_GetComponent_TisShipsDatabase_t1101834110_m3999776727(L_0, /*hidden argument*/GameObject_GetComponent_TisShipsDatabase_t1101834110_m3999776727_MethodInfo_var);
		__this->set_ships_2(L_1);
		Text_t356221433 * L_2 = __this->get_id_7();
		ShipsDatabase_t1101834110 * L_3 = __this->get_ships_2();
		NullCheck(L_3);
		List_1_t2004653347 * L_4 = L_3->get_dataBase_3();
		int32_t L_5 = __this->get_shipNumber_4();
		NullCheck(L_4);
		PlayerShip_t2635532215 * L_6 = List_1_get_Item_m2773528611(L_4, L_5, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_6);
		int32_t L_7 = L_6->get_shipID_0();
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3975374171, L_9, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_10);
		Text_t356221433 * L_11 = __this->get_shipName_5();
		ShipsDatabase_t1101834110 * L_12 = __this->get_ships_2();
		NullCheck(L_12);
		List_1_t2004653347 * L_13 = L_12->get_dataBase_3();
		int32_t L_14 = __this->get_shipNumber_4();
		NullCheck(L_13);
		PlayerShip_t2635532215 * L_15 = List_1_get_Item_m2773528611(L_13, L_14, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_15);
		String_t* L_16 = L_15->get_shipName_1();
		String_t* L_17 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2391892769, L_16, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_17);
		Text_t356221433 * L_18 = __this->get_price_6();
		ShipsDatabase_t1101834110 * L_19 = __this->get_ships_2();
		NullCheck(L_19);
		List_1_t2004653347 * L_20 = L_19->get_dataBase_3();
		int32_t L_21 = __this->get_shipNumber_4();
		NullCheck(L_20);
		PlayerShip_t2635532215 * L_22 = List_1_get_Item_m2773528611(L_20, L_21, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_22);
		int32_t L_23 = L_22->get_shipPrice_11();
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_24);
		String_t* L_26 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2240369535, L_25, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_26);
		ShipsDatabase_t1101834110 * L_27 = __this->get_ships_2();
		NullCheck(L_27);
		List_1_t2004653347 * L_28 = L_27->get_dataBase_3();
		int32_t L_29 = __this->get_shipNumber_4();
		NullCheck(L_28);
		PlayerShip_t2635532215 * L_30 = List_1_get_Item_m2773528611(L_28, L_29, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_30);
		bool L_31 = L_30->get_purchasedShip_10();
		if (!L_31)
		{
			goto IL_00db;
		}
	}
	{
		GameObject_t1756533147 * L_32 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		GameObject_SetActive_m2887581199(L_32, (bool)0, /*hidden argument*/NULL);
	}

IL_00db:
	{
		return;
	}
}
// System.Void Marketplace::BuyShip()
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2773528611_MethodInfo_var;
extern const uint32_t Marketplace_BuyShip_m3274103964_MetadataUsageId;
extern "C"  void Marketplace_BuyShip_m3274103964 (Marketplace_t506058393 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Marketplace_BuyShip_m3274103964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameController_t3607102586 * L_0 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_credits_4();
		ShipsDatabase_t1101834110 * L_2 = __this->get_ships_2();
		NullCheck(L_2);
		List_1_t2004653347 * L_3 = L_2->get_dataBase_3();
		int32_t L_4 = __this->get_shipNumber_4();
		NullCheck(L_3);
		PlayerShip_t2635532215 * L_5 = List_1_get_Item_m2773528611(L_3, L_4, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_shipPrice_11();
		if ((((int32_t)L_1) < ((int32_t)L_6)))
		{
			goto IL_0089;
		}
	}
	{
		GameController_t3607102586 * L_7 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		GameController_t3607102586 * L_8 = L_7;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_credits_4();
		ShipsDatabase_t1101834110 * L_10 = __this->get_ships_2();
		NullCheck(L_10);
		List_1_t2004653347 * L_11 = L_10->get_dataBase_3();
		int32_t L_12 = __this->get_shipNumber_4();
		NullCheck(L_11);
		PlayerShip_t2635532215 * L_13 = List_1_get_Item_m2773528611(L_11, L_12, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_13);
		int32_t L_14 = L_13->get_shipPrice_11();
		NullCheck(L_8);
		L_8->set_credits_4(((int32_t)((int32_t)L_9-(int32_t)L_14)));
		ShipsDatabase_t1101834110 * L_15 = __this->get_ships_2();
		NullCheck(L_15);
		List_1_t2004653347 * L_16 = L_15->get_dataBase_3();
		int32_t L_17 = __this->get_shipNumber_4();
		NullCheck(L_16);
		PlayerShip_t2635532215 * L_18 = List_1_get_Item_m2773528611(L_16, L_17, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_18);
		L_18->set_purchasedShip_10((bool)1);
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		GameObject_SetActive_m2887581199(L_19, (bool)0, /*hidden argument*/NULL);
		ShipsController_t970893927 * L_20 = __this->get_shipController_3();
		NullCheck(L_20);
		ShipsController_CheckPurchaseStatus_m2042212127(L_20, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void PlayerShip::.ctor(System.Int32,System.String,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Int32,System.String)
extern "C"  void PlayerShip__ctor_m2607274753 (PlayerShip_t2635532215 * __this, int32_t ___id0, String_t* ___name1, float ___armor2, float ___regenA3, float ___shield4, float ___regenS5, float ___speed6, float ___fireRate7, float ___adamage8, float ___sdamage9, bool ___purchased10, int32_t ___price11, String_t* ___type12, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___id0;
		__this->set_shipID_0(L_0);
		String_t* L_1 = ___name1;
		__this->set_shipName_1(L_1);
		float L_2 = ___armor2;
		__this->set_shipMaxArmor_2(L_2);
		float L_3 = ___regenA3;
		__this->set_shipRegenArmor_3(L_3);
		float L_4 = ___shield4;
		__this->set_shipMaxShield_4(L_4);
		float L_5 = ___regenS5;
		__this->set_shipRegenShield_5(L_5);
		float L_6 = ___speed6;
		__this->set_shipSpeed_6(L_6);
		float L_7 = ___fireRate7;
		__this->set_shipFireRate_7(L_7);
		float L_8 = ___adamage8;
		__this->set_armorDamage_8(L_8);
		float L_9 = ___sdamage9;
		__this->set_shieldDamage_9(L_9);
		bool L_10 = ___purchased10;
		__this->set_purchasedShip_10(L_10);
		int32_t L_11 = ___price11;
		__this->set_shipPrice_11(L_11);
		String_t* L_12 = ___type12;
		__this->set_shipType_12(L_12);
		return;
	}
}
// System.Void PlayerShip::.ctor()
extern "C"  void PlayerShip__ctor_m3901879582 (PlayerShip_t2635532215 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_shipID_0((-1));
		return;
	}
}
// System.Void ShipsController::.ctor()
extern Il2CppClass* List_1_t2004653347_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1289075548_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const uint32_t ShipsController__ctor_m1752701194_MetadataUsageId;
extern "C"  void ShipsController__ctor_m1752701194 (ShipsController_t970893927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsController__ctor_m1752701194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2004653347 * L_0 = (List_1_t2004653347 *)il2cpp_codegen_object_new(List_1_t2004653347_il2cpp_TypeInfo_var);
		List_1__ctor_m1289075548(L_0, /*hidden argument*/List_1__ctor_m1289075548_MethodInfo_var);
		__this->set_allShips_3(L_0);
		List_1_t1125654279 * L_1 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_1, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set_purchasedShips_4(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipsController::Start()
extern const MethodInfo* Component_GetComponent_TisItemInventory_t997429261_m281227978_MethodInfo_var;
extern const uint32_t ShipsController_Start_m462316142_MetadataUsageId;
extern "C"  void ShipsController_Start_m462316142 (ShipsController_t970893927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsController_Start_m462316142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ShipsController_CheckPurchaseStatus_m2042212127(__this, /*hidden argument*/NULL);
		ShipsController_SwapShips_m2468613320(__this, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_0 = __this->get_itemInventory_14();
		NullCheck(L_0);
		Component_GetComponent_TisItemInventory_t997429261_m281227978(L_0, /*hidden argument*/Component_GetComponent_TisItemInventory_t997429261_m281227978_MethodInfo_var);
		return;
	}
}
// System.Void ShipsController::Next()
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const uint32_t ShipsController_Next_m1809562893_MetadataUsageId;
extern "C"  void ShipsController_Next_m1809562893 (ShipsController_t970893927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsController_Next_m1809562893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_shipRotationNumber_13();
		__this->set_shipRotationNumber_13(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_shipRotationNumber_13();
		List_1_t1125654279 * L_2 = __this->get_purchasedShips_4();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m2764296230(L_2, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_002b;
		}
	}
	{
		__this->set_shipRotationNumber_13(0);
	}

IL_002b:
	{
		ShipsController_SwapShips_m2468613320(__this, /*hidden argument*/NULL);
		ShipsController_SendActivShipID_m3212050452(__this, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_4 = __this->get_itemInventory_14();
		NullCheck(L_4);
		ItemInventory_ChekEquipmentAtStartUp_m3977349727(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipsController::Previous()
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const uint32_t ShipsController_Previous_m1367434359_MetadataUsageId;
extern "C"  void ShipsController_Previous_m1367434359 (ShipsController_t970893927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsController_Previous_m1367434359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_shipRotationNumber_13();
		__this->set_shipRotationNumber_13(((int32_t)((int32_t)L_0-(int32_t)1)));
		int32_t L_1 = __this->get_shipRotationNumber_13();
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_002d;
		}
	}
	{
		List_1_t1125654279 * L_2 = __this->get_purchasedShips_4();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m2764296230(L_2, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		__this->set_shipRotationNumber_13(((int32_t)((int32_t)L_3-(int32_t)1)));
	}

IL_002d:
	{
		ShipsController_SwapShips_m2468613320(__this, /*hidden argument*/NULL);
		ShipsController_SendActivShipID_m3212050452(__this, /*hidden argument*/NULL);
		ItemInventory_t997429261 * L_4 = __this->get_itemInventory_14();
		NullCheck(L_4);
		ItemInventory_ChekEquipmentAtStartUp_m3977349727(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipsController::SwapShips()
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const uint32_t ShipsController_SwapShips_m2468613320_MetadataUsageId;
extern "C"  void ShipsController_SwapShips_m2468613320 (ShipsController_t970893927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsController_SwapShips_m2468613320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0029;
	}

IL_0007:
	{
		int32_t L_0 = __this->get_shipRotationNumber_13();
		int32_t L_1 = V_0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0025;
		}
	}
	{
		List_1_t1125654279 * L_2 = __this->get_purchasedShips_4();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		GameObject_t1756533147 * L_4 = List_1_get_Item_m939767277(L_2, L_3, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
	}

IL_0025:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_6 = V_0;
		List_1_t1125654279 * L_7 = __this->get_purchasedShips_4();
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m2764296230(L_7, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_9 = __this->get_shipRotationNumber_13();
		if (L_9)
		{
			goto IL_0058;
		}
	}
	{
		List_1_t1125654279 * L_10 = __this->get_purchasedShips_4();
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = List_1_get_Item_m939767277(L_10, 0, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_11);
		GameObject_SetActive_m2887581199(L_11, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_0058:
	{
		int32_t L_12 = __this->get_shipRotationNumber_13();
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_0077;
		}
	}
	{
		List_1_t1125654279 * L_13 = __this->get_purchasedShips_4();
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = List_1_get_Item_m939767277(L_13, 1, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_14);
		GameObject_SetActive_m2887581199(L_14, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_0077:
	{
		int32_t L_15 = __this->get_shipRotationNumber_13();
		if ((!(((uint32_t)L_15) == ((uint32_t)2))))
		{
			goto IL_0096;
		}
	}
	{
		List_1_t1125654279 * L_16 = __this->get_purchasedShips_4();
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = List_1_get_Item_m939767277(L_16, 2, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_0096:
	{
		int32_t L_18 = __this->get_shipRotationNumber_13();
		if ((!(((uint32_t)L_18) == ((uint32_t)3))))
		{
			goto IL_00b5;
		}
	}
	{
		List_1_t1125654279 * L_19 = __this->get_purchasedShips_4();
		NullCheck(L_19);
		GameObject_t1756533147 * L_20 = List_1_get_Item_m939767277(L_19, 3, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_00b5:
	{
		return;
	}
}
// System.Void ShipsController::CheckPurchaseStatus()
extern const MethodInfo* List_1_get_Item_m2773528611_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3476285208_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const uint32_t ShipsController_CheckPurchaseStatus_m2042212127_MetadataUsageId;
extern "C"  void ShipsController_CheckPurchaseStatus_m2042212127 (ShipsController_t970893927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsController_CheckPurchaseStatus_m2042212127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ShipsDatabase_t1101834110 * L_0 = __this->get_ships_2();
		NullCheck(L_0);
		List_1_t2004653347 * L_1 = L_0->get_dataBase_3();
		NullCheck(L_1);
		PlayerShip_t2635532215 * L_2 = List_1_get_Item_m2773528611(L_1, 0, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_2);
		bool L_3 = L_2->get_purchasedShip_10();
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		bool L_4 = __this->get_shipAdded0_5();
		if (L_4)
		{
			goto IL_005a;
		}
	}
	{
		List_1_t2004653347 * L_5 = __this->get_allShips_3();
		ShipsDatabase_t1101834110 * L_6 = __this->get_ships_2();
		NullCheck(L_6);
		List_1_t2004653347 * L_7 = L_6->get_dataBase_3();
		NullCheck(L_7);
		PlayerShip_t2635532215 * L_8 = List_1_get_Item_m2773528611(L_7, 0, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_5);
		List_1_Add_m3476285208(L_5, L_8, /*hidden argument*/List_1_Add_m3476285208_MethodInfo_var);
		List_1_t1125654279 * L_9 = __this->get_purchasedShips_4();
		GameObject_t1756533147 * L_10 = __this->get_ship0_9();
		NullCheck(L_9);
		List_1_Add_m3441471442(L_9, L_10, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		__this->set_shipAdded0_5((bool)1);
	}

IL_005a:
	{
		ShipsDatabase_t1101834110 * L_11 = __this->get_ships_2();
		NullCheck(L_11);
		List_1_t2004653347 * L_12 = L_11->get_dataBase_3();
		NullCheck(L_12);
		PlayerShip_t2635532215 * L_13 = List_1_get_Item_m2773528611(L_12, 1, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_13);
		bool L_14 = L_13->get_purchasedShip_10();
		if (!L_14)
		{
			goto IL_00b4;
		}
	}
	{
		bool L_15 = __this->get_shipAdded1_6();
		if (L_15)
		{
			goto IL_00b4;
		}
	}
	{
		List_1_t2004653347 * L_16 = __this->get_allShips_3();
		ShipsDatabase_t1101834110 * L_17 = __this->get_ships_2();
		NullCheck(L_17);
		List_1_t2004653347 * L_18 = L_17->get_dataBase_3();
		NullCheck(L_18);
		PlayerShip_t2635532215 * L_19 = List_1_get_Item_m2773528611(L_18, 1, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_16);
		List_1_Add_m3476285208(L_16, L_19, /*hidden argument*/List_1_Add_m3476285208_MethodInfo_var);
		List_1_t1125654279 * L_20 = __this->get_purchasedShips_4();
		GameObject_t1756533147 * L_21 = __this->get_ship1_10();
		NullCheck(L_20);
		List_1_Add_m3441471442(L_20, L_21, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		__this->set_shipAdded1_6((bool)1);
	}

IL_00b4:
	{
		ShipsDatabase_t1101834110 * L_22 = __this->get_ships_2();
		NullCheck(L_22);
		List_1_t2004653347 * L_23 = L_22->get_dataBase_3();
		NullCheck(L_23);
		PlayerShip_t2635532215 * L_24 = List_1_get_Item_m2773528611(L_23, 2, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_24);
		bool L_25 = L_24->get_purchasedShip_10();
		if (!L_25)
		{
			goto IL_010e;
		}
	}
	{
		bool L_26 = __this->get_shipAdded2_7();
		if (L_26)
		{
			goto IL_010e;
		}
	}
	{
		List_1_t2004653347 * L_27 = __this->get_allShips_3();
		ShipsDatabase_t1101834110 * L_28 = __this->get_ships_2();
		NullCheck(L_28);
		List_1_t2004653347 * L_29 = L_28->get_dataBase_3();
		NullCheck(L_29);
		PlayerShip_t2635532215 * L_30 = List_1_get_Item_m2773528611(L_29, 2, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_27);
		List_1_Add_m3476285208(L_27, L_30, /*hidden argument*/List_1_Add_m3476285208_MethodInfo_var);
		List_1_t1125654279 * L_31 = __this->get_purchasedShips_4();
		GameObject_t1756533147 * L_32 = __this->get_ship2_11();
		NullCheck(L_31);
		List_1_Add_m3441471442(L_31, L_32, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		__this->set_shipAdded2_7((bool)1);
	}

IL_010e:
	{
		ShipsDatabase_t1101834110 * L_33 = __this->get_ships_2();
		NullCheck(L_33);
		List_1_t2004653347 * L_34 = L_33->get_dataBase_3();
		NullCheck(L_34);
		PlayerShip_t2635532215 * L_35 = List_1_get_Item_m2773528611(L_34, 3, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_35);
		bool L_36 = L_35->get_purchasedShip_10();
		if (!L_36)
		{
			goto IL_0168;
		}
	}
	{
		bool L_37 = __this->get_shipAdded3_8();
		if (L_37)
		{
			goto IL_0168;
		}
	}
	{
		List_1_t2004653347 * L_38 = __this->get_allShips_3();
		ShipsDatabase_t1101834110 * L_39 = __this->get_ships_2();
		NullCheck(L_39);
		List_1_t2004653347 * L_40 = L_39->get_dataBase_3();
		NullCheck(L_40);
		PlayerShip_t2635532215 * L_41 = List_1_get_Item_m2773528611(L_40, 3, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_38);
		List_1_Add_m3476285208(L_38, L_41, /*hidden argument*/List_1_Add_m3476285208_MethodInfo_var);
		List_1_t1125654279 * L_42 = __this->get_purchasedShips_4();
		GameObject_t1756533147 * L_43 = __this->get_ship3_12();
		NullCheck(L_42);
		List_1_Add_m3441471442(L_42, L_43, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		__this->set_shipAdded3_8((bool)1);
	}

IL_0168:
	{
		return;
	}
}
// System.Void ShipsController::SendActivShipID()
extern Il2CppClass* GameController_t3607102586_il2cpp_TypeInfo_var;
extern const uint32_t ShipsController_SendActivShipID_m3212050452_MetadataUsageId;
extern "C"  void ShipsController_SendActivShipID_m3212050452 (ShipsController_t970893927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsController_SendActivShipID_m3212050452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_ship0_9();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m313590879(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		GameController_t3607102586 * L_2 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_2);
		L_2->set_shipID_3(0);
	}

IL_001b:
	{
		GameObject_t1756533147 * L_3 = __this->get_ship1_10();
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m313590879(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		GameController_t3607102586 * L_5 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_5);
		L_5->set_shipID_3(1);
	}

IL_0036:
	{
		GameObject_t1756533147 * L_6 = __this->get_ship2_11();
		NullCheck(L_6);
		bool L_7 = GameObject_get_activeSelf_m313590879(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		GameController_t3607102586 * L_8 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_8);
		L_8->set_shipID_3(2);
	}

IL_0051:
	{
		GameObject_t1756533147 * L_9 = __this->get_ship3_12();
		NullCheck(L_9);
		bool L_10 = GameObject_get_activeSelf_m313590879(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006c;
		}
	}
	{
		GameController_t3607102586 * L_11 = ((GameController_t3607102586_StaticFields*)GameController_t3607102586_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_11);
		L_11->set_shipID_3(3);
	}

IL_006c:
	{
		return;
	}
}
// System.Void ShipsDatabase::.ctor()
extern Il2CppClass* List_1_t2004653347_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1289075548_MethodInfo_var;
extern const uint32_t ShipsDatabase__ctor_m3331506775_MetadataUsageId;
extern "C"  void ShipsDatabase__ctor_m3331506775 (ShipsDatabase_t1101834110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsDatabase__ctor_m3331506775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2004653347 * L_0 = (List_1_t2004653347 *)il2cpp_codegen_object_new(List_1_t2004653347_il2cpp_TypeInfo_var);
		List_1__ctor_m1289075548(L_0, /*hidden argument*/List_1__ctor_m1289075548_MethodInfo_var);
		__this->set_dataBase_3(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipsDatabase::Start()
extern "C"  void ShipsDatabase_Start_m1770160195 (ShipsDatabase_t1101834110 * __this, const MethodInfo* method)
{
	{
		ShipsDatabase_LoadGame_m3185594747(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipsDatabase::SaveGame()
extern Il2CppClass* BinaryFormatter_t1866979105_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FileStream_t1695958676_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral522138953;
extern Il2CppCodeGenString* _stringLiteral3148397891;
extern const uint32_t ShipsDatabase_SaveGame_m2827057378_MetadataUsageId;
extern "C"  void ShipsDatabase_SaveGame_m2827057378 (ShipsDatabase_t1101834110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsDatabase_SaveGame_m2827057378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BinaryFormatter_t1866979105 * V_0 = NULL;
	BinaryFormatter_t1866979105 * V_1 = NULL;
	FileStream_t1695958676 * V_2 = NULL;
	{
		BinaryFormatter_t1866979105 * L_0 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		BinaryFormatter_t1866979105 * L_1 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, L_2, _stringLiteral522138953, /*hidden argument*/NULL);
		FileStream_t1695958676 * L_4 = (FileStream_t1695958676 *)il2cpp_codegen_object_new(FileStream_t1695958676_il2cpp_TypeInfo_var);
		FileStream__ctor_m785772645(L_4, L_3, 4, /*hidden argument*/NULL);
		V_2 = L_4;
		BinaryFormatter_t1866979105 * L_5 = V_0;
		FileStream_t1695958676 * L_6 = V_2;
		List_1_t2004653347 * L_7 = __this->get_dataBase_3();
		NullCheck(L_5);
		BinaryFormatter_Serialize_m433301673(L_5, L_6, L_7, /*hidden argument*/NULL);
		BinaryFormatter_t1866979105 * L_8 = V_1;
		FileStream_t1695958676 * L_9 = V_2;
		ItemInventory_t997429261 * L_10 = __this->get_inventory_2();
		NullCheck(L_10);
		List_1_t1440998580 * L_11 = L_10->get_inventoryByID_8();
		NullCheck(L_8);
		BinaryFormatter_Serialize_m433301673(L_8, L_9, L_11, /*hidden argument*/NULL);
		BinaryFormatter_t1866979105 * L_12 = V_1;
		FileStream_t1695958676 * L_13 = V_2;
		ItemInventory_t997429261 * L_14 = __this->get_inventory_2();
		NullCheck(L_14);
		ShipsEquipment_t1003200637 * L_15 = L_14->get_ship0Equipment_22();
		NullCheck(L_15);
		List_1_t1440998580 * L_16 = L_15->get_equipment_4();
		NullCheck(L_12);
		BinaryFormatter_Serialize_m433301673(L_12, L_13, L_16, /*hidden argument*/NULL);
		BinaryFormatter_t1866979105 * L_17 = V_1;
		FileStream_t1695958676 * L_18 = V_2;
		ItemInventory_t997429261 * L_19 = __this->get_inventory_2();
		NullCheck(L_19);
		ShipsEquipment_t1003200637 * L_20 = L_19->get_ship1Equipment_23();
		NullCheck(L_20);
		List_1_t1440998580 * L_21 = L_20->get_equipment_4();
		NullCheck(L_17);
		BinaryFormatter_Serialize_m433301673(L_17, L_18, L_21, /*hidden argument*/NULL);
		BinaryFormatter_t1866979105 * L_22 = V_1;
		FileStream_t1695958676 * L_23 = V_2;
		ItemInventory_t997429261 * L_24 = __this->get_inventory_2();
		NullCheck(L_24);
		ShipsEquipment_t1003200637 * L_25 = L_24->get_ship2Equipment_24();
		NullCheck(L_25);
		List_1_t1440998580 * L_26 = L_25->get_equipment_4();
		NullCheck(L_22);
		BinaryFormatter_Serialize_m433301673(L_22, L_23, L_26, /*hidden argument*/NULL);
		BinaryFormatter_t1866979105 * L_27 = V_1;
		FileStream_t1695958676 * L_28 = V_2;
		ItemInventory_t997429261 * L_29 = __this->get_inventory_2();
		NullCheck(L_29);
		ShipsEquipment_t1003200637 * L_30 = L_29->get_ship3Equipment_25();
		NullCheck(L_30);
		List_1_t1440998580 * L_31 = L_30->get_equipment_4();
		NullCheck(L_27);
		BinaryFormatter_Serialize_m433301673(L_27, L_28, L_31, /*hidden argument*/NULL);
		FileStream_t1695958676 * L_32 = V_2;
		NullCheck(L_32);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_32);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3148397891, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipsDatabase::LoadGame()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* BinaryFormatter_t1866979105_il2cpp_TypeInfo_var;
extern Il2CppClass* FileStream_t1695958676_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2004653347_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerShip_t2635532215_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m3476285208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral522138953;
extern Il2CppCodeGenString* _stringLiteral276530608;
extern Il2CppCodeGenString* _stringLiteral2031986231;
extern Il2CppCodeGenString* _stringLiteral2536003802;
extern Il2CppCodeGenString* _stringLiteral365127225;
extern Il2CppCodeGenString* _stringLiteral3732938296;
extern Il2CppCodeGenString* _stringLiteral2482320550;
extern Il2CppCodeGenString* _stringLiteral2620904;
extern Il2CppCodeGenString* _stringLiteral2795080817;
extern Il2CppCodeGenString* _stringLiteral3611346032;
extern Il2CppCodeGenString* _stringLiteral2364159610;
extern const uint32_t ShipsDatabase_LoadGame_m3185594747_MetadataUsageId;
extern "C"  void ShipsDatabase_LoadGame_m3185594747 (ShipsDatabase_t1101834110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsDatabase_LoadGame_m3185594747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BinaryFormatter_t1866979105 * V_0 = NULL;
	BinaryFormatter_t1866979105 * V_1 = NULL;
	FileStream_t1695958676 * V_2 = NULL;
	PlayerShip_t2635532215 * V_3 = NULL;
	PlayerShip_t2635532215 * V_4 = NULL;
	PlayerShip_t2635532215 * V_5 = NULL;
	PlayerShip_t2635532215 * V_6 = NULL;
	{
		String_t* L_0 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral522138953, /*hidden argument*/NULL);
		bool L_2 = File_Exists_m1685968367(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00e9;
		}
	}
	{
		BinaryFormatter_t1866979105 * L_3 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		BinaryFormatter_t1866979105 * L_4 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, L_5, _stringLiteral522138953, /*hidden argument*/NULL);
		FileStream_t1695958676 * L_7 = (FileStream_t1695958676 *)il2cpp_codegen_object_new(FileStream_t1695958676_il2cpp_TypeInfo_var);
		FileStream__ctor_m785772645(L_7, L_6, 3, /*hidden argument*/NULL);
		V_2 = L_7;
		BinaryFormatter_t1866979105 * L_8 = V_0;
		FileStream_t1695958676 * L_9 = V_2;
		NullCheck(L_8);
		Il2CppObject * L_10 = BinaryFormatter_Deserialize_m2771853471(L_8, L_9, /*hidden argument*/NULL);
		__this->set_dataBase_3(((List_1_t2004653347 *)CastclassClass(L_10, List_1_t2004653347_il2cpp_TypeInfo_var)));
		ItemInventory_t997429261 * L_11 = __this->get_inventory_2();
		BinaryFormatter_t1866979105 * L_12 = V_1;
		FileStream_t1695958676 * L_13 = V_2;
		NullCheck(L_12);
		Il2CppObject * L_14 = BinaryFormatter_Deserialize_m2771853471(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		L_11->set_inventoryByID_8(((List_1_t1440998580 *)CastclassClass(L_14, List_1_t1440998580_il2cpp_TypeInfo_var)));
		ItemInventory_t997429261 * L_15 = __this->get_inventory_2();
		NullCheck(L_15);
		ShipsEquipment_t1003200637 * L_16 = L_15->get_ship0Equipment_22();
		BinaryFormatter_t1866979105 * L_17 = V_1;
		FileStream_t1695958676 * L_18 = V_2;
		NullCheck(L_17);
		Il2CppObject * L_19 = BinaryFormatter_Deserialize_m2771853471(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		L_16->set_equipment_4(((List_1_t1440998580 *)CastclassClass(L_19, List_1_t1440998580_il2cpp_TypeInfo_var)));
		ItemInventory_t997429261 * L_20 = __this->get_inventory_2();
		NullCheck(L_20);
		ShipsEquipment_t1003200637 * L_21 = L_20->get_ship1Equipment_23();
		BinaryFormatter_t1866979105 * L_22 = V_1;
		FileStream_t1695958676 * L_23 = V_2;
		NullCheck(L_22);
		Il2CppObject * L_24 = BinaryFormatter_Deserialize_m2771853471(L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->set_equipment_4(((List_1_t1440998580 *)CastclassClass(L_24, List_1_t1440998580_il2cpp_TypeInfo_var)));
		ItemInventory_t997429261 * L_25 = __this->get_inventory_2();
		NullCheck(L_25);
		ShipsEquipment_t1003200637 * L_26 = L_25->get_ship2Equipment_24();
		BinaryFormatter_t1866979105 * L_27 = V_1;
		FileStream_t1695958676 * L_28 = V_2;
		NullCheck(L_27);
		Il2CppObject * L_29 = BinaryFormatter_Deserialize_m2771853471(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		L_26->set_equipment_4(((List_1_t1440998580 *)CastclassClass(L_29, List_1_t1440998580_il2cpp_TypeInfo_var)));
		ItemInventory_t997429261 * L_30 = __this->get_inventory_2();
		NullCheck(L_30);
		ShipsEquipment_t1003200637 * L_31 = L_30->get_ship3Equipment_25();
		BinaryFormatter_t1866979105 * L_32 = V_1;
		FileStream_t1695958676 * L_33 = V_2;
		NullCheck(L_32);
		Il2CppObject * L_34 = BinaryFormatter_Deserialize_m2771853471(L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		L_31->set_equipment_4(((List_1_t1440998580 *)CastclassClass(L_34, List_1_t1440998580_il2cpp_TypeInfo_var)));
		FileStream_t1695958676 * L_35 = V_2;
		NullCheck(L_35);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_35);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral276530608, /*hidden argument*/NULL);
		goto IL_0221;
	}

IL_00e9:
	{
		PlayerShip_t2635532215 * L_36 = (PlayerShip_t2635532215 *)il2cpp_codegen_object_new(PlayerShip_t2635532215_il2cpp_TypeInfo_var);
		PlayerShip__ctor_m2607274753(L_36, 0, _stringLiteral2031986231, (10.0f), (0.0f), (10.0f), (0.0f), (6.0f), (0.5f), (10.0f), (10.0f), (bool)1, 0, _stringLiteral2536003802, /*hidden argument*/NULL);
		V_3 = L_36;
		PlayerShip_t2635532215 * L_37 = (PlayerShip_t2635532215 *)il2cpp_codegen_object_new(PlayerShip_t2635532215_il2cpp_TypeInfo_var);
		PlayerShip__ctor_m2607274753(L_37, 1, _stringLiteral365127225, (20.0f), (0.0f), (10.0f), (0.0f), (5.0f), (0.4f), (10.0f), (10.0f), (bool)0, ((int32_t)5000), _stringLiteral3732938296, /*hidden argument*/NULL);
		V_4 = L_37;
		PlayerShip_t2635532215 * L_38 = (PlayerShip_t2635532215 *)il2cpp_codegen_object_new(PlayerShip_t2635532215_il2cpp_TypeInfo_var);
		PlayerShip__ctor_m2607274753(L_38, 2, _stringLiteral2482320550, (10.0f), (0.0f), (20.0f), (0.0f), (6.0f), (0.4f), (10.0f), (10.0f), (bool)0, ((int32_t)5000), _stringLiteral2620904, /*hidden argument*/NULL);
		V_5 = L_38;
		PlayerShip_t2635532215 * L_39 = (PlayerShip_t2635532215 *)il2cpp_codegen_object_new(PlayerShip_t2635532215_il2cpp_TypeInfo_var);
		PlayerShip__ctor_m2607274753(L_39, 3, _stringLiteral2795080817, (20.0f), (0.0f), (20.0f), (0.0f), (7.0f), (0.4f), (10.0f), (10.0f), (bool)0, ((int32_t)5000), _stringLiteral3611346032, /*hidden argument*/NULL);
		V_6 = L_39;
		List_1_t2004653347 * L_40 = __this->get_dataBase_3();
		PlayerShip_t2635532215 * L_41 = V_3;
		NullCheck(L_40);
		List_1_Add_m3476285208(L_40, L_41, /*hidden argument*/List_1_Add_m3476285208_MethodInfo_var);
		List_1_t2004653347 * L_42 = __this->get_dataBase_3();
		PlayerShip_t2635532215 * L_43 = V_4;
		NullCheck(L_42);
		List_1_Add_m3476285208(L_42, L_43, /*hidden argument*/List_1_Add_m3476285208_MethodInfo_var);
		List_1_t2004653347 * L_44 = __this->get_dataBase_3();
		PlayerShip_t2635532215 * L_45 = V_5;
		NullCheck(L_44);
		List_1_Add_m3476285208(L_44, L_45, /*hidden argument*/List_1_Add_m3476285208_MethodInfo_var);
		List_1_t2004653347 * L_46 = __this->get_dataBase_3();
		PlayerShip_t2635532215 * L_47 = V_6;
		NullCheck(L_46);
		List_1_Add_m3476285208(L_46, L_47, /*hidden argument*/List_1_Add_m3476285208_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2364159610, /*hidden argument*/NULL);
	}

IL_0221:
	{
		return;
	}
}
// System.Void ShipsEquipment::.ctor()
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const uint32_t ShipsEquipment__ctor_m2190693100_MetadataUsageId;
extern "C"  void ShipsEquipment__ctor_m2190693100 (ShipsEquipment_t1003200637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsEquipment__ctor_m2190693100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_0, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		__this->set_equipment_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipsEquipment::Start()
extern "C"  void ShipsEquipment_Start_m1113167804 (ShipsEquipment_t1003200637 * __this, const MethodInfo* method)
{
	{
		ShipsEquipment_SetEqupedItems_m3820658024(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipsEquipment::SetEqupedItems()
extern const MethodInfo* List_1_get_Item_m4027977911_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2506740058_MethodInfo_var;
extern const uint32_t ShipsEquipment_SetEqupedItems_m3820658024_MetadataUsageId;
extern "C"  void ShipsEquipment_SetEqupedItems_m3820658024 (ShipsEquipment_t1003200637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsEquipment_SetEqupedItems_m3820658024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		V_0 = 0;
		goto IL_0049;
	}

IL_0007:
	{
		ItemDatabase_t2502586666 * L_0 = __this->get_data_2();
		NullCheck(L_0);
		List_1_t1809589323 * L_1 = L_0->get_weaponsDB_2();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Item_t2440468191 * L_3 = List_1_get_Item_m4027977911(L_1, L_2, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_itemID_2();
		List_1_t1440998580 * L_5 = __this->get_equipment_4();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Item_m1921196075(L_5, 0, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_6))))
		{
			goto IL_0045;
		}
	}
	{
		ItemDatabase_t2502586666 * L_7 = __this->get_data_2();
		NullCheck(L_7);
		List_1_t1809589323 * L_8 = L_7->get_weaponsDB_2();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		Item_t2440468191 * L_10 = List_1_get_Item_m4027977911(L_8, L_9, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		__this->set_weapon_5(L_10);
	}

IL_0045:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_12 = V_0;
		ItemDatabase_t2502586666 * L_13 = __this->get_data_2();
		NullCheck(L_13);
		List_1_t1809589323 * L_14 = L_13->get_weaponsDB_2();
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m2506740058(L_14, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_12) < ((int32_t)L_15)))
		{
			goto IL_0007;
		}
	}
	{
		V_1 = 0;
		goto IL_00a8;
	}

IL_0066:
	{
		ItemDatabase_t2502586666 * L_16 = __this->get_data_2();
		NullCheck(L_16);
		List_1_t1809589323 * L_17 = L_16->get_armorsDB_3();
		int32_t L_18 = V_1;
		NullCheck(L_17);
		Item_t2440468191 * L_19 = List_1_get_Item_m4027977911(L_17, L_18, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_19);
		int32_t L_20 = L_19->get_itemID_2();
		List_1_t1440998580 * L_21 = __this->get_equipment_4();
		NullCheck(L_21);
		int32_t L_22 = List_1_get_Item_m1921196075(L_21, 1, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		if ((!(((uint32_t)L_20) == ((uint32_t)L_22))))
		{
			goto IL_00a4;
		}
	}
	{
		ItemDatabase_t2502586666 * L_23 = __this->get_data_2();
		NullCheck(L_23);
		List_1_t1809589323 * L_24 = L_23->get_armorsDB_3();
		int32_t L_25 = V_1;
		NullCheck(L_24);
		Item_t2440468191 * L_26 = List_1_get_Item_m4027977911(L_24, L_25, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		__this->set_armor_6(L_26);
	}

IL_00a4:
	{
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00a8:
	{
		int32_t L_28 = V_1;
		ItemDatabase_t2502586666 * L_29 = __this->get_data_2();
		NullCheck(L_29);
		List_1_t1809589323 * L_30 = L_29->get_armorsDB_3();
		NullCheck(L_30);
		int32_t L_31 = List_1_get_Count_m2506740058(L_30, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_28) < ((int32_t)L_31)))
		{
			goto IL_0066;
		}
	}
	{
		V_2 = 0;
		goto IL_0107;
	}

IL_00c5:
	{
		ItemDatabase_t2502586666 * L_32 = __this->get_data_2();
		NullCheck(L_32);
		List_1_t1809589323 * L_33 = L_32->get_shieldsDB_4();
		int32_t L_34 = V_2;
		NullCheck(L_33);
		Item_t2440468191 * L_35 = List_1_get_Item_m4027977911(L_33, L_34, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_35);
		int32_t L_36 = L_35->get_itemID_2();
		List_1_t1440998580 * L_37 = __this->get_equipment_4();
		NullCheck(L_37);
		int32_t L_38 = List_1_get_Item_m1921196075(L_37, 2, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		if ((!(((uint32_t)L_36) == ((uint32_t)L_38))))
		{
			goto IL_0103;
		}
	}
	{
		ItemDatabase_t2502586666 * L_39 = __this->get_data_2();
		NullCheck(L_39);
		List_1_t1809589323 * L_40 = L_39->get_shieldsDB_4();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		Item_t2440468191 * L_42 = List_1_get_Item_m4027977911(L_40, L_41, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		__this->set_shield_7(L_42);
	}

IL_0103:
	{
		int32_t L_43 = V_2;
		V_2 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_0107:
	{
		int32_t L_44 = V_2;
		ItemDatabase_t2502586666 * L_45 = __this->get_data_2();
		NullCheck(L_45);
		List_1_t1809589323 * L_46 = L_45->get_shieldsDB_4();
		NullCheck(L_46);
		int32_t L_47 = List_1_get_Count_m2506740058(L_46, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_44) < ((int32_t)L_47)))
		{
			goto IL_00c5;
		}
	}
	{
		V_3 = 0;
		goto IL_0166;
	}

IL_0124:
	{
		ItemDatabase_t2502586666 * L_48 = __this->get_data_2();
		NullCheck(L_48);
		List_1_t1809589323 * L_49 = L_48->get_enginesDB_5();
		int32_t L_50 = V_3;
		NullCheck(L_49);
		Item_t2440468191 * L_51 = List_1_get_Item_m4027977911(L_49, L_50, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		NullCheck(L_51);
		int32_t L_52 = L_51->get_itemID_2();
		List_1_t1440998580 * L_53 = __this->get_equipment_4();
		NullCheck(L_53);
		int32_t L_54 = List_1_get_Item_m1921196075(L_53, 3, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		if ((!(((uint32_t)L_52) == ((uint32_t)L_54))))
		{
			goto IL_0162;
		}
	}
	{
		ItemDatabase_t2502586666 * L_55 = __this->get_data_2();
		NullCheck(L_55);
		List_1_t1809589323 * L_56 = L_55->get_enginesDB_5();
		int32_t L_57 = V_3;
		NullCheck(L_56);
		Item_t2440468191 * L_58 = List_1_get_Item_m4027977911(L_56, L_57, /*hidden argument*/List_1_get_Item_m4027977911_MethodInfo_var);
		__this->set_engine_8(L_58);
	}

IL_0162:
	{
		int32_t L_59 = V_3;
		V_3 = ((int32_t)((int32_t)L_59+(int32_t)1));
	}

IL_0166:
	{
		int32_t L_60 = V_3;
		ItemDatabase_t2502586666 * L_61 = __this->get_data_2();
		NullCheck(L_61);
		List_1_t1809589323 * L_62 = L_61->get_enginesDB_5();
		NullCheck(L_62);
		int32_t L_63 = List_1_get_Count_m2506740058(L_62, /*hidden argument*/List_1_get_Count_m2506740058_MethodInfo_var);
		if ((((int32_t)L_60) < ((int32_t)L_63)))
		{
			goto IL_0124;
		}
	}
	{
		return;
	}
}
// System.Void ShipsEquipment::SetEquipmentIDs()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_set_Item_m635251882_MethodInfo_var;
extern const uint32_t ShipsEquipment_SetEquipmentIDs_m3042426158_MetadataUsageId;
extern "C"  void ShipsEquipment_SetEquipmentIDs_m3042426158 (ShipsEquipment_t1003200637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipsEquipment_SetEquipmentIDs_m3042426158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Item_t2440468191 * L_0 = __this->get_weapon_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		List_1_t1440998580 * L_2 = __this->get_equipment_4();
		Item_t2440468191 * L_3 = __this->get_weapon_5();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_itemID_2();
		NullCheck(L_2);
		List_1_set_Item_m635251882(L_2, 0, L_4, /*hidden argument*/List_1_set_Item_m635251882_MethodInfo_var);
	}

IL_0028:
	{
		Item_t2440468191 * L_5 = __this->get_armor_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		List_1_t1440998580 * L_7 = __this->get_equipment_4();
		Item_t2440468191 * L_8 = __this->get_armor_6();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_itemID_2();
		NullCheck(L_7);
		List_1_set_Item_m635251882(L_7, 1, L_9, /*hidden argument*/List_1_set_Item_m635251882_MethodInfo_var);
	}

IL_0050:
	{
		Item_t2440468191 * L_10 = __this->get_shield_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0078;
		}
	}
	{
		List_1_t1440998580 * L_12 = __this->get_equipment_4();
		Item_t2440468191 * L_13 = __this->get_shield_7();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_itemID_2();
		NullCheck(L_12);
		List_1_set_Item_m635251882(L_12, 2, L_14, /*hidden argument*/List_1_set_Item_m635251882_MethodInfo_var);
	}

IL_0078:
	{
		Item_t2440468191 * L_15 = __this->get_engine_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_15, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00a0;
		}
	}
	{
		List_1_t1440998580 * L_17 = __this->get_equipment_4();
		Item_t2440468191 * L_18 = __this->get_engine_8();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_itemID_2();
		NullCheck(L_17);
		List_1_set_Item_m635251882(L_17, 3, L_19, /*hidden argument*/List_1_set_Item_m635251882_MethodInfo_var);
	}

IL_00a0:
	{
		return;
	}
}
// System.Void ShipStats::.ctor()
extern "C"  void ShipStats__ctor_m1488302020 (ShipStats_t3127462681 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipStats::Start()
extern const MethodInfo* List_1_get_Item_m2773528611_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3164906370_MethodInfo_var;
extern const uint32_t ShipStats_Start_m1131029172_MetadataUsageId;
extern "C"  void ShipStats_Start_m1131029172 (ShipStats_t3127462681 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipStats_Start_m1131029172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0007:
	{
		ShipsDatabase_t1101834110 * L_0 = __this->get_ships_4();
		NullCheck(L_0);
		List_1_t2004653347 * L_1 = L_0->get_dataBase_3();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		PlayerShip_t2635532215 * L_3 = List_1_get_Item_m2773528611(L_1, L_2, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_shipID_0();
		int32_t L_5 = __this->get_shipID_2();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_003f;
		}
	}
	{
		ShipsDatabase_t1101834110 * L_6 = __this->get_ships_4();
		NullCheck(L_6);
		List_1_t2004653347 * L_7 = L_6->get_dataBase_3();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		PlayerShip_t2635532215 * L_9 = List_1_get_Item_m2773528611(L_7, L_8, /*hidden argument*/List_1_get_Item_m2773528611_MethodInfo_var);
		__this->set_ship_3(L_9);
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_11 = V_0;
		ShipsDatabase_t1101834110 * L_12 = __this->get_ships_4();
		NullCheck(L_12);
		List_1_t2004653347 * L_13 = L_12->get_dataBase_3();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m3164906370(L_13, /*hidden argument*/List_1_get_Count_m3164906370_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
