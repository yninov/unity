﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameController
struct GameController_t3607102586;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t3607102586  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 GameController::shipID
	int32_t ___shipID_3;
	// System.Int32 GameController::credits
	int32_t ___credits_4;
	// System.Int32 GameController::abilityTokes
	int32_t ___abilityTokes_5;

public:
	inline static int32_t get_offset_of_shipID_3() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___shipID_3)); }
	inline int32_t get_shipID_3() const { return ___shipID_3; }
	inline int32_t* get_address_of_shipID_3() { return &___shipID_3; }
	inline void set_shipID_3(int32_t value)
	{
		___shipID_3 = value;
	}

	inline static int32_t get_offset_of_credits_4() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___credits_4)); }
	inline int32_t get_credits_4() const { return ___credits_4; }
	inline int32_t* get_address_of_credits_4() { return &___credits_4; }
	inline void set_credits_4(int32_t value)
	{
		___credits_4 = value;
	}

	inline static int32_t get_offset_of_abilityTokes_5() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___abilityTokes_5)); }
	inline int32_t get_abilityTokes_5() const { return ___abilityTokes_5; }
	inline int32_t* get_address_of_abilityTokes_5() { return &___abilityTokes_5; }
	inline void set_abilityTokes_5(int32_t value)
	{
		___abilityTokes_5 = value;
	}
};

struct GameController_t3607102586_StaticFields
{
public:
	// GameController GameController::current
	GameController_t3607102586 * ___current_2;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(GameController_t3607102586_StaticFields, ___current_2)); }
	inline GameController_t3607102586 * get_current_2() const { return ___current_2; }
	inline GameController_t3607102586 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(GameController_t3607102586 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier(&___current_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
