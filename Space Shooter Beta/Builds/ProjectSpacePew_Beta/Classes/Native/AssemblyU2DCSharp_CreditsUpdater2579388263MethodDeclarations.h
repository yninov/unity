﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CreditsUpdater
struct CreditsUpdater_t2579388263;

#include "codegen/il2cpp-codegen.h"

// System.Void CreditsUpdater::.ctor()
extern "C"  void CreditsUpdater__ctor_m4210419418 (CreditsUpdater_t2579388263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreditsUpdater::Update()
extern "C"  void CreditsUpdater_Update_m536369203 (CreditsUpdater_t2579388263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
