﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// ItemDatabase
struct ItemDatabase_t2502586666;
// ItemInventory
struct ItemInventory_t997429261;
// MainMenu
struct MainMenu_t4009084430;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// Item
struct Item_t2440468191;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryPanel
struct  InventoryPanel_t2677987186  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject InventoryPanel::itemDatabase
	GameObject_t1756533147 * ___itemDatabase_2;
	// ItemDatabase InventoryPanel::data
	ItemDatabase_t2502586666 * ___data_3;
	// UnityEngine.GameObject InventoryPanel::mainMenu
	GameObject_t1756533147 * ___mainMenu_4;
	// ItemInventory InventoryPanel::itemInventory
	ItemInventory_t997429261 * ___itemInventory_5;
	// MainMenu InventoryPanel::menuScript
	MainMenu_t4009084430 * ___menuScript_6;
	// UnityEngine.UI.Text InventoryPanel::itemIdText
	Text_t356221433 * ___itemIdText_7;
	// UnityEngine.UI.Text InventoryPanel::itemNameText
	Text_t356221433 * ___itemNameText_8;
	// UnityEngine.UI.Text InventoryPanel::itemBonusText
	Text_t356221433 * ___itemBonusText_9;
	// UnityEngine.UI.Text InventoryPanel::itemTypeText
	Text_t356221433 * ___itemTypeText_10;
	// UnityEngine.UI.Image InventoryPanel::itemImageSprite
	Image_t2042527209 * ___itemImageSprite_11;
	// Item InventoryPanel::itemAdded
	Item_t2440468191 * ___itemAdded_12;

public:
	inline static int32_t get_offset_of_itemDatabase_2() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___itemDatabase_2)); }
	inline GameObject_t1756533147 * get_itemDatabase_2() const { return ___itemDatabase_2; }
	inline GameObject_t1756533147 ** get_address_of_itemDatabase_2() { return &___itemDatabase_2; }
	inline void set_itemDatabase_2(GameObject_t1756533147 * value)
	{
		___itemDatabase_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemDatabase_2, value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___data_3)); }
	inline ItemDatabase_t2502586666 * get_data_3() const { return ___data_3; }
	inline ItemDatabase_t2502586666 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(ItemDatabase_t2502586666 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier(&___data_3, value);
	}

	inline static int32_t get_offset_of_mainMenu_4() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___mainMenu_4)); }
	inline GameObject_t1756533147 * get_mainMenu_4() const { return ___mainMenu_4; }
	inline GameObject_t1756533147 ** get_address_of_mainMenu_4() { return &___mainMenu_4; }
	inline void set_mainMenu_4(GameObject_t1756533147 * value)
	{
		___mainMenu_4 = value;
		Il2CppCodeGenWriteBarrier(&___mainMenu_4, value);
	}

	inline static int32_t get_offset_of_itemInventory_5() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___itemInventory_5)); }
	inline ItemInventory_t997429261 * get_itemInventory_5() const { return ___itemInventory_5; }
	inline ItemInventory_t997429261 ** get_address_of_itemInventory_5() { return &___itemInventory_5; }
	inline void set_itemInventory_5(ItemInventory_t997429261 * value)
	{
		___itemInventory_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemInventory_5, value);
	}

	inline static int32_t get_offset_of_menuScript_6() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___menuScript_6)); }
	inline MainMenu_t4009084430 * get_menuScript_6() const { return ___menuScript_6; }
	inline MainMenu_t4009084430 ** get_address_of_menuScript_6() { return &___menuScript_6; }
	inline void set_menuScript_6(MainMenu_t4009084430 * value)
	{
		___menuScript_6 = value;
		Il2CppCodeGenWriteBarrier(&___menuScript_6, value);
	}

	inline static int32_t get_offset_of_itemIdText_7() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___itemIdText_7)); }
	inline Text_t356221433 * get_itemIdText_7() const { return ___itemIdText_7; }
	inline Text_t356221433 ** get_address_of_itemIdText_7() { return &___itemIdText_7; }
	inline void set_itemIdText_7(Text_t356221433 * value)
	{
		___itemIdText_7 = value;
		Il2CppCodeGenWriteBarrier(&___itemIdText_7, value);
	}

	inline static int32_t get_offset_of_itemNameText_8() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___itemNameText_8)); }
	inline Text_t356221433 * get_itemNameText_8() const { return ___itemNameText_8; }
	inline Text_t356221433 ** get_address_of_itemNameText_8() { return &___itemNameText_8; }
	inline void set_itemNameText_8(Text_t356221433 * value)
	{
		___itemNameText_8 = value;
		Il2CppCodeGenWriteBarrier(&___itemNameText_8, value);
	}

	inline static int32_t get_offset_of_itemBonusText_9() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___itemBonusText_9)); }
	inline Text_t356221433 * get_itemBonusText_9() const { return ___itemBonusText_9; }
	inline Text_t356221433 ** get_address_of_itemBonusText_9() { return &___itemBonusText_9; }
	inline void set_itemBonusText_9(Text_t356221433 * value)
	{
		___itemBonusText_9 = value;
		Il2CppCodeGenWriteBarrier(&___itemBonusText_9, value);
	}

	inline static int32_t get_offset_of_itemTypeText_10() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___itemTypeText_10)); }
	inline Text_t356221433 * get_itemTypeText_10() const { return ___itemTypeText_10; }
	inline Text_t356221433 ** get_address_of_itemTypeText_10() { return &___itemTypeText_10; }
	inline void set_itemTypeText_10(Text_t356221433 * value)
	{
		___itemTypeText_10 = value;
		Il2CppCodeGenWriteBarrier(&___itemTypeText_10, value);
	}

	inline static int32_t get_offset_of_itemImageSprite_11() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___itemImageSprite_11)); }
	inline Image_t2042527209 * get_itemImageSprite_11() const { return ___itemImageSprite_11; }
	inline Image_t2042527209 ** get_address_of_itemImageSprite_11() { return &___itemImageSprite_11; }
	inline void set_itemImageSprite_11(Image_t2042527209 * value)
	{
		___itemImageSprite_11 = value;
		Il2CppCodeGenWriteBarrier(&___itemImageSprite_11, value);
	}

	inline static int32_t get_offset_of_itemAdded_12() { return static_cast<int32_t>(offsetof(InventoryPanel_t2677987186, ___itemAdded_12)); }
	inline Item_t2440468191 * get_itemAdded_12() const { return ___itemAdded_12; }
	inline Item_t2440468191 ** get_address_of_itemAdded_12() { return &___itemAdded_12; }
	inline void set_itemAdded_12(Item_t2440468191 * value)
	{
		___itemAdded_12 = value;
		Il2CppCodeGenWriteBarrier(&___itemAdded_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
