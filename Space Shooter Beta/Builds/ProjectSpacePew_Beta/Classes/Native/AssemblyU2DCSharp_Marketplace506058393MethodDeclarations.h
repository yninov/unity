﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Marketplace
struct Marketplace_t506058393;

#include "codegen/il2cpp-codegen.h"

// System.Void Marketplace::.ctor()
extern "C"  void Marketplace__ctor_m872510910 (Marketplace_t506058393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marketplace::Start()
extern "C"  void Marketplace_Start_m3630212126 (Marketplace_t506058393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marketplace::BuyShip()
extern "C"  void Marketplace_BuyShip_m3274103964 (Marketplace_t506058393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
