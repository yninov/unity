﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ItemDatabase
struct ItemDatabase_t2502586666;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// Item
struct Item_t2440468191;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipsEquipment
struct  ShipsEquipment_t1003200637  : public MonoBehaviour_t1158329972
{
public:
	// ItemDatabase ShipsEquipment::data
	ItemDatabase_t2502586666 * ___data_2;
	// System.Int32 ShipsEquipment::shipID
	int32_t ___shipID_3;
	// System.Collections.Generic.List`1<System.Int32> ShipsEquipment::equipment
	List_1_t1440998580 * ___equipment_4;
	// Item ShipsEquipment::weapon
	Item_t2440468191 * ___weapon_5;
	// Item ShipsEquipment::armor
	Item_t2440468191 * ___armor_6;
	// Item ShipsEquipment::shield
	Item_t2440468191 * ___shield_7;
	// Item ShipsEquipment::engine
	Item_t2440468191 * ___engine_8;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(ShipsEquipment_t1003200637, ___data_2)); }
	inline ItemDatabase_t2502586666 * get_data_2() const { return ___data_2; }
	inline ItemDatabase_t2502586666 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(ItemDatabase_t2502586666 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier(&___data_2, value);
	}

	inline static int32_t get_offset_of_shipID_3() { return static_cast<int32_t>(offsetof(ShipsEquipment_t1003200637, ___shipID_3)); }
	inline int32_t get_shipID_3() const { return ___shipID_3; }
	inline int32_t* get_address_of_shipID_3() { return &___shipID_3; }
	inline void set_shipID_3(int32_t value)
	{
		___shipID_3 = value;
	}

	inline static int32_t get_offset_of_equipment_4() { return static_cast<int32_t>(offsetof(ShipsEquipment_t1003200637, ___equipment_4)); }
	inline List_1_t1440998580 * get_equipment_4() const { return ___equipment_4; }
	inline List_1_t1440998580 ** get_address_of_equipment_4() { return &___equipment_4; }
	inline void set_equipment_4(List_1_t1440998580 * value)
	{
		___equipment_4 = value;
		Il2CppCodeGenWriteBarrier(&___equipment_4, value);
	}

	inline static int32_t get_offset_of_weapon_5() { return static_cast<int32_t>(offsetof(ShipsEquipment_t1003200637, ___weapon_5)); }
	inline Item_t2440468191 * get_weapon_5() const { return ___weapon_5; }
	inline Item_t2440468191 ** get_address_of_weapon_5() { return &___weapon_5; }
	inline void set_weapon_5(Item_t2440468191 * value)
	{
		___weapon_5 = value;
		Il2CppCodeGenWriteBarrier(&___weapon_5, value);
	}

	inline static int32_t get_offset_of_armor_6() { return static_cast<int32_t>(offsetof(ShipsEquipment_t1003200637, ___armor_6)); }
	inline Item_t2440468191 * get_armor_6() const { return ___armor_6; }
	inline Item_t2440468191 ** get_address_of_armor_6() { return &___armor_6; }
	inline void set_armor_6(Item_t2440468191 * value)
	{
		___armor_6 = value;
		Il2CppCodeGenWriteBarrier(&___armor_6, value);
	}

	inline static int32_t get_offset_of_shield_7() { return static_cast<int32_t>(offsetof(ShipsEquipment_t1003200637, ___shield_7)); }
	inline Item_t2440468191 * get_shield_7() const { return ___shield_7; }
	inline Item_t2440468191 ** get_address_of_shield_7() { return &___shield_7; }
	inline void set_shield_7(Item_t2440468191 * value)
	{
		___shield_7 = value;
		Il2CppCodeGenWriteBarrier(&___shield_7, value);
	}

	inline static int32_t get_offset_of_engine_8() { return static_cast<int32_t>(offsetof(ShipsEquipment_t1003200637, ___engine_8)); }
	inline Item_t2440468191 * get_engine_8() const { return ___engine_8; }
	inline Item_t2440468191 ** get_address_of_engine_8() { return &___engine_8; }
	inline void set_engine_8(Item_t2440468191 * value)
	{
		___engine_8 = value;
		Il2CppCodeGenWriteBarrier(&___engine_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
