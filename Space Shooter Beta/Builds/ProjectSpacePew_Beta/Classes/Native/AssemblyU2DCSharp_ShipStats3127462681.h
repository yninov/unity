﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerShip
struct PlayerShip_t2635532215;
// ShipsDatabase
struct ShipsDatabase_t1101834110;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipStats
struct  ShipStats_t3127462681  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ShipStats::shipID
	int32_t ___shipID_2;
	// PlayerShip ShipStats::ship
	PlayerShip_t2635532215 * ___ship_3;
	// ShipsDatabase ShipStats::ships
	ShipsDatabase_t1101834110 * ___ships_4;

public:
	inline static int32_t get_offset_of_shipID_2() { return static_cast<int32_t>(offsetof(ShipStats_t3127462681, ___shipID_2)); }
	inline int32_t get_shipID_2() const { return ___shipID_2; }
	inline int32_t* get_address_of_shipID_2() { return &___shipID_2; }
	inline void set_shipID_2(int32_t value)
	{
		___shipID_2 = value;
	}

	inline static int32_t get_offset_of_ship_3() { return static_cast<int32_t>(offsetof(ShipStats_t3127462681, ___ship_3)); }
	inline PlayerShip_t2635532215 * get_ship_3() const { return ___ship_3; }
	inline PlayerShip_t2635532215 ** get_address_of_ship_3() { return &___ship_3; }
	inline void set_ship_3(PlayerShip_t2635532215 * value)
	{
		___ship_3 = value;
		Il2CppCodeGenWriteBarrier(&___ship_3, value);
	}

	inline static int32_t get_offset_of_ships_4() { return static_cast<int32_t>(offsetof(ShipStats_t3127462681, ___ships_4)); }
	inline ShipsDatabase_t1101834110 * get_ships_4() const { return ___ships_4; }
	inline ShipsDatabase_t1101834110 ** get_address_of_ships_4() { return &___ships_4; }
	inline void set_ships_4(ShipsDatabase_t1101834110 * value)
	{
		___ships_4 = value;
		Il2CppCodeGenWriteBarrier(&___ships_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
