﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShipsDatabase
struct ShipsDatabase_t1101834110;
// ShipsController
struct ShipsController_t970893927;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Marketplace
struct  Marketplace_t506058393  : public MonoBehaviour_t1158329972
{
public:
	// ShipsDatabase Marketplace::ships
	ShipsDatabase_t1101834110 * ___ships_2;
	// ShipsController Marketplace::shipController
	ShipsController_t970893927 * ___shipController_3;
	// System.Int32 Marketplace::shipNumber
	int32_t ___shipNumber_4;
	// UnityEngine.UI.Text Marketplace::shipName
	Text_t356221433 * ___shipName_5;
	// UnityEngine.UI.Text Marketplace::price
	Text_t356221433 * ___price_6;
	// UnityEngine.UI.Text Marketplace::id
	Text_t356221433 * ___id_7;

public:
	inline static int32_t get_offset_of_ships_2() { return static_cast<int32_t>(offsetof(Marketplace_t506058393, ___ships_2)); }
	inline ShipsDatabase_t1101834110 * get_ships_2() const { return ___ships_2; }
	inline ShipsDatabase_t1101834110 ** get_address_of_ships_2() { return &___ships_2; }
	inline void set_ships_2(ShipsDatabase_t1101834110 * value)
	{
		___ships_2 = value;
		Il2CppCodeGenWriteBarrier(&___ships_2, value);
	}

	inline static int32_t get_offset_of_shipController_3() { return static_cast<int32_t>(offsetof(Marketplace_t506058393, ___shipController_3)); }
	inline ShipsController_t970893927 * get_shipController_3() const { return ___shipController_3; }
	inline ShipsController_t970893927 ** get_address_of_shipController_3() { return &___shipController_3; }
	inline void set_shipController_3(ShipsController_t970893927 * value)
	{
		___shipController_3 = value;
		Il2CppCodeGenWriteBarrier(&___shipController_3, value);
	}

	inline static int32_t get_offset_of_shipNumber_4() { return static_cast<int32_t>(offsetof(Marketplace_t506058393, ___shipNumber_4)); }
	inline int32_t get_shipNumber_4() const { return ___shipNumber_4; }
	inline int32_t* get_address_of_shipNumber_4() { return &___shipNumber_4; }
	inline void set_shipNumber_4(int32_t value)
	{
		___shipNumber_4 = value;
	}

	inline static int32_t get_offset_of_shipName_5() { return static_cast<int32_t>(offsetof(Marketplace_t506058393, ___shipName_5)); }
	inline Text_t356221433 * get_shipName_5() const { return ___shipName_5; }
	inline Text_t356221433 ** get_address_of_shipName_5() { return &___shipName_5; }
	inline void set_shipName_5(Text_t356221433 * value)
	{
		___shipName_5 = value;
		Il2CppCodeGenWriteBarrier(&___shipName_5, value);
	}

	inline static int32_t get_offset_of_price_6() { return static_cast<int32_t>(offsetof(Marketplace_t506058393, ___price_6)); }
	inline Text_t356221433 * get_price_6() const { return ___price_6; }
	inline Text_t356221433 ** get_address_of_price_6() { return &___price_6; }
	inline void set_price_6(Text_t356221433 * value)
	{
		___price_6 = value;
		Il2CppCodeGenWriteBarrier(&___price_6, value);
	}

	inline static int32_t get_offset_of_id_7() { return static_cast<int32_t>(offsetof(Marketplace_t506058393, ___id_7)); }
	inline Text_t356221433 * get_id_7() const { return ___id_7; }
	inline Text_t356221433 ** get_address_of_id_7() { return &___id_7; }
	inline void set_id_7(Text_t356221433 * value)
	{
		___id_7 = value;
		Il2CppCodeGenWriteBarrier(&___id_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
