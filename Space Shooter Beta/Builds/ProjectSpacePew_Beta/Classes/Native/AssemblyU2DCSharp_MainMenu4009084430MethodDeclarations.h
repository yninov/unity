﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainMenu
struct MainMenu_t4009084430;

#include "codegen/il2cpp-codegen.h"

// System.Void MainMenu::.ctor()
extern "C"  void MainMenu__ctor_m2536434407 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::MarketPlaceMenuOpen()
extern "C"  void MainMenu_MarketPlaceMenuOpen_m4090598755 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::MarketPlaceMenuClose()
extern "C"  void MainMenu_MarketPlaceMenuClose_m2315653753 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::InventoryMenuOpen()
extern "C"  void MainMenu_InventoryMenuOpen_m3098346478 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::InventoryMenuClose()
extern "C"  void MainMenu_InventoryMenuClose_m882318020 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::AbilityMenuOpen()
extern "C"  void MainMenu_AbilityMenuOpen_m3358290612 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::AbilityMenuClose()
extern "C"  void MainMenu_AbilityMenuClose_m1703085502 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::MissionMenuOpen()
extern "C"  void MainMenu_MissionMenuOpen_m4198987900 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::MissionMenuClose()
extern "C"  void MainMenu_MissionMenuClose_m2505153874 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::WeaponInventoryMenuOpen()
extern "C"  void MainMenu_WeaponInventoryMenuOpen_m1494163322 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::WeaponInventoryMenuClose()
extern "C"  void MainMenu_WeaponInventoryMenuClose_m1434171012 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::ShieldInventoryMenuOpen()
extern "C"  void MainMenu_ShieldInventoryMenuOpen_m1653227129 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::ShieldInventoryMenuClose()
extern "C"  void MainMenu_ShieldInventoryMenuClose_m1486938927 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::ArmorInventoryMenuOpen()
extern "C"  void MainMenu_ArmorInventoryMenuOpen_m197611509 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::ArmorInventoryMenuClose()
extern "C"  void MainMenu_ArmorInventoryMenuClose_m1327901971 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::EngineInventoryMenuOpen()
extern "C"  void MainMenu_EngineInventoryMenuOpen_m339844472 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::EngineInventoryMenuClose()
extern "C"  void MainMenu_EngineInventoryMenuClose_m1809813282 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::ShipsSlotMenuOpen()
extern "C"  void MainMenu_ShipsSlotMenuOpen_m1881846937 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::WeaponsSlotMenuOpen()
extern "C"  void MainMenu_WeaponsSlotMenuOpen_m991590537 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::ArmorSlotMenuOpen()
extern "C"  void MainMenu_ArmorSlotMenuOpen_m3294957115 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::ShieldSlotMenuOpen()
extern "C"  void MainMenu_ShieldSlotMenuOpen_m1171592391 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenu::EngnesSlotMenuOpen()
extern "C"  void MainMenu_EngnesSlotMenuOpen_m2975246424 (MainMenu_t4009084430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
