﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShipsDatabase
struct ShipsDatabase_t1101834110;

#include "codegen/il2cpp-codegen.h"

// System.Void ShipsDatabase::.ctor()
extern "C"  void ShipsDatabase__ctor_m3331506775 (ShipsDatabase_t1101834110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsDatabase::Start()
extern "C"  void ShipsDatabase_Start_m1770160195 (ShipsDatabase_t1101834110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsDatabase::SaveGame()
extern "C"  void ShipsDatabase_SaveGame_m2827057378 (ShipsDatabase_t1101834110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShipsDatabase::LoadGame()
extern "C"  void ShipsDatabase_LoadGame_m3185594747 (ShipsDatabase_t1101834110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
