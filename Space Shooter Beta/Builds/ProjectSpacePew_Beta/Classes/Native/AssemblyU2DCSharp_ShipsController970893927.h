﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShipsDatabase
struct ShipsDatabase_t1101834110;
// System.Collections.Generic.List`1<PlayerShip>
struct List_1_t2004653347;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ItemInventory
struct ItemInventory_t997429261;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipsController
struct  ShipsController_t970893927  : public MonoBehaviour_t1158329972
{
public:
	// ShipsDatabase ShipsController::ships
	ShipsDatabase_t1101834110 * ___ships_2;
	// System.Collections.Generic.List`1<PlayerShip> ShipsController::allShips
	List_1_t2004653347 * ___allShips_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ShipsController::purchasedShips
	List_1_t1125654279 * ___purchasedShips_4;
	// System.Boolean ShipsController::shipAdded0
	bool ___shipAdded0_5;
	// System.Boolean ShipsController::shipAdded1
	bool ___shipAdded1_6;
	// System.Boolean ShipsController::shipAdded2
	bool ___shipAdded2_7;
	// System.Boolean ShipsController::shipAdded3
	bool ___shipAdded3_8;
	// UnityEngine.GameObject ShipsController::ship0
	GameObject_t1756533147 * ___ship0_9;
	// UnityEngine.GameObject ShipsController::ship1
	GameObject_t1756533147 * ___ship1_10;
	// UnityEngine.GameObject ShipsController::ship2
	GameObject_t1756533147 * ___ship2_11;
	// UnityEngine.GameObject ShipsController::ship3
	GameObject_t1756533147 * ___ship3_12;
	// System.Int32 ShipsController::shipRotationNumber
	int32_t ___shipRotationNumber_13;
	// ItemInventory ShipsController::itemInventory
	ItemInventory_t997429261 * ___itemInventory_14;

public:
	inline static int32_t get_offset_of_ships_2() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___ships_2)); }
	inline ShipsDatabase_t1101834110 * get_ships_2() const { return ___ships_2; }
	inline ShipsDatabase_t1101834110 ** get_address_of_ships_2() { return &___ships_2; }
	inline void set_ships_2(ShipsDatabase_t1101834110 * value)
	{
		___ships_2 = value;
		Il2CppCodeGenWriteBarrier(&___ships_2, value);
	}

	inline static int32_t get_offset_of_allShips_3() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___allShips_3)); }
	inline List_1_t2004653347 * get_allShips_3() const { return ___allShips_3; }
	inline List_1_t2004653347 ** get_address_of_allShips_3() { return &___allShips_3; }
	inline void set_allShips_3(List_1_t2004653347 * value)
	{
		___allShips_3 = value;
		Il2CppCodeGenWriteBarrier(&___allShips_3, value);
	}

	inline static int32_t get_offset_of_purchasedShips_4() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___purchasedShips_4)); }
	inline List_1_t1125654279 * get_purchasedShips_4() const { return ___purchasedShips_4; }
	inline List_1_t1125654279 ** get_address_of_purchasedShips_4() { return &___purchasedShips_4; }
	inline void set_purchasedShips_4(List_1_t1125654279 * value)
	{
		___purchasedShips_4 = value;
		Il2CppCodeGenWriteBarrier(&___purchasedShips_4, value);
	}

	inline static int32_t get_offset_of_shipAdded0_5() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___shipAdded0_5)); }
	inline bool get_shipAdded0_5() const { return ___shipAdded0_5; }
	inline bool* get_address_of_shipAdded0_5() { return &___shipAdded0_5; }
	inline void set_shipAdded0_5(bool value)
	{
		___shipAdded0_5 = value;
	}

	inline static int32_t get_offset_of_shipAdded1_6() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___shipAdded1_6)); }
	inline bool get_shipAdded1_6() const { return ___shipAdded1_6; }
	inline bool* get_address_of_shipAdded1_6() { return &___shipAdded1_6; }
	inline void set_shipAdded1_6(bool value)
	{
		___shipAdded1_6 = value;
	}

	inline static int32_t get_offset_of_shipAdded2_7() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___shipAdded2_7)); }
	inline bool get_shipAdded2_7() const { return ___shipAdded2_7; }
	inline bool* get_address_of_shipAdded2_7() { return &___shipAdded2_7; }
	inline void set_shipAdded2_7(bool value)
	{
		___shipAdded2_7 = value;
	}

	inline static int32_t get_offset_of_shipAdded3_8() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___shipAdded3_8)); }
	inline bool get_shipAdded3_8() const { return ___shipAdded3_8; }
	inline bool* get_address_of_shipAdded3_8() { return &___shipAdded3_8; }
	inline void set_shipAdded3_8(bool value)
	{
		___shipAdded3_8 = value;
	}

	inline static int32_t get_offset_of_ship0_9() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___ship0_9)); }
	inline GameObject_t1756533147 * get_ship0_9() const { return ___ship0_9; }
	inline GameObject_t1756533147 ** get_address_of_ship0_9() { return &___ship0_9; }
	inline void set_ship0_9(GameObject_t1756533147 * value)
	{
		___ship0_9 = value;
		Il2CppCodeGenWriteBarrier(&___ship0_9, value);
	}

	inline static int32_t get_offset_of_ship1_10() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___ship1_10)); }
	inline GameObject_t1756533147 * get_ship1_10() const { return ___ship1_10; }
	inline GameObject_t1756533147 ** get_address_of_ship1_10() { return &___ship1_10; }
	inline void set_ship1_10(GameObject_t1756533147 * value)
	{
		___ship1_10 = value;
		Il2CppCodeGenWriteBarrier(&___ship1_10, value);
	}

	inline static int32_t get_offset_of_ship2_11() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___ship2_11)); }
	inline GameObject_t1756533147 * get_ship2_11() const { return ___ship2_11; }
	inline GameObject_t1756533147 ** get_address_of_ship2_11() { return &___ship2_11; }
	inline void set_ship2_11(GameObject_t1756533147 * value)
	{
		___ship2_11 = value;
		Il2CppCodeGenWriteBarrier(&___ship2_11, value);
	}

	inline static int32_t get_offset_of_ship3_12() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___ship3_12)); }
	inline GameObject_t1756533147 * get_ship3_12() const { return ___ship3_12; }
	inline GameObject_t1756533147 ** get_address_of_ship3_12() { return &___ship3_12; }
	inline void set_ship3_12(GameObject_t1756533147 * value)
	{
		___ship3_12 = value;
		Il2CppCodeGenWriteBarrier(&___ship3_12, value);
	}

	inline static int32_t get_offset_of_shipRotationNumber_13() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___shipRotationNumber_13)); }
	inline int32_t get_shipRotationNumber_13() const { return ___shipRotationNumber_13; }
	inline int32_t* get_address_of_shipRotationNumber_13() { return &___shipRotationNumber_13; }
	inline void set_shipRotationNumber_13(int32_t value)
	{
		___shipRotationNumber_13 = value;
	}

	inline static int32_t get_offset_of_itemInventory_14() { return static_cast<int32_t>(offsetof(ShipsController_t970893927, ___itemInventory_14)); }
	inline ItemInventory_t997429261 * get_itemInventory_14() const { return ___itemInventory_14; }
	inline ItemInventory_t997429261 ** get_address_of_itemInventory_14() { return &___itemInventory_14; }
	inline void set_itemInventory_14(ItemInventory_t997429261 * value)
	{
		___itemInventory_14 = value;
		Il2CppCodeGenWriteBarrier(&___itemInventory_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
