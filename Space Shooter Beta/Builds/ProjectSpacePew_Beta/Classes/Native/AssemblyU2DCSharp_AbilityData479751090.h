﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AbilityData
struct  AbilityData_t479751090  : public MonoBehaviour_t1158329972
{
public:
	// System.Single AbilityData::shipMaxArmor
	float ___shipMaxArmor_2;
	// System.Single AbilityData::shipRegenArmor
	float ___shipRegenArmor_3;
	// System.Single AbilityData::shipMaxShield
	float ___shipMaxShield_4;
	// System.Single AbilityData::shipRegenShield
	float ___shipRegenShield_5;
	// System.Single AbilityData::shipSpeed
	float ___shipSpeed_6;
	// System.Single AbilityData::shipFireRate
	float ___shipFireRate_7;
	// System.Single AbilityData::shipDamage
	float ___shipDamage_8;
	// System.String AbilityData::abilityName
	String_t* ___abilityName_9;
	// System.Int32 AbilityData::abilityLevel
	int32_t ___abilityLevel_10;

public:
	inline static int32_t get_offset_of_shipMaxArmor_2() { return static_cast<int32_t>(offsetof(AbilityData_t479751090, ___shipMaxArmor_2)); }
	inline float get_shipMaxArmor_2() const { return ___shipMaxArmor_2; }
	inline float* get_address_of_shipMaxArmor_2() { return &___shipMaxArmor_2; }
	inline void set_shipMaxArmor_2(float value)
	{
		___shipMaxArmor_2 = value;
	}

	inline static int32_t get_offset_of_shipRegenArmor_3() { return static_cast<int32_t>(offsetof(AbilityData_t479751090, ___shipRegenArmor_3)); }
	inline float get_shipRegenArmor_3() const { return ___shipRegenArmor_3; }
	inline float* get_address_of_shipRegenArmor_3() { return &___shipRegenArmor_3; }
	inline void set_shipRegenArmor_3(float value)
	{
		___shipRegenArmor_3 = value;
	}

	inline static int32_t get_offset_of_shipMaxShield_4() { return static_cast<int32_t>(offsetof(AbilityData_t479751090, ___shipMaxShield_4)); }
	inline float get_shipMaxShield_4() const { return ___shipMaxShield_4; }
	inline float* get_address_of_shipMaxShield_4() { return &___shipMaxShield_4; }
	inline void set_shipMaxShield_4(float value)
	{
		___shipMaxShield_4 = value;
	}

	inline static int32_t get_offset_of_shipRegenShield_5() { return static_cast<int32_t>(offsetof(AbilityData_t479751090, ___shipRegenShield_5)); }
	inline float get_shipRegenShield_5() const { return ___shipRegenShield_5; }
	inline float* get_address_of_shipRegenShield_5() { return &___shipRegenShield_5; }
	inline void set_shipRegenShield_5(float value)
	{
		___shipRegenShield_5 = value;
	}

	inline static int32_t get_offset_of_shipSpeed_6() { return static_cast<int32_t>(offsetof(AbilityData_t479751090, ___shipSpeed_6)); }
	inline float get_shipSpeed_6() const { return ___shipSpeed_6; }
	inline float* get_address_of_shipSpeed_6() { return &___shipSpeed_6; }
	inline void set_shipSpeed_6(float value)
	{
		___shipSpeed_6 = value;
	}

	inline static int32_t get_offset_of_shipFireRate_7() { return static_cast<int32_t>(offsetof(AbilityData_t479751090, ___shipFireRate_7)); }
	inline float get_shipFireRate_7() const { return ___shipFireRate_7; }
	inline float* get_address_of_shipFireRate_7() { return &___shipFireRate_7; }
	inline void set_shipFireRate_7(float value)
	{
		___shipFireRate_7 = value;
	}

	inline static int32_t get_offset_of_shipDamage_8() { return static_cast<int32_t>(offsetof(AbilityData_t479751090, ___shipDamage_8)); }
	inline float get_shipDamage_8() const { return ___shipDamage_8; }
	inline float* get_address_of_shipDamage_8() { return &___shipDamage_8; }
	inline void set_shipDamage_8(float value)
	{
		___shipDamage_8 = value;
	}

	inline static int32_t get_offset_of_abilityName_9() { return static_cast<int32_t>(offsetof(AbilityData_t479751090, ___abilityName_9)); }
	inline String_t* get_abilityName_9() const { return ___abilityName_9; }
	inline String_t** get_address_of_abilityName_9() { return &___abilityName_9; }
	inline void set_abilityName_9(String_t* value)
	{
		___abilityName_9 = value;
		Il2CppCodeGenWriteBarrier(&___abilityName_9, value);
	}

	inline static int32_t get_offset_of_abilityLevel_10() { return static_cast<int32_t>(offsetof(AbilityData_t479751090, ___abilityLevel_10)); }
	inline int32_t get_abilityLevel_10() const { return ___abilityLevel_10; }
	inline int32_t* get_address_of_abilityLevel_10() { return &___abilityLevel_10; }
	inline void set_abilityLevel_10(int32_t value)
	{
		___abilityLevel_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
