﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemDatabase
struct ItemDatabase_t2502586666;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemDatabase::.ctor()
extern "C"  void ItemDatabase__ctor_m1971751031 (ItemDatabase_t2502586666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemDatabase::Start()
extern "C"  void ItemDatabase_Start_m3776298315 (ItemDatabase_t2502586666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
