﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t592507723;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
extern "C"  void CreateAssetMenuAttribute__ctor_m2684223354 (CreateAssetMenuAttribute_t592507723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
