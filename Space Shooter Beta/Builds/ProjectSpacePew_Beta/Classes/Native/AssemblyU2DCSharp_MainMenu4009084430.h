﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu
struct  MainMenu_t4009084430  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MainMenu::inventoryMenu
	GameObject_t1756533147 * ___inventoryMenu_2;
	// UnityEngine.GameObject MainMenu::abilityMenu
	GameObject_t1756533147 * ___abilityMenu_3;
	// UnityEngine.GameObject MainMenu::missionMenu
	GameObject_t1756533147 * ___missionMenu_4;
	// UnityEngine.GameObject MainMenu::marketPlaceMenu
	GameObject_t1756533147 * ___marketPlaceMenu_5;
	// UnityEngine.GameObject MainMenu::weaponInventoryMenu
	GameObject_t1756533147 * ___weaponInventoryMenu_6;
	// UnityEngine.GameObject MainMenu::shieldInventoryMenu
	GameObject_t1756533147 * ___shieldInventoryMenu_7;
	// UnityEngine.GameObject MainMenu::armorInventoryMenu
	GameObject_t1756533147 * ___armorInventoryMenu_8;
	// UnityEngine.GameObject MainMenu::engineInventoryMenu
	GameObject_t1756533147 * ___engineInventoryMenu_9;
	// UnityEngine.GameObject MainMenu::shipsSlotMenu
	GameObject_t1756533147 * ___shipsSlotMenu_10;
	// UnityEngine.GameObject MainMenu::weaponsSlotMenu
	GameObject_t1756533147 * ___weaponsSlotMenu_11;
	// UnityEngine.GameObject MainMenu::armorSlotMenu
	GameObject_t1756533147 * ___armorSlotMenu_12;
	// UnityEngine.GameObject MainMenu::shieldSlotMenu
	GameObject_t1756533147 * ___shieldSlotMenu_13;
	// UnityEngine.GameObject MainMenu::enginesSlotMenu
	GameObject_t1756533147 * ___enginesSlotMenu_14;
	// UnityEngine.Animator MainMenu::camInventoryAnim
	Animator_t69676727 * ___camInventoryAnim_15;
	// UnityEngine.Animator MainMenu::inventoryAnim
	Animator_t69676727 * ___inventoryAnim_16;

public:
	inline static int32_t get_offset_of_inventoryMenu_2() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___inventoryMenu_2)); }
	inline GameObject_t1756533147 * get_inventoryMenu_2() const { return ___inventoryMenu_2; }
	inline GameObject_t1756533147 ** get_address_of_inventoryMenu_2() { return &___inventoryMenu_2; }
	inline void set_inventoryMenu_2(GameObject_t1756533147 * value)
	{
		___inventoryMenu_2 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryMenu_2, value);
	}

	inline static int32_t get_offset_of_abilityMenu_3() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___abilityMenu_3)); }
	inline GameObject_t1756533147 * get_abilityMenu_3() const { return ___abilityMenu_3; }
	inline GameObject_t1756533147 ** get_address_of_abilityMenu_3() { return &___abilityMenu_3; }
	inline void set_abilityMenu_3(GameObject_t1756533147 * value)
	{
		___abilityMenu_3 = value;
		Il2CppCodeGenWriteBarrier(&___abilityMenu_3, value);
	}

	inline static int32_t get_offset_of_missionMenu_4() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___missionMenu_4)); }
	inline GameObject_t1756533147 * get_missionMenu_4() const { return ___missionMenu_4; }
	inline GameObject_t1756533147 ** get_address_of_missionMenu_4() { return &___missionMenu_4; }
	inline void set_missionMenu_4(GameObject_t1756533147 * value)
	{
		___missionMenu_4 = value;
		Il2CppCodeGenWriteBarrier(&___missionMenu_4, value);
	}

	inline static int32_t get_offset_of_marketPlaceMenu_5() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___marketPlaceMenu_5)); }
	inline GameObject_t1756533147 * get_marketPlaceMenu_5() const { return ___marketPlaceMenu_5; }
	inline GameObject_t1756533147 ** get_address_of_marketPlaceMenu_5() { return &___marketPlaceMenu_5; }
	inline void set_marketPlaceMenu_5(GameObject_t1756533147 * value)
	{
		___marketPlaceMenu_5 = value;
		Il2CppCodeGenWriteBarrier(&___marketPlaceMenu_5, value);
	}

	inline static int32_t get_offset_of_weaponInventoryMenu_6() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___weaponInventoryMenu_6)); }
	inline GameObject_t1756533147 * get_weaponInventoryMenu_6() const { return ___weaponInventoryMenu_6; }
	inline GameObject_t1756533147 ** get_address_of_weaponInventoryMenu_6() { return &___weaponInventoryMenu_6; }
	inline void set_weaponInventoryMenu_6(GameObject_t1756533147 * value)
	{
		___weaponInventoryMenu_6 = value;
		Il2CppCodeGenWriteBarrier(&___weaponInventoryMenu_6, value);
	}

	inline static int32_t get_offset_of_shieldInventoryMenu_7() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___shieldInventoryMenu_7)); }
	inline GameObject_t1756533147 * get_shieldInventoryMenu_7() const { return ___shieldInventoryMenu_7; }
	inline GameObject_t1756533147 ** get_address_of_shieldInventoryMenu_7() { return &___shieldInventoryMenu_7; }
	inline void set_shieldInventoryMenu_7(GameObject_t1756533147 * value)
	{
		___shieldInventoryMenu_7 = value;
		Il2CppCodeGenWriteBarrier(&___shieldInventoryMenu_7, value);
	}

	inline static int32_t get_offset_of_armorInventoryMenu_8() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___armorInventoryMenu_8)); }
	inline GameObject_t1756533147 * get_armorInventoryMenu_8() const { return ___armorInventoryMenu_8; }
	inline GameObject_t1756533147 ** get_address_of_armorInventoryMenu_8() { return &___armorInventoryMenu_8; }
	inline void set_armorInventoryMenu_8(GameObject_t1756533147 * value)
	{
		___armorInventoryMenu_8 = value;
		Il2CppCodeGenWriteBarrier(&___armorInventoryMenu_8, value);
	}

	inline static int32_t get_offset_of_engineInventoryMenu_9() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___engineInventoryMenu_9)); }
	inline GameObject_t1756533147 * get_engineInventoryMenu_9() const { return ___engineInventoryMenu_9; }
	inline GameObject_t1756533147 ** get_address_of_engineInventoryMenu_9() { return &___engineInventoryMenu_9; }
	inline void set_engineInventoryMenu_9(GameObject_t1756533147 * value)
	{
		___engineInventoryMenu_9 = value;
		Il2CppCodeGenWriteBarrier(&___engineInventoryMenu_9, value);
	}

	inline static int32_t get_offset_of_shipsSlotMenu_10() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___shipsSlotMenu_10)); }
	inline GameObject_t1756533147 * get_shipsSlotMenu_10() const { return ___shipsSlotMenu_10; }
	inline GameObject_t1756533147 ** get_address_of_shipsSlotMenu_10() { return &___shipsSlotMenu_10; }
	inline void set_shipsSlotMenu_10(GameObject_t1756533147 * value)
	{
		___shipsSlotMenu_10 = value;
		Il2CppCodeGenWriteBarrier(&___shipsSlotMenu_10, value);
	}

	inline static int32_t get_offset_of_weaponsSlotMenu_11() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___weaponsSlotMenu_11)); }
	inline GameObject_t1756533147 * get_weaponsSlotMenu_11() const { return ___weaponsSlotMenu_11; }
	inline GameObject_t1756533147 ** get_address_of_weaponsSlotMenu_11() { return &___weaponsSlotMenu_11; }
	inline void set_weaponsSlotMenu_11(GameObject_t1756533147 * value)
	{
		___weaponsSlotMenu_11 = value;
		Il2CppCodeGenWriteBarrier(&___weaponsSlotMenu_11, value);
	}

	inline static int32_t get_offset_of_armorSlotMenu_12() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___armorSlotMenu_12)); }
	inline GameObject_t1756533147 * get_armorSlotMenu_12() const { return ___armorSlotMenu_12; }
	inline GameObject_t1756533147 ** get_address_of_armorSlotMenu_12() { return &___armorSlotMenu_12; }
	inline void set_armorSlotMenu_12(GameObject_t1756533147 * value)
	{
		___armorSlotMenu_12 = value;
		Il2CppCodeGenWriteBarrier(&___armorSlotMenu_12, value);
	}

	inline static int32_t get_offset_of_shieldSlotMenu_13() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___shieldSlotMenu_13)); }
	inline GameObject_t1756533147 * get_shieldSlotMenu_13() const { return ___shieldSlotMenu_13; }
	inline GameObject_t1756533147 ** get_address_of_shieldSlotMenu_13() { return &___shieldSlotMenu_13; }
	inline void set_shieldSlotMenu_13(GameObject_t1756533147 * value)
	{
		___shieldSlotMenu_13 = value;
		Il2CppCodeGenWriteBarrier(&___shieldSlotMenu_13, value);
	}

	inline static int32_t get_offset_of_enginesSlotMenu_14() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___enginesSlotMenu_14)); }
	inline GameObject_t1756533147 * get_enginesSlotMenu_14() const { return ___enginesSlotMenu_14; }
	inline GameObject_t1756533147 ** get_address_of_enginesSlotMenu_14() { return &___enginesSlotMenu_14; }
	inline void set_enginesSlotMenu_14(GameObject_t1756533147 * value)
	{
		___enginesSlotMenu_14 = value;
		Il2CppCodeGenWriteBarrier(&___enginesSlotMenu_14, value);
	}

	inline static int32_t get_offset_of_camInventoryAnim_15() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___camInventoryAnim_15)); }
	inline Animator_t69676727 * get_camInventoryAnim_15() const { return ___camInventoryAnim_15; }
	inline Animator_t69676727 ** get_address_of_camInventoryAnim_15() { return &___camInventoryAnim_15; }
	inline void set_camInventoryAnim_15(Animator_t69676727 * value)
	{
		___camInventoryAnim_15 = value;
		Il2CppCodeGenWriteBarrier(&___camInventoryAnim_15, value);
	}

	inline static int32_t get_offset_of_inventoryAnim_16() { return static_cast<int32_t>(offsetof(MainMenu_t4009084430, ___inventoryAnim_16)); }
	inline Animator_t69676727 * get_inventoryAnim_16() const { return ___inventoryAnim_16; }
	inline Animator_t69676727 ** get_address_of_inventoryAnim_16() { return &___inventoryAnim_16; }
	inline void set_inventoryAnim_16(Animator_t69676727 * value)
	{
		___inventoryAnim_16 = value;
		Il2CppCodeGenWriteBarrier(&___inventoryAnim_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
