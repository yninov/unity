﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController
struct GameController_t3607102586;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m1439649957 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::Awake()
extern "C"  void GameController_Awake_m2066391706 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
