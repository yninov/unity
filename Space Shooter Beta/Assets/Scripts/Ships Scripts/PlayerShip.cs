﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerShip
{
    public int shipID;
    public string shipName;
    public float shipMaxArmor;
    public float shipRegenArmor;
    public float shipMaxShield;
    public float shipRegenShield;
    public float shipSpeed;
    public float shipFireRate;
    public float armorDamage;
    public float shieldDamage;
    public bool purchasedShip;
    public int shipPrice;
    public string shipType;

    public PlayerShip(int id, string name, float armor, float regenA, float shield,
        float regenS, float speed, float fireRate,float adamage, float sdamage, bool purchased,int price, string type)
    {
        shipID = id;
        shipName = name;
        shipMaxArmor = armor;
        shipRegenArmor = regenA;
        shipMaxShield = shield;
        shipRegenShield = regenS;
        shipSpeed = speed;
        shipFireRate = fireRate;
        armorDamage = adamage;
        shieldDamage = sdamage;
        purchasedShip = purchased;
        shipPrice = price;
        shipType = type;
    }

    public PlayerShip()
    {
        shipID = -1;
    }
}
