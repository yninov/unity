﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipsEquipment : MonoBehaviour 
{
    public ItemDatabase data;
    public int shipID;
    public List<int> equipment = new List<int>();

    public Item weapon;
    public Item armor;
    public Item shield;
    public Item engine;
	
    void Start()
    {
        SetEqupedItems();
    }

    void SetEqupedItems()
    {
        for (int i = 0; i < data.weaponsDB.Count; i++)
        {
            if (data.weaponsDB[i].itemID == equipment[0])
            {
                weapon = data.weaponsDB[i];
            }
        }
        for (int i = 0; i < data.armorsDB.Count; i++)
        {
            if (data.armorsDB[i].itemID == equipment[1])
            {
                armor = data.armorsDB[i];
            }
        }
        for (int i = 0; i < data.shieldsDB.Count; i++)
        {
            if (data.shieldsDB[i].itemID == equipment[2])
            {
                shield = data.shieldsDB[i];
            }
        }
        for (int i = 0; i < data.enginesDB.Count; i++)
        {
            if (data.enginesDB[i].itemID == equipment[3])
            {
                engine = data.enginesDB[i];
            }
        }
    }

    public void SetEquipmentIDs()
    {
        if (weapon != null)
        {
            equipment[0] = weapon.itemID;
        }
        if (armor != null)
        {
            equipment[1] = armor.itemID;
        }
        if (shield != null)
        {
            equipment[2] = shield.itemID;
        }
        if (engine != null)
        {
            equipment[3] = engine.itemID;
        }
    }
}
