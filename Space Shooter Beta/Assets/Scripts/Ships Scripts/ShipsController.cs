﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipsController : MonoBehaviour 
{
    public ShipsDatabase ships;
    
    public List<PlayerShip> allShips = new List<PlayerShip>();
    public List<GameObject> purchasedShips = new List<GameObject>();
    public bool shipAdded0 = false;
    public bool shipAdded1 = false;
    public bool shipAdded2 = false;
    public bool shipAdded3 = false;

    public GameObject ship0;
    public GameObject ship1;
    public GameObject ship2;
    public GameObject ship3;
    public int shipRotationNumber = 0;
    public ItemInventory itemInventory;

    void Start()
    {
        CheckPurchaseStatus();
        SwapShips();
        itemInventory.GetComponent<ItemInventory>();
    }
        
    public void Next()
    {
        shipRotationNumber++;
        if (shipRotationNumber == purchasedShips.Count)
        {
            shipRotationNumber = 0;
        }
        SwapShips();
        SendActivShipID();
        itemInventory.ChekEquipmentAtStartUp();
    }

    public void Previous()
    {
        shipRotationNumber--;
        if (shipRotationNumber == -1)
        {
            shipRotationNumber = purchasedShips.Count -1;
        }
        SwapShips();
        SendActivShipID();
        itemInventory.ChekEquipmentAtStartUp();
    }

    void SwapShips()
    {
        for (int i = 0; i < purchasedShips.Count; i++)
        {
            if (shipRotationNumber != i)
            {
                purchasedShips[i].SetActive(false);
            }
        }
        if (shipRotationNumber == 0)
        {
            purchasedShips[0].SetActive(true);
            return;
        }

        if (shipRotationNumber == 1)
        {
            purchasedShips[1].SetActive(true);
            return;
        }

        if (shipRotationNumber == 2)
        {          
            purchasedShips[2].SetActive(true);
            return;
        }

        if (shipRotationNumber == 3)
        {
            purchasedShips[3].SetActive(true);
            return;
        }

    }

    public void CheckPurchaseStatus()
    {
        if (ships.dataBase[0].purchasedShip == true && shipAdded0 == false)
        {
            allShips.Add(ships.dataBase[0]);
            purchasedShips.Add(ship0);
            shipAdded0 = true;
        }
        if (ships.dataBase[1].purchasedShip == true && shipAdded1 == false)
        {
            allShips.Add(ships.dataBase[1]);
            purchasedShips.Add(ship1);
            shipAdded1 = true;
        }
        if (ships.dataBase[2].purchasedShip == true && shipAdded2 == false)
        {
            allShips.Add(ships.dataBase[2]);
            purchasedShips.Add(ship2);
            shipAdded2 = true;
        }
        if (ships.dataBase[3].purchasedShip == true && shipAdded3 == false)
        {
            allShips.Add(ships.dataBase[3]);
            purchasedShips.Add(ship3);
            shipAdded3 = true;
        }
    }

    void SendActivShipID()
    {
        if (ship0.activeSelf)
        {
            GameController.current.shipID = 0;
        }
        if (ship1.activeSelf)
        {
            GameController.current.shipID = 1;
        }
        if (ship2.activeSelf)
        {
            GameController.current.shipID = 2;
        }
        if (ship3.activeSelf)
        {
            GameController.current.shipID = 3;
        }
    }
}



