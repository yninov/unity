﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmmisionControl : MonoBehaviour {

    public Renderer render;
    public Material mat;


    void Start()
    {

        render = GetComponent<Renderer>();
        mat = render.material;
    }

    void Update()
    {

        float emission = Mathf.PingPong (Time.time, 1.0f);
        Color baseColor = Color.yellow; //Replace this with whatever you want for your base color at emission level '1'

        Color finalColor = baseColor * Mathf.LinearToGammaSpace (emission);

        mat.SetColor ("_EmissionColor", finalColor);
       
    }
}
