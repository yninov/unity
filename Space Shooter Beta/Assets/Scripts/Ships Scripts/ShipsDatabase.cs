﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class ShipsDatabase : MonoBehaviour
{
    public ItemInventory inventory;
    public List<PlayerShip> dataBase = new List<PlayerShip>();

    void Start()
    {
        LoadGame();
    }

    public void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        BinaryFormatter bf2 = new BinaryFormatter();
        FileStream fs = new FileStream(Application.persistentDataPath + "/save.dat", FileMode.OpenOrCreate);
        bf.Serialize(fs, dataBase);
        bf2.Serialize(fs, inventory.inventoryByID);
        bf2.Serialize(fs, inventory.ship0Equipment.equipment);
        bf2.Serialize(fs, inventory.ship1Equipment.equipment);
        bf2.Serialize(fs, inventory.ship2Equipment.equipment);
        bf2.Serialize(fs, inventory.ship3Equipment.equipment);
        fs.Close();
        Debug.Log("SaveGame");
    }

    public void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/save.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            BinaryFormatter bf2 = new BinaryFormatter();
            FileStream fs = new FileStream(Application.persistentDataPath + "/save.dat", FileMode.Open);
            dataBase = (List<PlayerShip>)bf.Deserialize(fs);
            inventory.inventoryByID = (List<int>)bf2.Deserialize(fs);
            inventory.ship0Equipment.equipment = (List<int>)bf2.Deserialize(fs);
            inventory.ship1Equipment.equipment = (List<int>)bf2.Deserialize(fs);
            inventory.ship2Equipment.equipment = (List<int>)bf2.Deserialize(fs);
            inventory.ship3Equipment.equipment = (List<int>)bf2.Deserialize(fs);
            fs.Close();
            Debug.Log("Load Save.dat");
        }
        else
        {
            PlayerShip ship0 = new PlayerShip(0,"Shipy",10f,0f,10f,0f,6f,0.5f,10f,10f,true,0,"Alpha");
            PlayerShip ship1 = new PlayerShip(1,"Bulky",20f,0f,10f,0f,5f,0.4f,10f,10f,false,5000,"Beta");
            PlayerShip ship2 = new PlayerShip(2,"Wings",10,0f,20f,0f,6f,0.4f,10f,10f,false,5000,"Gama");
            PlayerShip ship3 = new PlayerShip(3,"Ultimate",20f,0f,20f,0f,7f,0.4f,10f,10f,false,5000,"Delta");
            dataBase.Add(ship0);
            dataBase.Add(ship1);
            dataBase.Add(ship2);
            dataBase.Add(ship3);
            Debug.Log("Load new game");
        }

    }
}
