﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipStats : MonoBehaviour {

    public int shipID;

    public PlayerShip ship;
    public ShipsDatabase ships;

    void Start()
    {
        for (int i = 0; i < ships.dataBase.Count; i++)
        {
            if (ships.dataBase[i].shipID == shipID)
            {
                ship = ships.dataBase[i];
            }
        }
    }
}
