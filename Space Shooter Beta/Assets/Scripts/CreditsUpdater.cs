﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsUpdater : MonoBehaviour {

    public Text creditsText;

    void Update()
    {
        creditsText.text = ("Credits:" + GameController.current.credits);
    }
}
