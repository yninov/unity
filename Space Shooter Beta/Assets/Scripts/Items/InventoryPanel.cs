﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPanel : MonoBehaviour 
{
    public GameObject itemDatabase;
    public ItemDatabase data;

    public GameObject mainMenu;
    public ItemInventory itemInventory;
    public MainMenu menuScript;

    public Text itemIdText;
    public Text itemNameText;
    public Text itemBonusText;
    public Text itemTypeText;
    public Image itemImageSprite;
    public Item itemAdded;

    void Start()
    {
        itemDatabase = GameObject.Find("ItemDatabase");
        data = itemDatabase.GetComponent<ItemDatabase>();
        mainMenu = GameObject.Find("Main Menu");
        itemInventory = mainMenu.GetComponent<ItemInventory>();
        menuScript = mainMenu.GetComponent<MainMenu>();
    }

    public void AddItem(Item itemToAdd)
    {
        if (itemToAdd.itemType == Item.ItemType.WeaponItem)
        {
            itemIdText.text = ("ID:" + itemToAdd.itemID);
            itemImageSprite.sprite = itemToAdd.itemImage;
            itemNameText.text = ("Name: " + itemToAdd.itemName);
            itemBonusText.text = ("Bonus: " + "\n" + "Fire Rate: " + itemToAdd.fireRate + "\n" + "Shiled Damage: " + itemToAdd.shieldDamage
            + "\n" + "Armor Damage: " + itemToAdd.armorDamage);
            itemTypeText.text = ("Type: " + itemToAdd.itemShipType);
            itemAdded = itemToAdd;
            return;
        }
        if (itemToAdd.itemType == Item.ItemType.ArmorItem)
        {
            itemIdText.text = ("ID:" + itemToAdd.itemID);
            itemImageSprite.sprite = itemToAdd.itemImage;
            itemNameText.text = ("Name: " + itemToAdd.itemName);
            itemBonusText.text = ("Bonus: " + "\n" + "Armor: " + itemToAdd.armor);
            itemTypeText.text = ("Type: " + itemToAdd.itemShipType);
            itemAdded = itemToAdd;
            return;
        }
        if (itemToAdd.itemType == Item.ItemType.ShieldItem)
        {
            itemIdText.text = ("ID:" + itemToAdd.itemID);
            itemImageSprite.sprite = itemToAdd.itemImage;
            itemNameText.text = ("Name: " + itemToAdd.itemName);
            itemBonusText.text = ("Bonus: " + "\n" + "Shield: " + itemToAdd.shield + "\n" + "Shield Regen: " + itemToAdd.shieldRegen );
            itemTypeText.text = ("Type: " + itemToAdd.itemShipType);
            itemAdded = itemToAdd;
            return;
        }
        if (itemToAdd.itemType == Item.ItemType.EngineItem)
        {
            itemIdText.text = ("ID:" + itemToAdd.itemID);
            itemImageSprite.sprite = itemToAdd.itemImage;
            itemNameText.text = ("Name: " + itemToAdd.itemName);
            itemBonusText.text = ("Bonus: " + "\n" + "Speed: " + itemToAdd.speed);
            itemTypeText.text = ("Type: " + itemToAdd.itemShipType);
            itemAdded = itemToAdd;
            return;
        }
    }

    public void BuyFromMarket()
    {
        if (GameController.current.credits >= itemAdded.itemPrice)
        {
            GameController.current.credits -= itemAdded.itemPrice;
            for (int i = 0; i < data.weaponsDB.Count; i++)
            {
                if (data.weaponsDB[i].itemID == itemAdded.itemID)
                {
                    gameObject.SetActive(false);
                    itemInventory.inventoryPanel.AddItem(itemAdded);
                    itemInventory.weaponsSlotMenu.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
                    itemInventory.weaponHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
                    GameObject newInventoryPanel = Instantiate(itemInventory.inventoryPanelEquip, itemInventory.weaponHolder.transform);
                    newInventoryPanel.transform.SetParent(itemInventory.weaponHolder.transform);
                    itemInventory.inventoryByID.Add(itemInventory.data.weaponsDB[i].itemID);
                    return;
                }
            }
            for (int i = 0; i < data.armorsDB.Count; i++)
            {
                if (data.armorsDB[i].itemID == itemAdded.itemID)
                {
                    gameObject.SetActive(false);
                    itemInventory.inventoryPanel.AddItem(itemAdded);
                    itemInventory.armorSlotMenu.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
                    itemInventory.armorHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
                    GameObject newInventoryPanel = Instantiate(itemInventory.inventoryPanelEquip, itemInventory.armorHolder.transform);
                    newInventoryPanel.transform.SetParent(itemInventory.armorHolder.transform);
                    itemInventory.inventoryByID.Add(itemInventory.data.armorsDB[i].itemID);
                    return;
                }
            }
            for (int i = 0; i < data.shieldsDB.Count; i++)
            {
                if (data.shieldsDB[i].itemID == itemAdded.itemID)
                {
                    gameObject.SetActive(false);
                    itemInventory.inventoryPanel.AddItem(itemAdded);
                    itemInventory.shieldSlotMenu.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
                    itemInventory.shieldHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0,320);
                    GameObject newInventoryPanel = Instantiate(itemInventory.inventoryPanelEquip, itemInventory.shieldHolder.transform);
                    newInventoryPanel.transform.SetParent(itemInventory.shieldHolder.transform);
                    itemInventory.inventoryByID.Add(itemInventory.data.shieldsDB[i].itemID);
                    return;
                }
            }
            for (int i = 0; i < data.enginesDB.Count; i++)
            {
                if (data.enginesDB[i].itemID == itemAdded.itemID)
                {
                    gameObject.SetActive(false);
                    itemInventory.inventoryPanel.AddItem(itemAdded);
                    itemInventory.engineSlotMenu.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
                    itemInventory.engineHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0,320);
                    GameObject newInventoryPanel = Instantiate(itemInventory.inventoryPanelEquip, itemInventory.engineHolder.transform);
                    newInventoryPanel.transform.SetParent(itemInventory.engineHolder.transform);
                    itemInventory.inventoryByID.Add(itemInventory.data.enginesDB[i].itemID);
                    return;
                }
            }
        }
    } 

    public void SellItemFromInventory()
    {     
        Destroy(gameObject);
        if (itemAdded.itemType == Item.ItemType.WeaponItem)
        {
            itemInventory.weaponHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
            for (int i = 0; i < itemInventory.inventoryByID.Count; i++)
            {
                if (itemInventory.inventoryByID[i] == itemAdded.itemID)
                {
                    itemInventory.inventoryByID.RemoveAt(i);
                    GameController.current.credits += (itemAdded.itemPrice / 2);
                }
            }
        }
        if (itemAdded.itemType == Item.ItemType.ArmorItem)
        {
            itemInventory.armorHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
            for (int i = 0; i < itemInventory.inventoryByID.Count; i++)
            {
                if (itemInventory.inventoryByID[i] == itemAdded.itemID)
                {
                    itemInventory.inventoryByID.RemoveAt(i);
                    GameController.current.credits += (itemAdded.itemPrice / 2);
                }
            }
        }
        if (itemAdded.itemType == Item.ItemType.ShieldItem)
        {
            itemInventory.shieldHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
            for (int i = 0; i < itemInventory.inventoryByID.Count; i++)
            {
                if (itemInventory.inventoryByID[i] == itemAdded.itemID)
                {
                    itemInventory.inventoryByID.RemoveAt(i);
                    GameController.current.credits += (itemAdded.itemPrice / 2);
                }
            }
        }
        if (itemAdded.itemType == Item.ItemType.EngineItem)
        {
            itemInventory.engineHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
            for (int i = 0; i < itemInventory.inventoryByID.Count; i++)
            {
                if (itemInventory.inventoryByID[i] == itemAdded.itemID)
                {
                    itemInventory.inventoryByID.RemoveAt(i);
                    GameController.current.credits += (itemAdded.itemPrice / 2);
                }
            }
        }
    }
        
    public void EquipItems()
    {
        if (itemAdded.itemType == Item.ItemType.WeaponItem)
        {
            for (int i = 0; i < itemInventory.shipsEquipment.Count ; i++)
            {   
                if (GameController.current.shipID == itemInventory.shipsEquipment[i].shipID)
                {
                    if (itemAdded.itemShipType != itemInventory.shipController.allShips[i].shipType)
                    {
                        return;
                    }
                    itemInventory.SwitchEquipedItems(itemInventory.shipsEquipment[i], itemAdded);
                    if (itemInventory.shipsEquipment[i].weapon == null)
                    {
                        itemInventory.weaponHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
                    }
                    itemInventory.shipsEquipment[i].weapon = itemAdded;
                    itemInventory.weaponEquipImage.sprite = itemAdded.itemImage;
                    itemInventory.shipsEquipment[i].SetEquipmentIDs();
                    for (int x = 0; x < itemInventory.inventoryByID.Count; x++)
                    {
                        if (itemInventory.inventoryByID[x] == itemAdded.itemID)
                        {
                            itemInventory.inventoryByID.RemoveAt(x);

                        }
                    }
                }
            }
            Destroy(gameObject);
            menuScript.WeaponInventoryMenuClose();
        }
        if (itemAdded.itemType == Item.ItemType.ArmorItem)
        {
            for (int i = 0; i < itemInventory.shipsEquipment.Count ; i++)
            {   
                if (GameController.current.shipID == itemInventory.shipsEquipment[i].shipID)
                {
                    if (itemAdded.itemShipType != itemInventory.shipController.allShips[i].shipType)
                    {
                        return;
                    }
                    itemInventory.SwitchEquipedItems(itemInventory.shipsEquipment[i], itemAdded);
                    if (itemInventory.shipsEquipment[i].armor == null)
                    {
                        itemInventory.armorHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
                    }
                    itemInventory.shipsEquipment[i].armor = itemAdded;
                    itemInventory.armorEquipImage.sprite = itemAdded.itemImage;
                    itemInventory.shipsEquipment[i].SetEquipmentIDs();
                    for (int x = 0; x < itemInventory.inventoryByID.Count; x++)
                    {
                        if (itemInventory.inventoryByID[x] == itemAdded.itemID)
                        {
                            itemInventory.inventoryByID.RemoveAt(x);

                        }
                    }
                }
            }
            Destroy(gameObject);
            menuScript.ArmorInventoryMenuClose();
        }
        if (itemAdded.itemType == Item.ItemType.ShieldItem)
        {
            for (int i = 0; i < itemInventory.shipsEquipment.Count ; i++)
            {   
                if (GameController.current.shipID == itemInventory.shipsEquipment[i].shipID)
                {
                    if (itemAdded.itemShipType != itemInventory.shipController.allShips[i].shipType)
                    {
                        return;
                    }
                    itemInventory.SwitchEquipedItems(itemInventory.shipsEquipment[i], itemAdded);
                    if (itemInventory.shipsEquipment[i].shield == null)
                    {
                        itemInventory.shieldHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
                    }
                    itemInventory.shipsEquipment[i].shield = itemAdded;
                    itemInventory.shieldEquipImage.sprite = itemAdded.itemImage;
                    itemInventory.shipsEquipment[i].SetEquipmentIDs();
                    for (int x = 0; x < itemInventory.inventoryByID.Count; x++)
                    {
                        if (itemInventory.inventoryByID[x] == itemAdded.itemID)
                        {
                            itemInventory.inventoryByID.RemoveAt(x);

                        }
                    }
                }
            }
            Destroy(gameObject);
            menuScript.ShieldInventoryMenuClose();
        }
        if (itemAdded.itemType == Item.ItemType.EngineItem)
        {
            for (int i = 0; i < itemInventory.shipsEquipment.Count ; i++)
            {   
                if (GameController.current.shipID == itemInventory.shipsEquipment[i].shipID)
                {
                    if (itemAdded.itemShipType != itemInventory.shipController.allShips[i].shipType)
                    {
                        return;
                    }
                    itemInventory.SwitchEquipedItems(itemInventory.shipsEquipment[i], itemAdded);
                    if (itemInventory.shipsEquipment[i].engine == null)
                    {
                        itemInventory.engineHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, -320);
                    }
                    itemInventory.shipsEquipment[i].engine = itemAdded;
                    itemInventory.engineEquipImage.sprite = itemAdded.itemImage;
                    itemInventory.shipsEquipment[i].SetEquipmentIDs();
                    for (int x = 0; x < itemInventory.inventoryByID.Count; x++)
                    {
                        if (itemInventory.inventoryByID[x] == itemAdded.itemID)
                        {
                            itemInventory.inventoryByID.RemoveAt(x);

                        }
                    }
                }
            }
            Destroy(gameObject);
            menuScript.EngineInventoryMenuClose();
        }
    }

}