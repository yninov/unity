﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemInventory : MonoBehaviour 
{
    public ItemDatabase data;
    public List<ShipsEquipment> shipsEquipment = new List<ShipsEquipment>();
    public List<Item> weaponItems = new List<Item>();
    public List<Item> armorItems = new List<Item>();
    public List<Item> shieldItems = new List<Item>();
    public List<Item> engineItems = new List<Item>();
    public List<int> inventoryByID = new List<int>();

    public GameObject weaponsSlotMenu;
    public GameObject armorSlotMenu;
    public GameObject shieldSlotMenu;
    public GameObject engineSlotMenu;


    public GameObject weaponHolder;
    public GameObject armorHolder;
    public GameObject shieldHolder;
    public GameObject engineHolder;
    public GameObject inventoryPanelBuy;
    public GameObject inventoryPanelEquip;
    public InventoryPanel marketInventoryPanel;
    public InventoryPanel inventoryPanel;
    public ShipsController shipController;

    public ShipsEquipment ship0Equipment;
    public ShipsEquipment ship1Equipment;
    public ShipsEquipment ship2Equipment;
    public ShipsEquipment ship3Equipment;

    public Image weaponEquipImage;
    public Image armorEquipImage;
    public Image shieldEquipImage;
    public Image engineEquipImage;

    void Start()
    {
        marketInventoryPanel = inventoryPanelBuy.GetComponent<InventoryPanel>();
        inventoryPanel = inventoryPanelEquip.GetComponent<InventoryPanel>();

        ItemsToList();
        ChekInventoryAtStartUp();
        ChekEquipmentAtStartUp();
        ControlItemsList();
        SpawnMarketItems();
        InstantiateWeapon();
    }

    void ItemsToList()
    {
        for (int i = 0; i < data.weaponsDB.Count; i++)
        {
            weaponItems.Add(data.weaponsDB[i]);
        }

        for (int i = 0; i < data.armorsDB.Count; i++)
        {
            armorItems.Add(data.armorsDB[i]);
        }

        for (int i = 0; i < data.shieldsDB.Count; i++)
        {
            shieldItems.Add(data.shieldsDB[i]);
        }

        for (int i = 0; i < data.enginesDB.Count; i++)
        {
            engineItems.Add(data.enginesDB[i]);
        }
    }

    void ControlItemsList() // need reworking
    {
        for (int i = 0; i < shipsEquipment.Count; i++)
        {
            Item weapon = GetWeaponItem(shipsEquipment[i].equipment[0]);
            Item armor = GetArmorItem(shipsEquipment[i].equipment[1]);
            Item shield = GetShieldItem(shipsEquipment[i].equipment[2]);
            Item engine = GetEngineItem(shipsEquipment[i].equipment[3]);

            if (weapon != null)
            {
                for (int a = 0; a < weaponItems.Count; a++)
                {
                    if (weapon == weaponItems[a])
                    {
                        weaponItems.RemoveAt(a);
                    }
                }
            }
            if (armor != null)
            {
                for (int a = 0; a < armorItems.Count; a++)
                {
                    if (armor == armorItems[a])
                    {
                        armorItems.RemoveAt(a);
                    }
                }
            }
            if (shield != null)
            {
                for (int a = 0; a < shieldItems.Count; a++)
                {
                    if (shield == shieldItems[a])
                    {
                        shieldItems.RemoveAt(a);
                    }
                }
            }

            if (engine != null)
            {
                for (int a = 0; a < engineItems.Count; a++)
                {
                    if (engine == engineItems[a])
                    {
                        engineItems.RemoveAt(a);
                    }
                }
            }

        }
    }

    public void ChekEquipmentAtStartUp() // need reworking
    {
        for (int i = 0; i < shipsEquipment.Count; i++)
        {
            Item weapon = GetWeaponItem(shipsEquipment[i].equipment[0]);
            Item armor = GetArmorItem(shipsEquipment[i].equipment[1]);
            Item shield = GetShieldItem(shipsEquipment[i].equipment[2]);
            Item engine = GetEngineItem(shipsEquipment[i].equipment[3]);
            if (GameController.current.shipID == shipsEquipment[i].shipID)
            {
                if (weapon != null)
                {
                    weaponEquipImage.sprite = weapon.itemImage;
                }
                else if (weapon == null)
                {
                    weaponEquipImage.sprite = null;
                }
                if (armor != null)
                {
                    armorEquipImage.sprite = armor.itemImage;
                }
                else if (armor == null)
                {
                    armorEquipImage.sprite = null;
                }
                if (shield != null)
                {
                    shieldEquipImage.sprite = shield.itemImage;
                }
                else if (shield == null)
                {
                    shieldEquipImage.sprite = null;
                }
                if (engine != null)
                {
                    engineEquipImage.sprite = engine.itemImage;
                }
                else if (engine == null)
                {
                    engineEquipImage.sprite = null;
                }
            }
        }
    }

    void ChekInventoryAtStartUp()
    {
        for (int i = 0; i < inventoryByID.Count; i++)
        {
            Item weapon = GetWeaponItem(inventoryByID[i]);
            Item armor = GetArmorItem(inventoryByID[i]);
            Item shield = GetShieldItem(inventoryByID[i]);
            Item engine = GetEngineItem(inventoryByID[i]);
            if (weapon != null)
            {
                inventoryPanel.AddItem(weapon);
                weaponHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
                GameObject newInventoryPanel = Instantiate(inventoryPanelEquip, weaponHolder.transform);
                newInventoryPanel.transform.SetParent(weaponHolder.transform);
                for(int a = 0; a < weaponItems.Count; a++)
                {
                    if (weapon == weaponItems[a])
                    {
                        weaponItems.RemoveAt(a);
                    }
                }
            }
            if (armor != null)
            {
                inventoryPanel.AddItem(armor);
                weaponHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
                GameObject newInventoryPanel = Instantiate(inventoryPanelEquip, armorHolder.transform);
                newInventoryPanel.transform.SetParent(armorHolder.transform);
                for(int a = 0; a < armorItems.Count; a++)
                {
                    if (armor == armorItems[a])
                    {
                        armorItems.RemoveAt(a);
                    }
                }

            }
            if (shield != null)
            {
                inventoryPanel.AddItem(shield);
                weaponHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
                GameObject newInventoryPanel = Instantiate(inventoryPanelEquip, shieldHolder.transform);
                newInventoryPanel.transform.SetParent(shieldHolder.transform);
                for(int a = 0; a < shieldItems.Count; a++)
                {
                    if (shield == shieldItems[a])
                    {
                        shieldItems.RemoveAt(a);
                    }
                }

            }
            if (engine != null)
            {
                inventoryPanel.AddItem(engine);
                weaponHolder.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
                GameObject newInventoryPanel = Instantiate(inventoryPanelEquip, engineHolder.transform);
                newInventoryPanel.transform.SetParent(engineHolder.transform);
                for(int a = 0; a < engineItems.Count; a++)
                {
                    if (engine == engineItems[a])
                    {
                        engineItems.RemoveAt(a);
                    }
                }

            }

        }
    }

    Item GetWeaponItem(int id)
    {
        for (int i = 0; i < data.weaponsDB.Count; i++)
        {
            if (data.weaponsDB[i].itemID == id)
            {
                return data.weaponsDB[i];
            }

        }
        return null;
    }

    Item GetArmorItem(int id)
    {
        for (int i = 0; i < data.armorsDB.Count; i++)
        {
            if (data.armorsDB[i].itemID == id)
            {
                return data.armorsDB[i];
            }

        }
        return null;
    }

    Item GetShieldItem(int id)
    {
        for (int i = 0; i < data.shieldsDB.Count; i++)
        {
            if (data.shieldsDB[i].itemID == id)
            {
                return data.shieldsDB[i];
            }

        }
        return null;
    }

    Item GetEngineItem(int id)
    {
        for (int i = 0; i < data.enginesDB.Count; i++)
        {
            if (data.enginesDB[i].itemID == id)
            {
                return data.enginesDB[i];
            }

        }
        return null;
    }

    void SpawnMarketItems()
    {
        for (int i = 0; i < weaponItems.Count; i++)
        {
            marketInventoryPanel.AddItem(weaponItems[i]);
            weaponsSlotMenu.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
            GameObject newInventoryPanel = Instantiate(inventoryPanelBuy, weaponsSlotMenu.transform);
            newInventoryPanel.transform.SetParent(weaponsSlotMenu.transform);
        }
        for (int i = 0; i < armorItems.Count; i++)
        {
            marketInventoryPanel.AddItem(armorItems[i]);
            armorSlotMenu.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
            GameObject newInventoryPanel = Instantiate(inventoryPanelBuy, armorSlotMenu.transform);
            newInventoryPanel.transform.SetParent(armorSlotMenu.transform);
        }
        for (int i = 0; i < shieldItems.Count; i++)
        {
            marketInventoryPanel.AddItem(shieldItems[i]);
            shieldSlotMenu.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
            GameObject newInventoryPanel = Instantiate(inventoryPanelBuy, shieldSlotMenu.transform);
            newInventoryPanel.transform.SetParent(shieldSlotMenu.transform);
        }
        for (int i = 0; i < engineItems.Count; i++)
        {
            marketInventoryPanel.AddItem(engineItems[i]);
            engineSlotMenu.GetComponent<RectTransform>().sizeDelta += new Vector2(0, 320);
            GameObject newInventoryPanel = Instantiate(inventoryPanelBuy, engineSlotMenu.transform);
            newInventoryPanel.transform.SetParent(engineSlotMenu.transform);
        }
    }

    public void SwitchEquipedItems(ShipsEquipment ship, Item item)
    {
        SwitchEquipedItemsVisual(item);
        if (item.itemType == Item.ItemType.WeaponItem)
        {
            if (ship.weapon != null)
            {
                inventoryPanel.AddItem(ship.weapon);
                GameObject newInventoryPanel = Instantiate(inventoryPanelEquip, weaponHolder.transform);
                newInventoryPanel.transform.SetParent(weaponHolder.transform);
                inventoryByID.Add(ship.weapon.itemID);
            }
        }
        if (item.itemType == Item.ItemType.ArmorItem)
        {
            if (ship.armor != null)
            {
                inventoryPanel.AddItem(ship.armor);
                GameObject newInventoryPanel = Instantiate(inventoryPanelEquip, armorHolder.transform);
                newInventoryPanel.transform.SetParent(armorHolder.transform);
                inventoryByID.Add(ship.armor.itemID);
            }
        }
        if (item.itemType == Item.ItemType.ShieldItem)
        {
            if (ship.shield != null)
            {
                inventoryPanel.AddItem(ship.shield);
                GameObject newInventoryPanel = Instantiate(inventoryPanelEquip, shieldHolder.transform);
                newInventoryPanel.transform.SetParent(shieldHolder.transform);
                inventoryByID.Add(ship.shield.itemID);
            }
        }
        if (item.itemType == Item.ItemType.EngineItem)
        {
            if (ship.engine != null)
            {
                inventoryPanel.AddItem(ship.engine);
                GameObject newInventoryPanel = Instantiate(inventoryPanelEquip, engineHolder.transform);
                newInventoryPanel.transform.SetParent(engineHolder.transform);
                inventoryByID.Add(ship.engine.itemID);
            }
        }

    }



    /* UNDER CONSTRUCTION */

    public void InstantiateWeapon()
    {
        for (int i = 0; i < shipsEquipment.Count; i++)
        {
            if (GameController.current.shipID == shipsEquipment[i].shipID)
            {
                if (shipsEquipment[i].weapon != null)
                {
                    GameObject newWeapon = Instantiate(shipsEquipment[i].weapon.itemVisual, shipController.purchasedShips[i].transform);
                    newWeapon.transform.SetParent(shipController.purchasedShips[i].transform);
                }
                if (shipsEquipment[i].armor != null)
                {
                    GameObject newArmor = Instantiate(shipsEquipment[i].armor.itemVisual, shipController.purchasedShips[i].transform);
                    newArmor.transform.SetParent(shipController.purchasedShips[i].transform);
                }
                if (shipsEquipment[i].shield != null)
                {
                    GameObject newShield = Instantiate(shipsEquipment[i].shield.itemVisual, shipController.purchasedShips[i].transform);
                    newShield.transform.SetParent(shipController.purchasedShips[i].transform);
                }
                if (shipsEquipment[i].engine != null)
                {
                    GameObject newEngine = Instantiate(shipsEquipment[i].engine.itemVisual, shipController.purchasedShips[i].transform);
                    newEngine.transform.SetParent(shipController.purchasedShips[i].transform);
                }

            }
        }
    }

    public void SwitchEquipedItemsVisual(Item item)
    {
        for (int i = 0; i < shipsEquipment.Count; i++)
        {
            if (GameController.current.shipID == shipsEquipment[i].shipID)
            {
                if (item.itemType == Item.ItemType.WeaponItem)
                {
                    if (shipsEquipment[i].weapon != null)
                    {
                        GameObject currentWeapon = GameObject.FindGameObjectWithTag("WeaponItem");
                        Destroy(currentWeapon);
                    }
                    GameObject newWeapon = Instantiate(item.itemVisual, shipController.purchasedShips[i].transform);
                    newWeapon.transform.SetParent(shipController.purchasedShips[i].transform);
                }

                if (item.itemType == Item.ItemType.ArmorItem)
                {
                    if (shipsEquipment[i].armor != null)
                    {
                        GameObject currentArmor = GameObject.FindGameObjectWithTag("ArmorItem");
                        Destroy(currentArmor);
                    }
                    GameObject newArmor = Instantiate(item.itemVisual, shipController.purchasedShips[i].transform);
                    newArmor.transform.SetParent(shipController.purchasedShips[i].transform);
                }

                if (item.itemType == Item.ItemType.ShieldItem)
                {
                    if (shipsEquipment[i].shield != null)
                    {
                        GameObject currentShield = GameObject.FindGameObjectWithTag("ShieldItem");
                        Destroy(currentShield);
                    }
                    GameObject newShiled = Instantiate(item.itemVisual, shipController.purchasedShips[i].transform);
                    newShiled.transform.SetParent(shipController.purchasedShips[i].transform);
                }

                if (item.itemType == Item.ItemType.EngineItem)
                {
                    if (shipsEquipment[i].engine != null)
                    {
                        GameObject currentEngine = GameObject.FindGameObjectWithTag("EngineItem");
                        Destroy(currentEngine);
                    }
                    GameObject newEngine = Instantiate(item.itemVisual, shipController.purchasedShips[i].transform);
                    newEngine.transform.SetParent(shipController.purchasedShips[i].transform);
                }
            }
        }
    }
}
