﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour 
{
    public List<Item> weaponsDB = new List<Item>();
    public List<Item> armorsDB = new List<Item>();
    public List<Item> shieldsDB = new List<Item>();
    public List<Item> enginesDB = new List<Item>();

    void Start()
    {
        for (int i = 0; i < weaponsDB.Count; i++)
        {
            if (weaponsDB[i].itemID == 0)
            {
                int randomID = Random.Range(1, 100);
                if (randomID != weaponsDB[i].itemID)
                {
                    weaponsDB[i].itemID = randomID;
                }
            }
        }
        for (int i = 0; i < armorsDB.Count; i++)
        {
            if (armorsDB[i].itemID == 0)
            {
                int randomID = Random.Range(101, 200);
                if (randomID != armorsDB[i].itemID)
                {
                    armorsDB[i].itemID = randomID;
                }
            }
        }
        for (int i = 0; i < shieldsDB.Count; i++)
        {
            if (shieldsDB[i].itemID == 0)
            {
                int randomID = Random.Range(201, 300);
                if (randomID != shieldsDB[i].itemID)
                {
                    shieldsDB[i].itemID = randomID;
                }
            }
        }
        for (int i = 0; i < enginesDB.Count; i++)
        {
            if (enginesDB[i].itemID == 0)
            {
                int randomID = Random.Range(301, 400);
                if (randomID != enginesDB[i].itemID)
                {
                    enginesDB[i].itemID = randomID;
                }
            }
        }
    }
}
