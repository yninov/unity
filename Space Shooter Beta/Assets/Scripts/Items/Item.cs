﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu()]
public class Item : ScriptableObject 
{
    public int itemID;
    public string itemName;
    public int itemPrice;
    public float armor;
    public float shield;
    public float shieldRegen;
    public float speed;
    public float fireRate;
    public float shieldDamage;
    public float armorDamage;
    public Sprite itemImage;
    public GameObject itemVisual;
    public bool itemPurchased;
    public WeaponType weaponType;
    public ItemType itemType;
    public ItemRarity itemRarity;
    public string itemShipType;

    public enum WeaponType
    {
        None,
        PulseLaser,
        BeamLaser,
        Roket,
        Drone,
        Hybrid,
        Autocannon,
        Lighting
    }

    public enum ItemType
    {
        WeaponItem,
        ShieldItem,
        ArmorItem,
        EngineItem
    }
	
    public enum ItemRarity
    {
        Grey,
        Green,
        Blue,
        Purple,
        Orange
    }
}
