﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {
    
    public GameObject inventoryMenu;
    public GameObject abilityMenu;
    public GameObject missionMenu;
    public GameObject marketPlaceMenu;
    public GameObject weaponInventoryMenu;
    public GameObject shieldInventoryMenu;
    public GameObject armorInventoryMenu;
    public GameObject engineInventoryMenu;
    public GameObject shipsSlotMenu;
    public GameObject weaponsSlotMenu;
    public GameObject armorSlotMenu;
    public GameObject shieldSlotMenu;
    public GameObject enginesSlotMenu;

    public Animator camInventoryAnim;
    public Animator inventoryAnim;





    public void MarketPlaceMenuOpen()
    {
        marketPlaceMenu.SetActive(true);
    }
    public void MarketPlaceMenuClose()
    {
        marketPlaceMenu.SetActive(false);
    }

    public void InventoryMenuOpen()
    {
        //inventoryMenu.SetActive(true);
        camInventoryAnim.SetTrigger("CamInventoryOpen");
        inventoryAnim.SetTrigger("InventoryOpen");
    }
    public void InventoryMenuClose()
    {
        //inventoryMenu.SetActive(false);
        camInventoryAnim.SetTrigger("CamInventoryClose");
        inventoryAnim.SetTrigger("InventoryClose");
    }

    public void AbilityMenuOpen()
    {
        abilityMenu.SetActive(true);
    }
    public void AbilityMenuClose()
    {
        abilityMenu.SetActive(false);
    }

    public void MissionMenuOpen()
    {
        missionMenu.SetActive(true);
    }
    public void MissionMenuClose()
    {
        missionMenu.SetActive(false);
    }

    public void WeaponInventoryMenuOpen()
    {
        weaponInventoryMenu.SetActive(true);
    }
    public void WeaponInventoryMenuClose()
    {
        weaponInventoryMenu.SetActive(false);
    }

    public void ShieldInventoryMenuOpen()
    {
        shieldInventoryMenu.SetActive(true);
    }
    public void ShieldInventoryMenuClose()
    {
        shieldInventoryMenu.SetActive(false);
    }

    public void ArmorInventoryMenuOpen()
    {
        armorInventoryMenu.SetActive(true);
    }
    public void ArmorInventoryMenuClose()
    {
        armorInventoryMenu.SetActive(false);
    }

    public void EngineInventoryMenuOpen()
    {
        engineInventoryMenu.SetActive(true);
    }
    public void EngineInventoryMenuClose()
    {
        engineInventoryMenu.SetActive(false);
    }

    public void ShipsSlotMenuOpen()
    {
        shipsSlotMenu.SetActive(true);
        weaponsSlotMenu.SetActive(false);
        armorSlotMenu.SetActive(false);
        shieldSlotMenu.SetActive(false);
        enginesSlotMenu.SetActive(false);
    }
    public void WeaponsSlotMenuOpen()
    {
        shipsSlotMenu.SetActive(false);
        weaponsSlotMenu.SetActive(true);
        armorSlotMenu.SetActive(false);
        shieldSlotMenu.SetActive(false);
        enginesSlotMenu.SetActive(false);
    }
    public void ArmorSlotMenuOpen()
    {
        shipsSlotMenu.SetActive(false);
        weaponsSlotMenu.SetActive(false);
        armorSlotMenu.SetActive(true);
        shieldSlotMenu.SetActive(false);
        enginesSlotMenu.SetActive(false);
    }
    public void ShieldSlotMenuOpen()
    {
        shipsSlotMenu.SetActive(false);
        weaponsSlotMenu.SetActive(false);
        armorSlotMenu.SetActive(false);
        shieldSlotMenu.SetActive(true);
        enginesSlotMenu.SetActive(false);
    }
    public void EngnesSlotMenuOpen()
    {
        shipsSlotMenu.SetActive(false);
        weaponsSlotMenu.SetActive(false);
        armorSlotMenu.SetActive(false);
        shieldSlotMenu.SetActive(false);
        enginesSlotMenu.SetActive(true);
    }
}
