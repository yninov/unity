﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Marketplace : MonoBehaviour 
{  
    private ShipsDatabase ships;
    public ShipsController shipController;

    //public GameObject slot;
    public int shipNumber;
    public Text shipName;
    public Text price;
    public Text id;


    void Start()
    {
        ships = GameObject.FindGameObjectWithTag("ShipDatabase").GetComponent<ShipsDatabase>();
        id.text = ("ID: " + ships.dataBase[shipNumber].shipID);
        shipName.text = ("Name: " + ships.dataBase[shipNumber].shipName);
        price.text = ("Price: " + ships.dataBase[shipNumber].shipPrice);

        if (ships.dataBase[shipNumber].purchasedShip == true)
        {
            gameObject.SetActive(false);
        }
    }

    public void BuyShip()
    {
        if (GameController.current.credits >= ships.dataBase[shipNumber].shipPrice)
        {
            GameController.current.credits -= ships.dataBase[shipNumber].shipPrice;
            ships.dataBase[shipNumber].purchasedShip = true;
            gameObject.SetActive(false);
            shipController.CheckPurchaseStatus();
        }
    }
}
