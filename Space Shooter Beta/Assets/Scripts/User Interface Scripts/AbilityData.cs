﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityData : MonoBehaviour 
{
    public float shipMaxArmor;
    public float shipRegenArmor;
    public float shipMaxShield;
    public float shipRegenShield;
    public float shipSpeed;
    public float shipFireRate;
    public float shipDamage;


    public string abilityName;
    public int abilityLevel;

    void CheckShipId()
    {
        if (GameController.current.shipID == 0)
        {
            shipMaxArmor = 10f;
            shipRegenArmor = 1f;
            shipMaxShield = 10f;
            shipRegenShield = 1f;
            shipSpeed = 0.1f;
            shipFireRate = 5f;
            shipDamage = 10f;
        }

        if (GameController.current.shipID == 1)
        {
            shipMaxArmor = 20f;
            shipRegenArmor = 1f;
            shipMaxShield = 20f;
            shipRegenShield = 1f;
            shipSpeed = 0.2f;
            shipFireRate = 4f;
            shipDamage = 10f;
        }

        if (GameController.current.shipID == 2)
        {
            shipMaxArmor = 30f;
            shipRegenArmor = 1f;
            shipMaxShield = 30f;
            shipRegenShield = 1f;
            shipSpeed = 0.2f;
            shipFireRate = 5f;
            shipDamage = 15f;
        }

        if (GameController.current.shipID == 3)
        {
            shipMaxArmor = 40f;
            shipRegenArmor = 1f;
            shipMaxShield = 40f;
            shipRegenShield = 1f;
            shipSpeed = 0.2f;
            shipFireRate = 6f;
            shipDamage = 20f;
        }
    }


	
}
