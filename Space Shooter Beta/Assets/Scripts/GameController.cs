﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController current;

    public int shipID;
    public int credits;
    public int abilityTokes;
    

    void Awake()
    {
        if (current == null)
        {
            current = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (current != this)
        {
            Destroy(gameObject);
        }
        
    }


}
